<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    public function user() {
    	return $this -> hasOne( 'App\User' , 'id' , 'from' ) -> latest();
    }
    public function lastMessage() {
    	return $this -> hasOne( 'App\Message' , 'chat_id' , 'id' ) -> latest();
    }
}
