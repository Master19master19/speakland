<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
    protected $guarded = [];

	public function mission () {
		return $this -> hasOne( 'App\Mission', 'id' , 'mission_id' ) -> orderBy( 'order' );
	}
	public function steps () {
		return $this -> hasMany( 'App\Step', 'section_id' , 'id' ) -> orderBy( 'order' );
	}
	public function stepCount () {
		return $this -> steps() -> count();
	}
}
