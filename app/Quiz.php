<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    //
    protected $guarded = [];
	public function mission () {
		return $this -> hasOne( 'App\Mission', 'id' , 'mission_id' );
	}
	public function getDurationAttribute () {
		return gmdate( "i:s" , $this -> duration_seconds );
	}
	public function questions () {
		return $this -> hasMany( 'App\Question', 'quiz_id' , 'id' );
	}
}
