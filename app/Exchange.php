<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    //
    protected $guarded = [];

	public function currency_from () {
		return $this -> hasOne( 'App\Currency', 'code_bestchange' , 'from' );
	}
    public function getPriceeAttribute ( $value ) {
        return env( 'COIN_DECIMAL' ) / $this -> price;
    }
	public function currency_to () {
		return $this -> hasOne( 'App\Currency', 'code_bestchange' , 'to' );
	}
}
