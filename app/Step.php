<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Step extends Model
{
    //
    protected $guarded = [];
    public function getNextStepAttribute () {
    	$step = Step::whereSectionId( $this -> section_id ) -> where( 'order' , '>' , $this -> order ) -> first();
    	if ( null !== $step ) {
    		if ( Auth::user() -> canStep( $step -> id ) ) {
    			return $step -> id;
    		}
    	}
    	return false;
    }
    public function getPreviousStepAttribute () {
    	$step = Step::whereSectionId( $this -> section_id ) -> where( 'order' , '<' , $this -> order ) -> orderBy( 'order' , 'DESC' ) -> first();
    	if ( null !== $step ) return $step -> id;
    	return false;
    }
	public function mission () {
		return $this -> hasOneThrough( 'App\Mission', 'App\Section' , 'id' , 'id' , 'section_id' , 'mission_id' );
	}
	public function section () {
		return $this -> hasOne( 'App\Section', 'id' , 'section_id' );
	}
	public function media () {
		return $this -> hasMany( 'App\Media', 'step_id' , 'id' ) -> orderBy( 'order' );
	}
	public function getDurationAttribute () {
		return gmdate( "i:s" , $this -> duration_seconds );
	}
}
