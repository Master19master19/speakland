<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Setting;


class Transaction extends Model
{
    //
	public function rating () {
		return $this -> hasOne( 'App\Rating', 'transaction_id' , 'id' );
	}
	public function from () {
		return $this -> hasOne( 'App\Currency', 'code_bestchange' , 'send_from' );
	}
	public function to () {
		return $this -> hasOne( 'App\Currency', 'code_bestchange' , 'send_to' );
	}
	public function user () {
		return $this -> hasOne( 'App\User', 'id' , 'user_id' );
	}
	
	public function getExpiredAttribute () {
		if ( $this -> status != 0 ) {
			return false;
		}
		$timeout = env( 'TRANSACTION_TIMEOUT_MINUTES' );
		$created = strtotime( $this -> created_at );
		if ( time() - $created < $timeout * 60 ) {
			return false;
		}
		return true;
	}
	public function getStatusTitleAttribute () {
		$statuses = [
			'Ждем оплату',
			'Проверяем вашу оплату',
			'Отправляем',
			'Транзакция прошла успешно'
		];
		if ( $this -> expired ) {
			return __( 'Время транзакции вышло' );
		} else if ( $this -> canceled ) {
			return __( 'Транзакция отменена пользователем' );
		} else {
			return __( $statuses[ $this -> status ] );
		}
	}
	public function getStatusTitleMailAttribute () {
		$statuses = [
			'Ждем оплату',
			'Проверяем вашу оплату',
			'Отправляем',
			'Транзакция прошла успешно'
		];
		if ( $this -> expired ) {
			return __( 'Время транзакции вышло' );
		} else if ( $this -> canceled ) {
			return __( 'Транзакция отменена пользователем' );
		} else {
			return __( $statuses[ $this -> status ] );
		}
	}
	public function getExpiresInAttribute () {
		$timeout = env( 'TRANSACTION_TIMEOUT_MINUTES' );
		$created = strtotime( $this -> created_at );
		return (  $created - strtotime( "-{$timeout} minutes" ) );
	}
	public function getAddressAttribute () {
		if ( $this -> btc == 1 ) {
			if ( strlen( $this -> btc_address ) > 7 ) {
				return $this -> btc_address;
			} else {
				return 'Ошибка';
			}
		} else {
			$code = $this -> from -> code;
			if ( $code == 'ETH' ) {
				$address = Setting::whereCode( 'eth_address' ) -> first() -> value;
			} else if ( $code == 'XRP' ) {
				$address = [
					Setting::whereCode( 'ripple_address' ) -> first() -> value,
					Setting::whereCode( 'ripple_destination_tag' ) -> first() -> value
				];
			} else if ( $code == 'USDT' ) {
				$address = Setting::whereCode( 'tether_address' ) -> first() -> value;
			} else if ( $code == 'p24' ) {
				$address = [
					Setting::whereCode( 'privat24_receiver_card' ) -> first() -> value,
					Setting::whereCode( 'privat24_receiver_fullname' ) -> first() -> value
				];
			} else {
				$address = 'Ошибка';
			}
			return $address;
		}
	}
	public function getQrAttribute() {
		$amount = $this -> total_send / env( 'COIN_DECIMAL' );
		if ( $this -> btc == 1 ) {
			if ( strlen( $this -> btc_address ) > 7 ) {
				$url = "https://chart.googleapis.com/chart?chs=200x200&cht=qr&chf=bg,s,FF000000&chl=bitcoin:{$this -> btc_address}?amount={$amount}";
				return $url;
			} else {
				return "Ошибка";
				// $url = "https://chart.googleapis.com/chart?chs=200x200&cht=qr&chf=bg,s,FF000000&chl=somecoin:{$this -> btc_address}?amount={$amount}";
				// return $url;
			}
		} else {
			$code = $this -> from -> code;
			if ( $code == 'ETH' ) {
				$address = Setting::whereCode( 'eth_address' ) -> first() -> value;
				$qr = "https://chart.googleapis.com/chart?chs=300&300&chld=L|2&cht=qr&chl=ethereum:" . $address;
			} else if ( $code == 'XRP' ) {
				$qr = null;
				// $address = Setting::whereCode( 'ripple_address' ) -> first() -> value;
				// $qr = "https://chart.googleapis.com/chart?chs=300&300&chld=L|2&cht=qr&chl=ripple:" . $address;
			} else if ( $code == 'USDT' ) {
				$qr = null;
				// $address = Setting::whereCode( 'tether_address' ) -> first() -> value;
				// $qr = "https://chart.googleapis.com/chart?chs=300&300&chld=L|2&cht=qr&chl=tether:" . $address;
			} else if ( $code == 'p24' ) {
				$qr = null;
			} else {
				$qr = null;
			}
			return $qr;
		}
		
	}
    protected $guarded = [
    	'status',
    	'id',
    	'checked',
    ];
}
