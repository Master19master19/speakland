<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Helpers\OtherHelper;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public function getProgress() {
        $stepOrder = $this -> step -> absolute_mission - 1;
        $stepCount = $this -> mission -> stepCount();
        $progress = 100 * $stepOrder / $stepCount;
        $progress = floor( $progress );
        return compact( 'progress' , 'stepCount' , 'stepOrder' );
    }
    public function section () {
        return $this -> hasOne( 'App\Section', 'id' , 'section_id' );
    }
    public function mission () {
        return $this -> hasOne( 'App\Mission', 'id' , 'mission_id' );
    }
    public function step () {
        return $this -> hasOne( 'App\Step', 'id' , 'step_id' );
    }

    public function canQuiz ( $quizId ) {
        $quiz = \App\Quiz::whereId( $quizId ) -> first();
        if ( ! $this -> paid ) return false;
        $missionLastStep = OtherHelper::getMissionLastStep( $quiz -> mission -> id );
        // dd($this -> step , $missionLastStep->mission,$quiz -> mission );
        if ( $this -> step -> absolute_order > $missionLastStep -> absolute_order ) {
            return true;
        } else if ( $this -> step -> absolute_order == $missionLastStep -> absolute_order ) {
            return 1;
        } else {
            return false;
        }
        if ( $quiz -> mission -> id < $this -> mission -> id ) return true;
        $section = \App\Section::where( 'mission_id' , $quiz -> mission -> id ) -> orderBy( 'order' , 'DESC' ) -> first();
        if ( $quiz -> mission -> id == $this -> mission -> id && $section -> id !== $this -> section -> id ) return false;
        $lastStep = \App\Step::where( 'section_id' , $section -> id ) -> orderBy( 'order' , 'DESC' ) -> first();
        if ( $lastStep -> id == $this -> step -> id ) return true;
        return false;
        if ( $mission -> order == $this -> mission -> order ) {
            return 1;
        } else if ( $mission -> order > $this -> mission -> order ) {
            return false;
        } else {
            return true;
        }
    }
    public function canMission ( $missionId ) {
        $mission = \App\Mission::whereId( $missionId ) -> first();
        if ( ! $this -> paid ) return false;
        if ( $mission -> order == $this -> mission -> order ) {
            return 1;
        } else if ( $mission -> order > $this -> mission -> order ) {
            return false;
        } else {
            return true;
        }
    }
    public function canSection ( $sectionId ) {
        $section = \App\Section::whereId( $sectionId ) -> first();
        if ( ! $this -> paid ) return false;
        if ( $this -> section -> absolute_order > $section -> absolute_order ) {
            return true;
        } else if ( $section -> absolute_order == $this -> section -> absolute_order ) {
            return 1;
        } else {
            return false;
        }
    }
    public function canStep ( $stepId ) {
        $step = \App\Step::find( $stepId );
        if ( ! $this -> paid ) return false;
        if ( $this -> step -> absolute_order > $step -> absolute_order ) {
            return true;
        } else if ( $step -> absolute_order == $this -> step -> absolute_order ) {
            return 1;
        } else {
            return false;
        }
    }

    public function getPaidAttribute () {
        return true;
    }
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
