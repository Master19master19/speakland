<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;

class Currency {
	protected $currencyURL = "";
	protected $currencyURLPlanA = "https://api.exchangeratesapi.io/latest";
	protected $currencyBackUpURL = "https://api.ratesapi.io/api/latest";
	protected $currencyURLPlanC = "http://data.fixer.io/api/latest?access_key=d2763633e44c4a265eff34ac6533f190";
	protected $customEntityURL = 'https://online.moysklad.ru/api/remap/1.1/entity/customentity/e9b12e75-be81-11e9-9ff4-34e80005e764/29b217fd-be83-11e9-9ff4-34e8000633aa';
	public $rate = null;
	public function __construct() {
		$this -> currencyURL = $this -> currencyURLPlanA;
	}
    protected function getCurrency () {
		$client = new \GuzzleHttp\Client();
		$request = $client->get( $this -> currencyURL );
		if ( $request->getStatusCode() == 200 ) {
			$resp = $request->getBody()->getContents();
			$response = json_decode( $resp );
			$this -> rate = $response -> rates -> CZK;
		}
    }
    protected function getCurrencyBackUp () {
		$this -> currencyURL = $this -> currencyBackUpURL;
    	$this -> getCurrency();
    }
    protected function getCurrencyPlanC () {
		$this -> currencyURL = $this -> currencyURLPlanC;
    	$this -> getCurrency();
    }
    public function setCurrency () {
    	$this -> getCurrency();
		if ( $this -> rate == null ) $this -> getCurrencyBackUp();
		if ( $this -> rate == null ) $this -> getCurrencyPlanC();
		$client = new \GuzzleHttp\Client();
		$date = date( "m-d H:i" );
		$this -> rate = str_replace( '.' , ',' , $this -> rate );
		$response = $client->put( $this -> customEntityURL , [
			'auth' => [
				env( 'MS_LOGIN' ),
				env( 'MS_PASS' )
			],
			\GuzzleHttp\RequestOptions::JSON => [
				'name' => "{$this -> rate}",
				'description' => "Helper for fresh CZK rate (last updated at: {$date})",
				'code' => "{$this -> rate}",
			],
		]);
		if ( $response->getStatusCode() == 200 ) {
			$res = $response->getBody()->getContents();
			$resp = json_decode( $res , 1 );
		}
    }
}