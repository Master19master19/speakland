<?php

namespace App\Helpers;
use Mailgun\Mailgun;
use App\Transaction;
use View;

class Emailer {
    public static function userPay ( $user ) {
        $data = [
            'email' => $user -> email,
            'password' => $user -> password_plain,
            'href' => env( "APP_URL" ) . "/login/{$user -> email}",
        ];
        $template = "mail.user_pay";
        $to = $user -> email;
        $res = self::sendMessage( $to , $data , $template );
        return $res;
    } 
    public static function userRegister ( $user ) {
        $data = [
            'email' => $user -> email,
            'phone' => $user -> phone,
            'href' => env( "APP_URL" ) . "/payment/{$user -> email}",
        ];
        $template = "mail.user_register";
        $to = $user -> email;
        $res = self::sendAdminUserRegister( $user );
        $res = self::sendMessage( $to , $data , $template );
        return $res;
    } 
    public static function sendAdminUserRegister ( $user ) {
        $message = "Телефон: " . $user -> phone . "\n";
        $message .= "Почта: " . $user -> email . "\n";
        $message .= "utm_medium: " . $user -> utm_medium . "\n";
        $message .= "utm_content: " . $user -> utm_content . "\n";
        $message .= "utm_campaign: " . $user -> utm_campaign . "\n";
        self::sendAdmin( $message , 'Новая регистрация' );
    }
    public static function sendMessage ( $to , $data , $template , $subject = 'Онлайн-платформа Speak Land' ) {
        $mg = Mailgun::create( env( 'MAILGUN_KEY' ) , 'https://api.eu.mailgun.net' );
        $res = false;
        $view = view( $template , [ 'data' => $data ]);
        $message = $view -> render();
        try {
            $res = $mg -> messages() -> send( env( 'MAILGUN_DOMAIN' ) , [
                'from'    => env( 'MAILGUN_FROM' ),
                'to'      => $to == null ? env( 'MAILGUN_TO' ) : $to ,
                'subject' => $subject ,
                'html'    => $message,
                // 'v:order_id' => (string) 30
            ]);
            // self::sendAdmin( $message , $subject );
        } catch( \Exception $e ) {
            return false;
        }
        return $res;
    }
    public static function notifyFat ( $email ) {
        sleep( 2 );
        $view = view( 'email' );
        $message = $view -> render();
        return self::send( $message , "Metabolism.com" , $email );
        return $message;
    }

    protected static function send ( $message , $subject = 'cashalot.exchange' , $to = null ) {
        // $message = self::generateMessage( $contact );
        $mg = Mailgun::create( env( 'MAILGUN_KEY' ) , 'https://api.eu.mailgun.net' );
        $res = false;
        try {
            $res = $mg -> messages() -> send( env( 'MAILGUN_DOMAIN' ) , [
                'from'    => env( 'MAILGUN_FROM' ),
                'to'      => $to == null ? env( 'MAILGUN_TO' ) : $to ,
                'subject' => $subject ,
                'html'    => $message,
                // 'v:order_id' => (string) 30
            ]);
            self::sendAdmin( $message , $subject );
        } catch( \Exception $e ) {
            return false;
        }
        return $res;
    }
    protected static function sendAdmin ( $message , $subject = 'cashalot.exchange' ) {
        $mg = Mailgun::create( env( 'MAILGUN_KEY' ) , 'https://api.eu.mailgun.net' );
        $res = false;
        try {
            $res = $mg -> messages() -> send( env( 'MAILGUN_DOMAIN' ) , [
                'from'    => env( 'MAILGUN_FROM' ),
                'to'      => env( 'MAILGUN_TO' ),
                'subject' => $subject ,
                'html'    => $message,
            ]);
        } catch( \Exception $e ) {
            return false;
        }
        return $res;
    }
}