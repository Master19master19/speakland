<?php

namespace App\Helpers;

use App\User;
use App\Mission;
use App\Section;
use App\Step;
use Auth;

class OtherHelper {
    public static function moveUserToNextMission () {
        $user = User::whereId( Auth::user() -> id ) -> first();
        $nextMission = Mission::where( 'order' , '>' , $user -> mission -> order ) -> has( 'sections' ) -> orderBy( 'order' , 'ASC' ) -> first();
        $section = Section::where( 'mission_id' , $nextMission -> id ) -> has( 'steps' ) -> orderBy( 'order' , 'ASC' ) -> first();
        $step = Step::where( 'section_id' , $section -> id ) -> orderBy( 'order' , 'ASC' ) -> first();
        $user -> mission_id = $nextMission -> id;
        $user -> section_id = $section -> id;
        $user -> step_id = $step -> id;
        $user -> save();
        return $step;
    }
    public static function quizMoveUserToNextMission ( $mission ) {
        $user = User::whereId( Auth::user() -> id ) -> first();
        $nextMission = Mission::where( 'order' , '>' , $mission -> order ) -> has( 'sections' ) -> orderBy( 'order' , 'ASC' ) -> first();
        $section = Section::where( 'mission_id' , $nextMission -> id ) -> has( 'steps' ) -> orderBy( 'order' , 'ASC' ) -> first();
        // $mission = ;
        // var_dump($mission -> order == Auth::user() -> mission -> order);
        // exit;
        if ( $mission -> order == Auth::user() -> mission -> order ) {   // first time passed quiz
            $step = Step::where( 'section_id' , $section -> id ) -> orderBy( 'order' , 'ASC' ) -> first();
            $user -> mission_id = $nextMission -> id;
            $user -> section_id = $section -> id;
            $user -> step_id = $step -> id;
            $user -> save();
        } else {   // repeating quiz
            return $nextMission;
        }
        return $nextMission;
    }
    public static function getNextStep ( $stepId , $pagination = true ) {
        $step = Step::whereId( $stepId ) -> first();
        if ( null == $step ) {
            return null;
        }
        $nextStep = Step::where( 'absolute_mission' , '>' , $step -> absolute_mission ) -> whereMissionId( $step -> mission -> id ) -> orderBy( 'absolute_order' , 'ASC' ) -> first();
        if ( null == $nextStep ) { // quiz
            // if ( $pagination ) {
            //     return [ 'type' => 'none' , 'value' => [] ];
            // }
            // $step = self::moveUserToNextMission( $pagination );
            // return [ 'type' => 'step' , 'value' => $step ];
            $mission = Mission::whereId( $step -> mission_id ) -> first();
            $quiz = $step -> mission -> quiz;
            // dd( Auth::user() -> canQuiz( $quiz -> id ) );
            // dd($quiz);
            return [ 'type' => 'quiz' , 'value' => $quiz ];
        } else {
            return [ 'type' => 'step' , 'value' => $nextStep ];
        }
    }

    public static function getMissionLastStep ( $missionId ) {
        $step = Step::whereMissionId( $missionId ) -> orderBy( 'absolute_order' , 'DESC' ) -> first();
        return $step;
    }

    public static function getPrevStep ( $stepId ) {
        $step = Step::whereId( $stepId ) -> first();
        if ( null == $step ) {
            return [ 'type' => 'none' , 'value' => [] ];
        }
        $prevStep = Step::where( 'absolute_mission' , '<' , $step -> absolute_mission ) -> whereMissionId( $step -> mission -> id ) -> orderBy( 'absolute_order' , 'DESC' ) -> first();
        if ( null == $prevStep ) { // quiz
            $mission = Mission::where( 'order' , '<' , $step -> mission -> id ) -> orderBy( 'order' , 'DESC' ) -> first();
            $prevStep = Step::whereMissionId( $mission -> id ) -> orderBy( 'absolute_order' , 'DESC' ) -> first();
            if ( null == $prevStep ) {
                return [ 'type' => 'none' , 'value' => [] ];
            } else {
                return [ 'type' => 'step' , 'value' => $prevStep ];
            }
        } else {
            return [ 'type' => 'step' , 'value' => $prevStep ];
        }
    }

    public static function createUser ( $data , $login = false ) {
        $oldUser = User::where([ 'email' => $data[ 'email' ] ]) -> first();
        if ( null !== $oldUser ) {
            return $oldUser;
        } else {
            $oldUser = User::where([ 'phone' => $data[ 'phone' ] ]) -> first();
            if ( null !== $oldUser ) {
                return $oldUser;
            }
        }
        $pass = uniqid();
        $data[ 'password_plain' ] = $pass;
        $data[ 'duration' ] = 0;
        $data[ 'password' ] = bcrypt( $pass );
        $mission = Mission::has( 'sections' ) -> has( 'steps' ) -> orderBy( 'order' , 'ASC' ) -> first();
        $section = Section::has( 'steps' ) -> whereMissionId( $mission -> id ) -> orderBy( 'order' , 'ASC' ) -> first();
        $step = Step::whereSectionId( $section -> id ) -> orderBy( 'order' , 'ASC' ) -> first();
        $data[ 'step_id' ] = $step -> id;
        $data[ 'section_id' ] = $section -> id;
        $data[ 'mission_id' ] = $mission -> id;
        $user = User::create( $data );
        if ( $login ) {
            $res = Auth::attempt([
                'email' => $data[ 'email' ],
                'password' => $pass
            ]);
        }
        return $user;
    }
}