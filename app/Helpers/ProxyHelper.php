<?php


namespace App\Helpers;
use App\Proxy;


class ProxyHelper {
    private static function filter ( $data ) {
        $data = str_replace( '\\r' , '' , $data );
        $data = str_replace( '\n' , '' , $data );
        $data = trim( $data );
        $data = preg_replace( "/^[a-z]+$/i" , '' , $data );
        return $data;
    }
    public static function populate ( $data ) {
        if ( count( $data ) ) {
            foreach ( $data as $key => $proxy ) {
                $proxy = self::filter( $proxy );
                if ( strlen( $proxy ) < 5 ) continue;
                $check = Proxy::whereIp( $proxy ) -> count();
                if ( ! $check ) {
                    Proxy::create([ 'ip' => $proxy ]);
                }
            }
            return true;
        }
        return false;
    }
}