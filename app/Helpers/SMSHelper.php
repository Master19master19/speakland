<?php

namespace App\Helpers;


class SMSHelper {
    public static function send ( $code , $user ) {
        // file_put_contents( __DIR__ . '/log.log' , $str . PHP_EOL , FILE_APPEND );
        // return true;
        $code = 'Код авторизации: ' . $code;
        $key = env( 'TELEGRAM_BOT_KEY' );
        $chat_id = env( 'TELEGRAM_CHAT_ID' );
        $message = urlencode( $code );
        $finalStr = "https://api.telegram.org/bot{$key}/sendMessage?chat_id={$chat_id}&parse_mode=html&text=" . $message;
        file_put_contents(__DIR__ . '/ss.err.log' ,$finalStr);
        try {
            $res = file_get_contents( $finalStr );
        } catch ( \Exception $e ) {
            // exit(0)
            file_put_contents( __DIR__ . '/log.err.log' , $e -> getMessage() . PHP_EOL , FILE_APPEND );
            // dd($e);
        }
    }
}