<?php
namespace App\Helpers;
use App\Exchange;
use App\Currency;


class ExchangeHelper {
    public static function populate () {
        $currencies = Currency::all();
        foreach ( $currencies as $key => $currency ) {
            $code = $currency -> code_bestchange;
            foreach ( $currencies as $k => $cur ) {
                if ( $k != $key ) {
                    $data_0 = [
                        'to' => $code,
                        'from' => $cur -> code_bestchange
                    ];
                    $data_1 = [
                        'from' => $code,
                        'to' => $cur -> code_bestchange
                    ];
                    $check_0 = Exchange::where( $data_0 ) -> count();
                    if ( ! $check_0 ) Exchange::create( $data_0 );
                    $check_1 = Exchange::where( $data_1 ) -> count();
                    if ( ! $check_1 ) Exchange::create( $data_1 );
                }
            }
        }
    }
    public static function depopulate ( $id ) {
        $currency = Currency::findOrFail( $id );
        $id = $currency -> code_bestchange;
        Exchange::whereFrom( $id ) -> delete();
        Exchange::whereTo( $id ) -> delete();
    }
    public static function update ( $data ) {
        foreach ( $data as $key => $val ) {
            $expl = explode( '--' , $key );
            if ( count( $expl ) == 2 ) {
                $data = [
                    'from' => $expl[ 0 ],
                    'to' => $expl[ 1 ],
                ];
                Exchange::where( $data ) -> update([ 'price' => env( 'COIN_DECIMAL' ) / $val , 'parse' => 0 ]);
            } else if ( count( $expl ) == 3 ) {
                $data = [
                    'from' => $expl[ 1 ],
                    'to' => $expl[ 2 ],
                ];
                Exchange::where( $data ) -> update([ 'parse' => 1 ]);
            }
        }
    }
}