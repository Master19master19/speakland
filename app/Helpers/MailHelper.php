<?php

namespace App\Helpers;
use Mailgun\Mailgun;
use App\User;

class MailHelper {
	public static function sendForgot ( $email = null ) {
		$code = rand( 1000 , 9999 );
		$user = User::whereEmail( $email ) -> firstOrFail();
		$user -> forgot_code = $code;
		$user -> forgot_token = md5( $code );
		$user -> save();
		$res = \Mail::send( 'mail.forgot' , [ 'code' => $code ] , function( $message ) use ( $email ) {
			$message -> to( $email );
			$message -> subject( __( 'Password recovery' ) );
		});
		return $res;
	}
	public static function sendMail ( $title , $desc , $to ) {
		$data = [
			'title' => $title ,
			'desc' => $desc
		];
		$res = \Mail::send( 'mail.send' , $data , function( $message ) use ( $to ) {
			$message -> to( $to );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendToAdmin ( $data ) {
		$title = "Новая оплата";
		$data = [
			'title' => $title ,
			'data' => $data
		];
		// $message = view( 'mail.admin' ,  $data );
		// $message = $message -> render();
		// exit($message);
		$res = \Mail::send( 'mail.admin' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendFeedBackToAdmin ( $title , $info ) {
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];

		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'email' => $info[ 'email' ],
			'phone' => $info[ 'phone' ],
			'message' => $info[ 'message' ],
		];
		$res = \Mail::send( 'mail.feedback' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendpersonalChildTrainingOrderToAdmin ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'child_name' => $info[ 'child_name' ],
			'phone_number' => $info[ 'phone_number' ],
			'parent_name' => $info[ 'parent_name' ],
			'child_birthday' => $info[ 'child_birthday' ],
			'additional' => $info[ 'additional' ],
			'level' => $info[ 'level' ],
		];
		$res = \Mail::send( 'mail.personalTrainingChild' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendpersonalTrainingOrder ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'phone' => $info[ 'phone' ],
			'email' => $info[ 'email' ],
			'additional' => $info[ 'additional' ],
		];
		$res = \Mail::send( 'mail.personalTraining' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			// var_dump($emails);
			// exit;
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
}