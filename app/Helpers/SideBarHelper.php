<?php

namespace App\Helpers;

use App\Mission;
use App\Section;
use App\Step;
use App\Quiz;

class SideBarHelper {
    public static function getInfo ( $missionId , $section = null ) {
        $mission = Mission::whereId( $missionId ) -> first();
        $sections = Section::with( 'steps' ) -> orderBy( 'order' ) -> where( 'mission_id' , $missionId ) -> get();
        $quiz = Quiz::whereMissionId( $missionId ) -> first();
		$data = [ 'mission' => $mission , 'sections' => $sections , 'quiz' => $quiz ];
        if ( $section !== null ) {
        	$data[ 'section' ] = $section;
        }
        return $data;
    }
}