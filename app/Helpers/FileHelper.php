<?php
namespace App\Helpers;

class FileHelper {
    public static function upload ( $file , $location , $type = '') {
        $filename = $file -> getClientOriginalName();
        $extension = $file -> getClientOriginalExtension();
        $tempPath = $file -> getRealPath();
        $fileSize = $file -> getSize();
        $maxFileSize = 140971520000000000000;
        $check = file_exists( $location . '/' . $filename );
        if (
            // $type !== 'step_video'
            $extension !== 'mp4'
        ) {
            if ( $check ) $filename = time() . "_" . $filename;
        }
        if ( $fileSize <= $maxFileSize ) {
            $res = $file -> move( $location , $filename );
            $path = $res -> getPathname();
            $title = $res -> getFilename();
            return compact( 'title' , 'path' );
        }
        return false;
    }
    public static function uploadProxies ( $file ) {
        $tempPath = $file -> getRealPath();
        $res = explode( PHP_EOL , file_get_contents( $tempPath ) );
        return $res;
    }
}