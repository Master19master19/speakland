<?php

namespace App\Helpers;

use App\User;
use App\Mission;
use App\Section;
use App\Step;
use Auth;

class OrderHelper {
    public static function orderAll () {
        self::orderSectionsAndSteps();
        self::orderStepsMissions();
        return true;
    }
    public static function orderSectionsAndSteps() {
        $absoluteOrder = 1;
        $absoluteOrderStep = 1;
        $sectionCount = Section::has( 'steps' ) -> count();
        $missions = Mission::has( 'sections' ) -> orderBy( 'order' , 'ASC' ) -> get();
        foreach ( $missions as $key => $mission ) {
            $sections = Section::whereMissionId( $mission -> id ) -> has( 'steps' ) -> orderBy( 'order' , 'ASC' ) -> get();
            foreach ( $sections as $section ) {
                $steps = Step::whereSectionId( $section -> id ) -> orderBy( 'order' , 'ASC' ) -> get();
                foreach ( $steps as $step ) {
                    $step -> absolute_order = $absoluteOrderStep;
                    $absoluteOrderStep++;
                    $step -> save();
                }
                $section -> absolute_order = $absoluteOrder;
                $absoluteOrder++;
                $section -> save();
            }
        }
    }
    public static function orderStepsMissions() {
        $missions = Mission::has( 'sections' ) -> orderBy( 'order' , 'ASC' ) -> get();
        foreach ( $missions as $key => $mission ) {
            $sections = Section::whereMissionId( $mission -> id ) -> has( 'steps' ) -> orderBy( 'order' , 'ASC' ) -> get();
            $absoluteOrder = 1;
            foreach ( $sections as $section ) {
                $steps = Step::whereSectionId( $section -> id ) -> orderBy( 'order' , 'ASC' ) -> get();
                foreach ( $steps as $step ) {
                    $step -> absolute_mission = $absoluteOrder;
                    $absoluteOrder++;
                    $step -> save();
                }
            }
        }
    }
}