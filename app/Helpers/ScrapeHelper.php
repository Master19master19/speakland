<?php
namespace App\Helpers;
use App\Exchange;
use App\Currency;
use App\Proxy;
use App\Setting;
use App\Helpers\CharConvertHelper;
use App\Helpers\LogHelper;


class ScrapeHelper {
    private static $my_static = 'foo';
    private static function getProxy () {
        $proxy = Proxy::orderBy( 'updated_at' , 'ASC' ) -> where( 'failed' , '<' , 10 ) -> first();
        if ( null == $proxy ) {
            $proxy = new \stdClass;
            $proxy -> ip = '';
            $proxy -> localOnly = true;
        }
        return $proxy;
    }
    private static function clearTooManyFailed () {
        $proxy = Proxy::where( 'failed' , '>' , '10' ) -> delete();
    }
    private static function failProxy ( $proxy ) {
        if ( ! $proxy -> localOnly ) {
            $proxy -> increment( 'failed' );
        }
    }
    private static function successProxy ( $proxy) {
        if ( ! $proxy -> localOnly ) {
            $proxy -> increment( 'used' );
        }
    }
    private static function downloadRemoteFile ( $url , $ip , $port , $user , $password ) {
        $temp_filename = env( 'PARSE_TEMP_PATH' );
        unlink( $temp_filename );
        $fp = fopen ( $temp_filename , "w+" );
        if ( $ip == '' ) {
            $ip = '"локальный"';
            $res = file_get_contents ( $url );
        } else {
            $proxy = "tcp://" . $ip . ":" . $port -> value;
            $proxyauth = $user -> value . ":" . $password -> value;
            $auth = base64_encode( $proxyauth );
            $aContext = [
                'http' => [
                    'proxy'           => $proxy ,
                    'request_fulluri' => true ,
                    'header' => "Proxy-Authorization: Basic $auth"
                ],
            ];
            $cxContext = stream_context_create( $aContext );
            $res = file_get_contents( $url , false, $cxContext );
            // $ch = curl_init();
            // curl_setopt ( $ch , CURLOPT_URL , $url );
            // curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , 0 );
            // curl_setopt( $ch , CURLOPT_SSL_VERIFYHOST , 0 );
            // curl_setopt( $ch , CURLOPT_BINARYTRANSFER , 1 );
            // curl_setopt( $ch , CURLOPT_PROXY , $proxy );
            // curl_setopt( $ch , CURLOPT_PROXYUSERPWD , $proxyauth );
            // curl_setopt( $ch , CURLOPT_FOLLOWLOCATION , 1 );
            // curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT , 10 );
            // // curl_setopt( $ch , CURLOPT_FILE , $fp );
            // curl_setopt( $ch , CURLOPT_TIMEOUT , 10 );
            // curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
            // curl_setopt( $ch , CURLOPT_HEADER , 1 );
            // $raw_file_data = curl_exec( $ch );
            // dd($raw_file_data);
            // file_put_contents( $temp_filename , $raw_file_data );
            // if ( curl_errno( $ch ) ){
            //     LogHelper::log( 'Проблема скачивания файла с помощью ip ' . $ip . ' Error: ' . curl_error( $ch ) );
            //     return false;
            // }
            // curl_close( $ch );
        }
        fputs ( $fp , $res );
        fclose ( $fp );
        $size = filesize( $temp_filename );
        if ( $size < 10 ) {
            LogHelper::log( 'Проблема скачивания файла с помощью ip ' . $ip );
            return false;
        }
        return true;
    }
    protected static function getDataFromSavedFile () {
        $temp_filename = env( 'PARSE_TEMP_PATH' );
        $size = filesize( $temp_filename );
        $zip = new \ZipArchive;
        if ( ! $zip -> open( $temp_filename ) ) {
            LogHelper::log( 'Проблема открытия файла ' . $temp_filename );
            exit( "Error opening file" );
        }
        $rates = array();
        foreach ( explode( "\n" , $zip -> getFromName( "bm_rates.dat" ) ) as $value ) {
            $entry = explode( ";" , $value );
            $rates[ $entry[ 0 ] ][ $entry[ 1 ] ][ $entry[ 2 ] ] = [
                "rate" => $entry[ 3 ] / $entry[ 4 ],
                "reserve" => $entry[ 5 ] ,
                "reviews" => str_replace( "." , "/" , $entry[ 6 ] )
            ];
        }
        $zip -> close();
        return $rates;
  }
    protected static function parse () {
        $rates = self::getDataFromSavedFile ();
        if ( ! count( $rates ) ) {
            LogHelper::log( 'Проблема с файлом ' . $temp_filename . ' пустой массив' );
            exit( "Error with file file" );
        }
        $exchanges = Exchange::all();
        foreach ( $exchanges as $key => $value ) {
            $cur = $rates[ $value -> from ][ $value -> to ];
            uasort( $cur , function ( $a , $b ) {
                if ( $a[ "rate" ] > $b[ "rate" ] ) return 1;
                if ( $a[ "rate" ] < $b[ "rate" ] ) return -1;
                return ( 0 );
            });
            $unKey = array_values( $cur );
            $finalRate = ( $unKey[ 0 ][ 'rate' ] + $unKey[ 1 ][ 'rate' ] + $unKey[ 2 ][ 'rate' ] ) / 3;
            if ( $value -> parse ) {
                $value -> price = $finalRate  * env( 'COIN_DECIMAL' );
                $value -> save();
            }
        }
        return 'ok';
    }
    public static function scrape ( $url ) {
        // return self::parse();
        // return;
        // self::clearTooManyFailed();
        $proxy = self::getProxy();
        $port = Setting::whereCode( 'proxy_port' ) -> first();
        $password = Setting::whereCode( 'proxy_password' ) -> first();
        $login = Setting::whereCode( 'proxy_login' ) -> first();
        $res = self::downloadRemoteFile( $url , $proxy -> ip , $port , $login , $password );
        if ( ! $res ) {
            self::failProxy( $proxy );
        } else {
            self::successProxy( $proxy );
            return self::parse();
        }
    }
}