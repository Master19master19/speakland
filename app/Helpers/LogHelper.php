<?php

namespace App\Helpers;


class LogHelper {
    public static function log ( $str ) {
        file_put_contents( __DIR__ . '/log.log' , $str . PHP_EOL , FILE_APPEND );
        return true;
        $key = env( 'TELEGRAM_BOT_KEY' );
        $chat_id = env( 'TELEGRAM_CHAT_ID' );
        $message = urlencode( $str );
        $finalStr = "https://api.telegram.org/bot{$key}/sendMessage?chat_id={$chat_id}&parse_mode=html&text=" . $message;
        try {
            $res = file_get_contents( $finalStr );
        } catch ( \Exception $e ) {
            // exit(0)
            file_put_contents( __DIR__ . '/log.err.log' , $e -> getMessage() . PHP_EOL , FILE_APPEND );
            // dd($e);
        }
    }
}