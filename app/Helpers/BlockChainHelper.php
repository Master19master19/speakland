<?php
namespace App\Helpers;
use App\Helpers\LogHelper;
use App\Setting;


class BlockChainHelper {
    protected $xPub = null;
    protected $key = null;
    protected $callback = null;
    protected $bitcoin_wallet = null;
    protected $secret = null;
    public function __construct () {
    	$this -> bitcoin_wallet = Setting::whereCode( 'bitcoin_address_api' ) -> first() -> value;
        $this -> bitcoin_wallet_settings = Setting::whereCode( 'bitcoin_wallet' ) -> first() -> value;
        $this -> xPub = env( 'XPUB' );
        $this -> key = env( 'BLOCKCHAIN_KEY' );
        $this -> callback = urlencode( env( 'BLOCKCHAIN_CALLBACK' ) );
    }
    public function createAddress () {
        $this -> secret =  time() . uniqid();
        return [ $this -> bitcoin_wallet_settings , $this -> secret ];
    }

    public function createAddressOld () {
        $this -> secret =  time() . uniqid();
        $url = "https://blockchainapi.org/api/receive?method=create&address={$this->bitcoin_wallet}&callback={$this->callback}?secret={$this -> secret}";
        try {
            $res = file_get_contents( $url );
        } catch ( \Exception $e ) {
            // exit($url);
            LogHelper::log( $e -> getMessage() );
            exit( 'Техническая проблема' );
        }
        $js = json_decode( $res );
        // dd($res,$js);
        // $res = $this -> spyAddress( $js -> address );
        return [ $js -> input_address , $this -> secret ];
    }


    protected function createAddressOlder () {
        $url = "https://api.blockchain.info/v2/receive?xpub={$this->xPub}&callback={$this->callback}&key={$this->key}";
        try {
            $res = file_get_contents( $url );
        } catch ( \Exception $e ) {
            // exit($url);
            LogHelper::log( $e -> getMessage() );
            exit( 'Техническая проблема' );
        }
        $js = json_decode( $res );
        $res = $this -> spyAddress( $js -> address );
        return $js -> address;
    }
    protected function spyAddress ( $addr ) {
        $url = "https://api.blockchain.info/v2/receive/balance_update";
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client -> post(
                $url,
                array(
                    'json' => array(
                        'key' => $this -> key,
                        'addr' => $addr ,
                        'confs' => '3',
                        'op' => 'RECEIVE',
                        'onNotification' => 'KEEP',
                        'callback' => env( 'BLOCKCHAIN_RECEIVE_CALLBACK' )
                    )
                )
            );
            if ( strlen( json_decode( $res -> getBody() -> getContents() ) -> id ) > 2 ) {
                return true;
            } else {
                return false;
            }
        } catch ( \Exception $e ) {
            // dd($e);
            LogHelper::log( $e -> getMessage() );
            exit( 'Техническая проблема' );
        }
    }
}