<?php

namespace App\Helpers;

use App\Mission;
use App\Section;
use App\Safe;
use App\Step;
use Auth;

class SafeHelper {
    public static function getInfo () {
    	$user = Auth::user();
    	if ( $user == null ) {
    		return [];
    	}
    	$response = [];
    	$safes = Safe::whereUserId( $user -> id ) -> get();
    	foreach ( $safes as $key => $value ) {
    		$type = $value -> type;
    		if ( $type == 'section' ) {
    			$response[ 'sections' ][ $value -> section_id ] = Section::with( 'steps' ) -> orderBy( 'order' ) -> where( 'id' , $value -> section_id ) -> first() -> toArray();
    		} else if ( $type == 'step' ) {
    			// dd($response);
    			$step = Step::where( 'id' , $value -> step_id ) -> first() -> toArray();
    			$section = Section::whereId( $step[ 'section_id' ] ) -> first() -> toArray();
    			// dd(isset( $response[ 'sections' ][ $section[ 'id' ] ] ));
    			if ( isset( $response[ 'sections' ][ $section[ 'id' ] ] ) ) {
					$response[ 'sections' ][ $section[ 'id' ] ][ 'steps' ][] = $step;
    			} else {
    				$section[ 'steps' ][] = $step;
    				$response[ 'sections' ][ $section[ 'id' ] ] = $section;
    			}
    		}
    	}
    	// dd($response);
    	return $response;
        $mission = Mission::whereId( $missionId ) -> first();
        return [ 'mission' => $mission , 'sections' => $sections ];
    }
}