<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $guarded = [];
	public function quiz () {
		return $this -> hasOne( 'App\Quiz', 'id' , 'quiz_id' );
	}
	public function answers () {
		return $this -> hasMany( 'App\Answer', 'question_id' , 'id' ) -> orderBy( 'order' , 'ASC' );
	}
	public function getDurationAttribute () {
		return gmdate( "i:s" , $this -> duration_seconds );
	}
	public function scopeExclude($query, $value = array()) 
{
  return $query->select( array_diff( $this->columns(), $value) );
}

}
