<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Step extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:150',
            'transcription' => 'nullable|string|min:1|max:20',
            'description' => 'nullable|string|min:10|max:200',
            'order' => 'required|integer',
            'duration_seconds' => 'required|integer|min:1',
            'pass_price' => 'required|integer|min:1|max:10000',
            'section_id' => 'required|exists:sections,id',
            'img' => 'nullable|file|mimes:jpeg,bmp,png,svg',
            'pdf' => 'nullable|file|mimes:pdf',
            'video' => 'nullable|file|mimes:mp4,mov,avi',
            'audio' => 'nullable|file|mimes:mpga,wav',
        ];
    }
}
