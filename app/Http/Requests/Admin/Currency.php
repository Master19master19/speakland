<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Currency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize () {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:20',
            'code' => 'required|string|min:2|max:5',
            'code_bestchange' => 'required|integer|exists:bestchange,id',
            'reserve' => 'required|numeric|min:0',
            'currency_group_id' => 'required|exists:currency_groups,id',
            'file' => 'required_without:id|file|mimes:jpeg,bmp,png,svg',
        ];
    }
}
