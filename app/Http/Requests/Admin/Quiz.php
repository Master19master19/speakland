<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Quiz extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:150',
            'description' => 'nullable|string|min:10|max:200',
            'duration_seconds' => 'required|integer|min:1',
            'min_score' => 'required|integer|min:1',
            'pass_price' => 'required|integer|min:1|max:10000',
            'mission_id' => 'required|exists:missions,id',
        ];
    }
}
