<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Section extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:50',
            'description' => 'nullable|string|min:10|max:200',
            'order' => 'required|integer',
            'pass_price' => 'required|integer|min:1|max:10000',
            'mission_id' => 'required|exists:missions,id',
            'file' => 'nullable|file|mimes:jpeg,bmp,png,svg',
        ];
    }
}
