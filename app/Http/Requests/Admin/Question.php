<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Question extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:150',
            'order' => 'required|integer',
            'description' => 'nullable|string|min:10|max:200',
            'duration_seconds' => 'required|integer|min:1',
            'quiz_id' => 'required|exists:quizzes,id',
        ];
    }
}
