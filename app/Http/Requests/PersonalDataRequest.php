<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function withValidator( $validator ) {
        $validator -> after( function ( $validator ) {
            \App::setLocale( 'en' );
            // if ( $validator -> fails() ) {
                // return redirect( '/auth#register' ) -> withInput() -> withErrors();
            // }
        });
    }


    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255','min:7'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'old_password' => [ 'nullable' , 'string', 'min:8'],
            'telegram' => [ 'nullable' , 'string', 'min:4'],
            'phone' => [ 'nullable' , 'string', 'min:8'],
            'credit_card' => [ 'nullable' , 'digits:16' ],
            'bitcoin_address' => [ 'nullable' , 'string' , 'min:8' , 'max:50' ],
            'ethereum_address' => [ 'nullable' , 'string' , 'min:8' , 'max:50' ],
            'skype' => [ 'nullable' , 'string' , 'min:5' , 'max:50' ],
            'password' => [ 'nullable' , 'string', 'min:8', 'confirmed' , 'required_with:old_password'],
        ];
    }
    public function messages()
    {
        return [
            'password.required_with' => __( 'Please input password' ),
        ];
    }
}
