<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function withValidator( $validator ) {
        // $validator->after(function ($validator) {
        //     if ( $validator -> fails() ) {
        //         return redirect( '/auth#register' ) -> withInput() -> withErrors();
        //     }
        // });
    }


    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255','min:7'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
