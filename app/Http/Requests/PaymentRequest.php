<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function withValidator( $validator ) {
        // $validator->after(function ($validator) {
        //     if ( $validator -> fails() ) {
        //         return redirect( '/auth#register' ) -> withInput() -> withErrors();
        //     }
        // });
    }


    public function rules()
    {
        return [
            // 'phone' => ['required', 'string', 'max:20','min:7', 'unique:users'],
            // 'email' => ['required', 'string', 'email', 'max:40', 'unique:users'],
            // 'phone_try' => ['required', 'string', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
    public function messages () {
        return [
            'phone.unique' => 'Этот номер телефона уже используется другим пользователем',
            'email.unique' => 'Эта почта уже используется другим пользователем',
            'email.max' => 'Введенная почта очень длинная',
            'phone.max' => 'Введенный номер телефона очень длинный',
            'phone.min' => 'Неверный номер телефона',
            // 'email' => ['required', 'string', 'email', 'max:40', 'unique:users'],
            // 'phone_try' => ['required', 'string', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
