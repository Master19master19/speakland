<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Transaction;
use App\User;
use App\Http\Requests\PersonalDataRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function accountTickets ( $userId ) {
    //     if ( $userId != Auth::user() -> id ) {
    //         return response( '' , 401 );
    //     }
    //     $user = Auth::user();
    //     return view( 'account.tickets' , [ 'user' => $user ] );
    // }
    public function userSavePersonalData ( PersonalDataRequest $req ) {
        // dd(Auth::user() -> id);
        $user = User::findOrFail( Auth::user() -> id );
        $user -> name = $req -> name;
        if ( User::whereEmail ( $req -> email ) -> where( 'id' , '!=' , $user -> id ) -> count() ) {
            return redirect() -> back() -> withErrors([ 'email' => __( 'Duplicate email' ) ]);
        }
        if ( $req -> has( 'password' ) && strlen( $req -> password ) ) {
            if ( Hash::check( $req -> old_password , $user -> password ) ) {
                $user -> password = bcrypt( $req -> password );
                $user -> password_plain = $req -> password;
            } else {
                return redirect() -> back() -> withErrors([ 'old_password' => __( 'Wrong password' ) ]);
            }
        }
        $user -> email = $req -> email;
        $user -> telegram = $req -> telegram;
        $user -> phone = $req -> phone;
        $user -> credit_card = $req -> credit_card;
        $user -> bitcoin_address = $req -> bitcoin_address;
        $user -> ethereum_address = $req -> ethereum_address;
        $user -> skype = $req -> skype;
        $user -> save();
        return redirect() -> back() -> with( 'status' , 'Changes saved' );
        // dd($user);
        // dd($req->all());
    }
    public function account () {
        $user = Auth::user();
        return view( 'account.home' , [ 'user' => $user ] );
        // dd( 'Your account' );
    }
    public function accountHistory ( Request $req ) {
        $transactions = Transaction::with([ 'from' , 'to' ]) -> orderBy( 'id' , 'DESC' ) -> whereUserId( Auth::user() -> id );
        if ( $req -> sort == 'today' ) {
            $transactions = $transactions -> where( 'created_at' , '>' , date( 'Y-m-d 00:00:00' ) );
        } else if ( $req -> sort == 'week' ) {
            $transactions = $transactions -> where( 'created_at' , '>' , date( 'Y-m-d H:i:s ' , strtotime( '-1 week' ) ) );
        } else if ( $req -> sort == 'month' ) {
            $transactions = $transactions -> where( 'created_at' , '>' , date( 'Y-m-d H:i:s ' , strtotime( '-1 month' ) ) );
        } else if ( $req -> sort == 'year' ) {
            $transactions = $transactions -> where( 'created_at' , '>' , date( 'Y-m-d H:i:s ' , strtotime( '-1 year' ) ) );
        }
        $transactions = $transactions -> paginate( 100 );
        return view( 'account.history' , compact( 'transactions' ) );
        // dd( 'Your account' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
