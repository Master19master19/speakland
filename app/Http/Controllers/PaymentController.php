<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;
use App\User;
use App\Payment;
use App\Helpers\Emailer;
use App\Helpers\OtherHelper;
use Auth;

class PaymentController extends Controller
{

    protected function createUser ( $data ) {
        return OtherHelper::createUser( $data );
    }

    protected function createPaymentBak ( $user ) {
        $data = [
            'user_id' => $user -> id,
            'amount' => env( 'PAYMENT_AMOUNT' ),
            'ip' => \Request::ip()
        ];
        $payment = Payment::create( $data );
        return $payment;
    }
    protected function createPayment ( $user ) {
        $data = [
            'user_id' => $user -> id,
            'amount' => env( 'PAYMENT_AMOUNT' ),
            'ip' => \Request::ip()
        ];
        $payment = Payment::create( $data );
        return $payment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showPaymentForm ( $user , $payment ) {
        $data = [
            'account' => env( 'PAYMENT_ACCOUNT' ),
            'domain' => env( 'PAYMENT_DOMAIN' ),
            'payment_id' => "payment_" . $payment -> id,
            'time' => time(),
            'amount' => $payment -> amount,
            'currency' => "USD",
            'product' => "Подписка на онлайн-платформу Speak Land",
            'count' => "1",
            'price' => $payment -> amount,
            'email' => $user -> email,
            'phone' => $user -> phone,
            'regularMode' => env( 'PAYMENT_FREQUENCY' ),
            'regularAmount' => $payment -> amount,
            'regularCount' => env( 'PAYMENT_RECURRING_COUNT' ),
            'regularOn' => 1,
            'dateNext' => date( 'd.m.Y' , strtotime( env( 'PAYMENT_FREQUENCY_DATE_STR' ) )  ),
            'returnUrl' => env( 'PAYMENT_RETURN_URL' )
        ];
        $time = time();
        $string = $data[ 'account' ] . ";" . $data[ 'domain' ] . ";" . $data[ 'payment_id' ] . ";" . $data[ 'time' ] . ";" . $data[ 'amount' ] . ";" . $data[ 'currency' ] . ";" . $data[ 'product' ] . ";" . $data[ 'count' ] . ";" . $data[ 'price' ];
        $key = env( 'PAYMENT_SECRET' );
        $data[ 'hash' ] = hash_hmac( "md5" , $string , $key );
        return view( 'payment_form' , [ 'data' => $data ] );
    }
    public function create_with_email ( Request $req ) {
        $email = $req -> email;
        $user = User::whereEmail( $email ) -> firstOrFail();
        $payment = $this -> createPayment( $user );
        return $this -> showPaymentForm( $user , $payment );
    }
    public function create ( PaymentRequest $req ) {
        $input = $req -> except( '_token' );
        $user = $this -> createUser( $input );
        Emailer::userRegister( $user );
        $payment = $this -> createPayment( $user );
        return $this -> showPaymentForm( $user , $payment );
        // return;
        // dd($user);
        // dd($input);
        $data = [
           "requestType" => "CREATE",
           "merchantAccount" => env( 'PAYMENT_ACCOUNT' ),
           "merchantPassword" => env( 'PAYMENT_SECRET' ),
           "regularMode" => "monthly",
           "amount" => $payment -> amount,
           "currency" => "USD",
           "dateBegin" => date( 'd.m.Y' , strtotime( '+1 month' )  ),
           "dateEnd" => date( 'd.m.Y' , strtotime( '+12 months' ) ),
           "orderReference" => "payment_10",
           // "orderReference" => "payment_" . $payment -> id,
           "email" => $req -> email
        ];
        // dd($data);
        $client = new \GuzzleHttp\Client();
        $response = $client -> post(
            'https://api.wayforpay.com/regularApi',
            array(
                'json' => $data
            )
        );
        echo $response->getBody();
        dd($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notify ( Request $req ) {
        $input = json_decode( $req -> getContent() , 1 );
        file_put_contents( __DIR__ . '/pay.log' , $req -> getContent() . PHP_EOL , FILE_APPEND );
        $orderReference = $input[ 'orderReference' ];
        $transactionStatus = $input[ 'transactionStatus' ];
        if ( $transactionStatus == 'Approved' ) {
            $xpl = explode( '_' , $orderReference );
            $payment = Payment::whereId( $xpl[ 1 ] ) -> firstOrFail();
            $payment -> paid = 1;
            $payment -> save();
            $user = User::whereId( $payment -> user_id ) -> firstOrFail();
            Emailer::userPay( $user );
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
