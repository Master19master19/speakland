<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Validator;
use App\User;
use Auth;
use App;
use App\Helpers\MailHelper;
use App\Helpers\SMSHelper;


class AuthController extends Controller
{
    public function logout () {
        Auth::logout();
        if ( App::getLocale() == 'en' ) {
            return redirect( '/en' );
        } else {
            return redirect( '/' );
        }
    }
    public function forgot () {
        return view( 'auth.forgot' );
    }
    public function forgotStatus () {
        return view( 'auth.forgot_status' );
    }
    public function passwordReset ( $token ) {
        return view( 'auth.password_reset' , [ 'forgot_token' => $token ]);
    }
    public function forgotPost ( Request $req ) {
        $validation = [
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users'],
        ];
        $validator = Validator::make( $req -> all() , $validation );
        if ( $validator -> fails() ) {
            if ( \App::getLocale() == 'en' ) {
                return redirect( '/en/forgot' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            } else {
                return redirect( '/forgot' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            }
        }
        MailHelper::sendForgot( $req -> email );
        if ( \App::getLocale() == 'en' ) {
            return redirect( '/en/forgot.status' ) -> with( 'status' , __( 'Check your email!' ) );
        } else {
            return redirect( '/forgot.status' ) -> with( 'status' , __( 'Check your email!' ) );
        }

    }
    public function forgotCheck ( Request $req ) {
        $validation = [
            'code' => [ 'required' , 'digits:4' , 'numeric' , 'exists:users,forgot_code'],
        ];
        $validator = Validator::make( $req -> all() , $validation );
        if ( $validator -> fails() ) {
            if ( \App::getLocale() == 'en' ) {
                return redirect( '/en/forgot.status' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            } else {
                return redirect( '/forgot.status' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            }
        }
        $user = User::whereForgotCode( $req -> code ) -> first();
        if ( \App::getLocale() == 'en' ) {
            return redirect( '/en/password.reset/' . $user -> forgot_token );
        } else {
            return redirect( '/password.reset/' . $user -> forgot_token );
        }

    }
    public function login ( Request $req ) {
        $validation = [
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users'],
            'password' => ['required', 'string', 'min:8'],
        ];
        $validator = Validator::make( $req -> all() , $validation );
        if ( $validator -> fails() ) {
            if ( \App::getLocale() == 'en' ) {
                return redirect( '/en/auth' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            } else {
                return redirect( '/auth' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            }
        }
        if ( \App::getLocale() == 'en' ) {
            if ( Auth::attempt( $req -> only( 'email' , 'password' ) ) ) {
                if ( Auth::user() -> type == 1 ) {
                    session([ 'memcache_key' => Auth::user() -> id ]);
                    $sms_code = rand( 111111 , 999999 );
                    SMSHelper::send( $sms_code , Auth::user() );
                    User::whereId( Auth::user() -> id ) -> update([ 'sms_code' => $sms_code ]);
                    Auth::logout();
                    return redirect( '/admin/sms' );
                }
                return redirect() -> intended( '/en/account' );
            }
            return redirect( '/en/auth' ) -> withErrors([ 'email' => __( 'Wrong credentials' ) ]) -> withInput( $req -> all() );
        } else {
            if ( Auth::attempt( $req -> only( 'email' , 'password' ) ) ) {
                if ( Auth::user() -> type == 1 ) {
                    session([ 'memcache_key' => Auth::user() -> id ]);
                    $sms_code = rand( 111111 , 999999 );
                    SMSHelper::send( $sms_code , Auth::user() );
                    User::whereId( Auth::user() -> id ) -> update([ 'sms_code' => $sms_code ]);
                    Auth::logout();
                    return redirect( '/admin/sms' );
                }
                return redirect() -> intended( '/account' );
            }
            return redirect( '/auth' ) -> withErrors([ 'email' => __( 'Wrong credentials' ) ]) -> withInput( $req -> all() );
        }
    }
    public function passwordResetPost ( Request $req ) {
        $validation = [
            'password' => ['required', 'string', 'min:8' , 'confirmed' ],
        ];
        $validator = Validator::make( $req -> all() , $validation );
        if ( $validator -> fails() ) {
            return redirect() -> back() -> withErrors( $validator ) -> withInput( $req -> all() );
        }
        $user = User::whereForgotToken( $req -> forgot_token ) -> firstOrFail();
        $user -> password = bcrypt( $req -> password );
        $user -> save();
        $data = [
            'email' => $user -> email,
            'password' => $req -> password,
        ];
        $res = Auth::attempt( $data );
        if ( \App::getLocale() == 'en' ) {
            return redirect() -> intended( '/en/account' ) -> with( [ 'status' => 'Successfully changed password!' ]);
        }
        return redirect() -> intended( '/account' ) -> with( [ 'status' => 'Successfully changed password!' ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct () {
        // $this -> middleware( 'guest' );
    }
    public function index () {
        return view( 'auth.auth' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerUser ( Request $req ) {
        $validation = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'password_confirmation' => ['required', 'same:password'],
        ];
        $validator = Validator::make( $req -> all() , $validation );
        // dd($validator ->errors());
        if ( $validator -> fails() ) {
            if ( \App::getLocale() == 'en' ) {
                return redirect( '/en/auth#register' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            } else {
                return redirect( '/auth#register' ) -> withErrors( $validator ) -> withInput( $req -> all() );
            }
        }
        $user = new User;
        $user -> name = $req -> name;
        $user -> password = bcrypt( $req -> password );
        $user -> password_plain = $req -> password;
        $user -> email = $req -> email;
        $user -> save();
        return redirect() -> back() -> withInput( $req -> all() ) -> with( 'status' , __( 'Successfully registered!' ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
