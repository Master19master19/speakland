<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exchange;
use App\Currency;

class XmlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exchanges = Exchange::with([ 'currency_from' , 'currency_to' ]) -> get();
        $res = '<?xml version="1.0" encoding="UTF-8"?>';
        $res .= '<rates>';
        foreach ( $exchanges as $ex ) {
            if ( $ex -> currency_from -> active_out ) {
                $res .= '
                    <item>
                    <from>' . $ex -> currency_from -> xml_code . '</from>
                    <to>' . $ex -> currency_to -> xml_code . '</to>
                    <in>1000</in>
                    <out>' . ( float ) round( env( 'COIN_DECIMAL' ) * 1000 / $ex -> price , env( 'COIN_FLOAT' ) ) . '</out>
                    <amount>' . $ex -> currency_to -> reserve . '</amount>
                    <minamount>' . $ex -> currency_from -> min . '</minamount>
                    <maxamount>' . $ex -> currency_from -> max. '</maxamount>
                    <param>manual , juridical</param>
                    </item>
                ';
            }
        }
        $res .= '</rates>';
        return response( $res , 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
