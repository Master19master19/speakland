<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exchange;
use App\Transaction;
use App\Currency;


class ApiController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactionsStatuses () {
        $transactions = Transaction::orderBy( 'id' , 'desc' ) -> take( 50 ) -> select([ 'status' , 'id' ]) -> get();
        return response() -> json( $transactions );
    }
    public function getRates( $from , $to , $amount , $direction ) {
        $exchange = Exchange::where([
            'from' => $from,
            'to' => $to
        ]) -> firstOrFail();
        $exchange -> price_initial = round( 1 / ( $exchange -> price / env( 'COIN_DECIMAL' ) ) , env( 'COIN_FLOAT' ) );
        if ( $exchange -> price_initial < 1 ) {
            $exchange -> price_initial_initial = round( ( $exchange -> price / env( 'COIN_DECIMAL' ) ) , env( 'COIN_FLOAT' ) );
        }
        // dd($exchange -> price);
        if ( $direction == 'ltr' ) {
            $exchange -> price =  round( $amount / ( $exchange -> price / env( 'COIN_DECIMAL' ) ) , env( 'COIN_FLOAT' ) );
        } else {
            $in = $exchange -> price * $amount;
            // dd($in);
            $exchange -> price = round( $in / env( 'COIN_DECIMAL' ) , env( 'COIN_FLOAT' ) );
            // $exchange -> price = $in / env( 'COIN_DECIMAL' );
            // $exchange -> price =  round( ( $exchange -> price ) / $amount / env( 'COIN_DECIMAL' ) , env( 'COIN_FLOAT' ) );
        }
        $limit = Currency::where( 'code_bestchange' , $from ) -> select([ 'min' , 'max' ]) -> get()[ 0 ];
        // $limits[ $to ] = Currency::where( 'code_bestchange' , $to ) -> select([ 'code_bestchange' , 'min_out' , 'max_out' ]) -> get();
        // dd($limits);
        $exchange -> limits = $limit;
        return response() -> json( $exchange );
        // exit('ok'.$from.$to);/
        //
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
