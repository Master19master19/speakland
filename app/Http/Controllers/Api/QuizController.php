<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Question;
use App\Quiz;
use App\Helpers\OtherHelper;
use App\UserAnswer;
use DB;
use Auth;

class QuizController extends Controller {
    public function getUser ( Request $req ) {
        return response() -> json( Auth::user() );
    }
    public function passed ( Request $req , $quizId , $result ) {
        $quiz = Quiz::whereId( $quizId ) -> first();
        $res = UserAnswer::create([
            'user_id' => Auth::user() -> id,
            'quiz_id' => $quizId,
            'result' => $result
        ]);
        $mission = OtherHelper::quizMoveUserToNextMission( $quiz -> mission );
        return response() -> json([ 'url' => env( 'APP_URL' ) . '/mission/' . ( $mission -> id ) ]);
    }
    public function getQuestions ( Request $req , $quizId ) {
        $questions = Question::where( 'quiz_id' , $quizId ) -> with( 'answers' ) -> has( 'answers' ) -> orderBy( 'order' , 'ASC' ) -> get();
        return response() -> json( $questions );
    }
    public function auth ( Request $req ) {
        // dd($req->all());
        if ( Auth::attempt([ 'email' => $req -> email , 'password' => $req -> password ]) ) { 
            $user = Auth::user();
            $token = $user -> createToken( 'MyApp' ) -> accessToken;
            return response() -> json( [ 'token' => $token ] , 200 ); 
        } else {
            return response() -> json( [ 'error' => __( 'Wrong credentials' ) ] , 422 );
        }
    }
    // public function lastMessages ( Request $req , $to ) {
    //     $response = DB::select( DB::raw( "SELECT DISTINCT `id` , `from` FROM `messages` WHERE `to` = $to ;" ) );
    //     return response() -> json( $response );
    // }
    public function messages ( Request $req , $chatId ) {
        if ( Auth::user() -> type == 1 ) {
            $response = DB::select( DB::raw( "SELECT * FROM `messages` WHERE ( `chat_id` = $chatId ) LIMIT 100 ;" ) );
        } else {
            $chat = Chat::whereFrom( Auth::user() -> id ) -> whereId( $chatId ) -> first();
            $response = [];
            if ( null !== $chat ) {
                $response = DB::select( DB::raw( "SELECT * FROM `messages` WHERE ( `chat_id` = $chatId ) ;" ) );
            }
        }
        return response() -> json( $response );
    }

    // public function userMessages ( Request $req ) {
    //     $chat = Chat::whereFrom( Auth::user() -> id ) -> first();
    //     $response = [];
    //     if ( null !== $chat ) {
    //         $response = DB::select( DB::raw( "SELECT * FROM `messages` WHERE `chat_id` = {$chat->id} ;" ) );
    //     }
    //     return response() -> json( $response );
    // }

    public function chats () {
        if ( Auth::user() -> type == 1 ) {
            $chats = Chat::with([ 'user' , 'lastMessage' ]) -> orderBy( 'closed' , 'ASC' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        } else {
            $chats = Chat::with([ 'user' , 'lastMessage' ]) -> whereFrom( Auth::user() -> id ) -> orderBy( 'closed' , 'ASC' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        }
        return response() -> json( $chats );
    }
    public function closeChat ( $chatId ) {
        if ( Auth::user() -> type == 1 ) {
            Chat::whereId( $chatId ) -> update([ 'closed' => 1 ]);
        } else {
            $chat = Chat::whereFrom( Auth::user() -> id ) -> whereId( $chatId ) -> first();
            if ( null !== $chat ) {
                Chat::whereId( $chatId ) -> update([ 'closed' => 1 ]);
            }
        }
        return response() -> json([] , 200);
    }
    public function createChat ( Request $req ) {
        $validation = [
            'title' => [ 'required' , 'string' , 'min:3' ],
        ];
        $validator = \Validator::make( $req -> all() , $validation );
        $chat = new Chat;
        $chat -> title = $req -> title;
        $chat -> from = Auth::user() -> id;
        $chat -> to = 100000;
        $chat -> save();
        return response() -> json([] , 200);
    }
    public function sendMessage ( Request $req , $chatId ) {
        if ( Auth::user() -> type == 1 ) {
            $chat = Chat::whereId( $chatId ) -> first();
        } else {
            $chat = Chat::whereId( $chatId ) -> whereFrom( Auth::user() -> id ) -> first();
            if ( null == $chat ) {
                return response( '' , 401 );
            }
        }
        if ( $chat -> closed ) return response( '' , 401 );
        // if ( null == $chat ) {
        //     $chat = new Chat;
        //     $chat -> from = $sender;
        //     $chat -> to = 100000;
        //     $chat -> save();
        //     $chatId = $chat -> id;
        // }
        $message = new Message;
        $message -> chat_id = $chatId;
        $message -> sender = Auth::user() -> id;
        $message -> message = $req -> message;
        $message -> save();
        return response() -> json( $message );
    }
}