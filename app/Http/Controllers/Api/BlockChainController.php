<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;

class BlockChainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blockChainReceiveCallback ( Request $req ) {
        file_put_contents( __DIR__ . '/receive' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        return response() -> json([]);
    }
    public function blockChainCallback ( Request $req ) {
        file_put_contents( __DIR__ . '/asasdada' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        if ( $req -> confirmations > 2 ) {
            $addr = $req -> address;
            $amount = $req -> value;
            $secret = $req -> secret;
            $transaction_hash = $req -> transaction_hash;
            $transaction = Transaction::where([ 'btc_address' => $addr , 'btc_secret' => $secret ]) -> firstOrFail();
            if ( abs( $transaction -> total_send - $amount ) < 100000 ) {
                $transaction -> status = 3;
                $transaction -> save();
            }
            exit( '*ok*' );
        }
        return response() -> json([]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
