<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Cookie;
use App\User;
use App\Mission;
use App\Section;
use App\Step;
use App\Quiz;
use App\Helpers\Emailer;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\SettingRequest;
use App\Helpers\SideBarHelper;
use App\Helpers\FileHelper;
use App\Helpers\SafeHelper;
use App\Helpers\OtherHelper;

class HomeController extends Controller
{
    public function payment_success ( Request $req ) {
        $email = $req -> email;
        $user = User::whereEmail( $email ) -> firstOrFail();
        $auth = Auth::attempt([
            'email' => $email,
            'password' => $user -> password_plain
        ]);
        return view( 'payment.success' , [ 'email' => $email ] );
    }
    public function payment_error ( Request $req ) {
        dd($req->all());
    }
    public function login_with_email ( Request $req ) {
        return view( 'auth.login' , [ 'email' => $req -> email ] );
    }
    public function testemail() {
        $user = User::whereEmail( 'vmxitaryan@yandex.ru' ) -> first();
        $res = Emailer::userPay( $user );
        dd($res);
    }
    public function fat() {
        $email = 'vmxitaryan@gmail.com';
        Emailer::notifyFat( $email );

    }
    public function save_settings ( SettingRequest $req ) {
        $inp = $req -> only([ 'name' , 'email' ]);
        $user = User::find( Auth::user() -> id );
        if ( $user -> email !== $req -> email ) {
            $check = User::whereEmail( $req -> email ) -> count();
            if ( $check ) {
                return redirect() -> back() -> withErrors([ 'email' => "Почта занята" ]);
            }
        }
        $user -> update( $inp );
        $user -> password = bcrypt( $req -> password );
        $user -> password_plain = $req -> password;
        if ( $req -> has( 'avatar' ) ) {
            $file = $req -> file( 'avatar' );
            $fileInfo = FileHelper::upload( $file , env( 'AVATAR_PATH' ) );
            $user -> avatar_url = env( "AVATAR_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
            // $record -> video_path = $fileInfo[ 'path' ];
        }
        $user -> save();
        return redirect() -> back() -> withStatus( 'Изменения сохранены!' );
        // return view( 'home.index' );
    }

    public function promotion_terms( Request $req ) {
        return view( 'static.promotion_terms' );
    }
    public function terms( Request $req ) {
        return view( 'static.terms' );
    }
    public function privacy( Request $req ) {
        return view( 'static.privacy' );
    }
    public function index( Request $req ) {
        if ( null != Cookie::get( 'auth' ) ) {
            // return redirect() -> route( 'login' );            
        }
        return view( 'landing' );
        return view( 'home.index' );
    }
    public function home( Request $req ) {
        // Cookie::set( 'auth' , '' );
        Cookie::queue( 'auth' , '1' , 2628000 );
        $missions = Mission::all();
        $data = [
            'missions' => $missions
        ];
        return view( 'account.index' , $data );
    }
    public function settings( Request $req ) {
        return view( 'account.settings' );
    }
    public function notifications( Request $req ) {
        return view( 'account.notifications' );
    }
    public function promotions( Request $req ) {
        return view( 'account.promotions' );
    }
    public function safe( Request $req ) {
        $safeInfo = SafeHelper::getInfo();
        $data = [
            'safeInfo' => $safeInfo
        ];
        return view( 'account.safe' , $data );
        return view( 'account.safe' );
    }
    public function mission ( Request $req , $missionId ) {
        $sideBarInfo = SideBarHelper::getInfo( $missionId );
        $mission = Mission::findOrFail( $missionId );
        // if ( ! Auth::user() -> canMission( $mission -> id ) ) return redirect( '/' );
        $sections = Section::where( 'mission_id' , $missionId ) -> get();
        $data = [
            'mission' => $mission,
            'sections' => $sections,
            'sideBarInfo' => $sideBarInfo
        ];
        return view( 'account.mission' , $data );
    }
    public function stepDone ( $id ) {
        $step = Step::find( $id );
        $user = User::whereId( Auth::user() -> id ) -> first();
        if ( $user -> step -> id != $step -> id ) return redirect( '/' );
        $nextStepInfo = OtherHelper::getNextStep( $step -> id , false );
        $nextStep = $nextStepInfo[ 'value' ];
        $nextType = $nextStepInfo[ 'type' ];
        $user -> balance = $user -> balance + $step -> pass_price;
        if ( $nextType == 'step' ) {
            $user -> step_id = $nextStep -> id;
            if ( $nextStep -> section -> id !== $user -> section -> id ) {
                $user -> section_id = $nextStep -> section -> id;
            }
            $user -> save();
            return redirect() -> route( 'account.step' , $nextStep -> id );
        } else if ( $nextType == 'quiz' ) {
            $user -> can_quiz = 1;
            $user -> save();
            return redirect() -> route( 'account.quiz' , $user -> mission -> quiz -> id );
        }
        exit( 'Problem with next step' );
    }

    public function quiz ( Request $req , $quizId ) {
        $quiz = Quiz::whereId( $quizId ) -> firstOrFail();
        if ( ! Auth::user() -> canQuiz( $quiz -> id ) ) {
            return redirect( '/' );
        } else {
            $sideBarInfo = SideBarHelper::getInfo( $quiz -> mission_id );
            $data = [
                'quiz' => $quiz,
                'sideBarInfo' => $sideBarInfo
            ];
            return view( 'account.quiz' , $data );
        }
    }

    public function stepProcess ( Request $req , $id ) {
        $step = Step::find( $id );
        $duration = $req -> duration / 1000;
        $user = User::whereId( Auth::user() -> id ) -> first();
        if ( $user -> step -> id != $id ) return response() -> json([ 'status' => 'fuck' ]); 
        $user -> duration = $duration;
        $user -> save();
        return response() -> json([ 'status' => 'ok' ]);
    }
    public function section ( Request $req , $sectionId ) {
        $section = Section::findOrFail( $sectionId );
        if ( ! Auth::user() -> canSection( $section -> id ) ) return redirect( '/' );
        $sideBarInfo = SideBarHelper::getInfo( $section -> mission_id , $section );
        $steps = Step::where( 'section_id' , $sectionId ) -> get();
        $data = [
            'section' => $section,
            'steps' => $steps,
            'sideBarInfo' => $sideBarInfo
        ];
        return view( 'account.section' , $data );
    }
    public function step ( Request $req , $stepId ) {
        $step = Step::findOrFail( $stepId );
        $section = Section::findOrFail( $step -> section_id );
        // dd($step,$section,Auth::user() -> canStep( $step -> id ) );
        if ( ! Auth::user() -> canStep( $step -> id ) ) return redirect( '/' );
        // if ( Auth::user() -> section -> order < $section -> order ) return redirect( '/' );
        $sideBarInfo = SideBarHelper::getInfo( $section -> mission_id , $section );
        $data = [
            'step' => $step,
            'sideBarInfo' => $sideBarInfo
        ];
        return view( 'account.step' , $data );
    }
    protected function createUser ( $req ) {
        $data = $req -> only( [ 'phone' , 'email' , 'name' ]);
        $pass = uniqid();
        $data[ 'password_plain' ] = $pass;
        $data[ 'password' ] = bcrypt( $pass );
        $user = User::create( $data );
        $res = Auth::attempt([
            'email' => $data[ 'email' ],
            'password' => $pass
        ]);
        // dd($res);
    }
    public function register ( RegisterRequest $req ) {
        $user = $this -> createUser( $req );
        return redirect() -> route( 'account.home' );
    }
    public function register_try ( RegisterRequest $req ) {
        $user = $this -> createUser( $req );
        return redirect() -> route( 'account.home' );
    }
    public function logout () {
        Auth::logout();
        return redirect( '/' );
    }
}
