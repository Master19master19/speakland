<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Safe;
use App\Step;

class SafeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save ( Request $req , $id , $type ) {
        $user_id = Auth::user() -> id;
        $data = [ 'user_id' => $user_id ];
        if ( $type == 'section' ) {
            $check = Safe::where([ 'user_id' => $user_id , 'section_id' => $id ]) -> count();
            if ( $check ) return redirect() -> back() -> withStatus( 'Успешно сохранено' );
            $data[ 'section_id' ] = $id;
            $data[ 'type' ] = 'section';
        } else if ( $type == 'step' ) {
            $check = Safe::where([ 'user_id' => $user_id , 'step_id' => $id ]) -> count();
            if ( $check ) return redirect() -> back() -> withStatus( 'Успешно сохранено' );
            $step = Step::whereId( $id ) -> firstOrFail();
            $check = Safe::where([ 'user_id' => $user_id , 'section_id' => $step -> section_id ]) -> count();
            if ( $check ) return redirect() -> back() -> withStatus( 'Успешно сохранено' );
            $data[ 'step_id' ] = $id;
            $data[ 'type' ] = 'step';
        } else if ( $type == 'mission' ) {
            $data[ 'mission_id' ] = $id;
            $data[ 'type' ] = 'mission';
        }
        Safe::create( $data );
        return redirect() -> back() -> withStatus( 'Успешно сохранено' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
