<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Transaction;
use App\Exchange;
use App\Rating;
use App\Helpers\BlockChainHelper;
use App\Helpers\Emailer;
use Illuminate\Support\Str;
use Auth;


class TransactionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel ( $uuid , Request $req ) {
        $transaction = Transaction::where( 'uuid' , $uuid ) -> firstOrFail();
        $transaction -> canceled = 1;
        $transaction -> save();
        // Transaction::where( 'uuid' , $uuid ) -> firstOrFail() -> update([ 'canceled' => 1 ]);
        Emailer::notifyTransaction( $transaction -> id );
        return redirect( '/' );
    }
    public function set_paid ( $uuid , Request $req ) {
        $transaction = Transaction::where( 'uuid' , $uuid ) -> firstOrFail();
        $transaction -> status = 1;
        $transaction -> save();
        Emailer::notifyTransaction( $transaction -> id );
        return redirect() -> back();
    }
    public function rating ( $transaction_id , Request $req ) {
        $rating = new Rating;
        $rating -> rating = $req -> rating;
        $rating -> message = $req -> message;
        $rating -> name = $req -> name;
        $rating -> transaction_id = $transaction_id;
        if ( null !== Auth::user() ) {
            $rating -> user_id = Auth::user() -> id;
        }
        $rating -> save();
        return redirect() -> back() -> withStatus( 'Спасибо за отзыв!' );
    }
    public function index()
    {
        //
    }
    protected function fatal ( ) {
        return back() -> withErrors([
            'Ошибка'
        ]);
    }
    protected function validateFromBank ( $req ) {
        $validatedData = $req -> validate([
            'from_bank_name' => 'required|string|min:5|max:150',
            'from_bank_card' => 'required|digits:16',
        ]);
    }
    protected function validateToBank ( $req ) {
        $validatedData = $req -> validate([
            'to_bank_name' => 'required|string|min:5|max:150',
            'to_bank_card' => 'required|digits:16',
        ]);
    }
    protected function validateToCrypto ( $req , $to ) {
        $validatedData = $req -> validate([
            'coin_address' => 'required|string|min:12|max:150',
        ]);
        if ( strlen( $to -> additional_field_placeholder ) ) {
            $validatedData = $req -> validate([
                'coin_additional_address' => 'required|string|min:2|max:150',
            ]);
        }
    }
    protected function stepOneSess ( $transaction ) {
        setcookie( "transaction" , 1 , time() + ( 60 * 20 ) ); // 60 seconds ( 1 minute) * 20 = 20 minutes
        setcookie( "transaction_obj" , $transaction , time() + ( 60 * 60 * 2 ) ); // 60 seconds ( 1 minute) * 20 = 20 minutes
    }
    protected function fillColumns ( $transaction , $from , $to , $amount ) {
        if ( $from -> code_bestchange == 93 ) $transaction -> btc = 1;
        if ( $from -> bank ) $transaction -> from_bank = 1;
        if ( $to -> bank ) $transaction -> to_bank = 1;
        $exchange = Exchange::where([
            'from' => $from -> code_bestchange,
            'to' => $to -> code_bestchange
        ]) -> first();
        // $price = $amount * ( $exchange -> price );
        $transaction -> rate = env( 'COIN_DECIMAL' ) / ( $exchange -> price / env( 'COIN_DECIMAL' ) );
        $transaction -> total_send = $amount * env( 'COIN_DECIMAL' );
        $transaction -> total_receive = $amount / ( $exchange -> price / env( 'COIN_DECIMAL' ) ) * env( 'COIN_DECIMAL' );
        if ( $transaction -> btc == 1 ) {
            $blc = new BlockChainHelper;
            $address = $blc -> createAddress();
            $transaction -> btc_address = $address[ 0 ];
            $transaction -> btc_secret = $address[ 1 ];
        }
        $transaction -> save();
    }
    public function start ( Request $req ) {
        $validatedData = $req -> validate([
            'phone' => 'required|min:7|max:150',
            'email' => 'required|email',
            'amount' => 'required|numeric|min:0.0001|max:100000'
        ]);
        $from = $req -> send_from;
        $to = $req -> send_to;
        if ( $from == $to ) return $this -> fatal();
        $from = Currency::where( 'code_bestchange' , $from ) -> firstOrFail();
        $to = Currency::where( 'code_bestchange' , $to ) -> firstOrFail();
        if ( $from -> bank ) {
            $this -> validateFromBank( $req );
        }
        if ( $to -> bank ) {
            $this -> validateToBank( $req );
        } else {
            $this -> validateToCrypto( $req , $to );
        }
        $data = $req -> only([
            'coin_address',
            'coin_additional_address',
            'from_bank_name',
            'from_bank_card',
            'to_bank_name',
            'to_bank_card',
            'send_from',
            'send_to',
            'email',
            'phone',
        ]);
        if ( Auth::user() ) $data[ 'user_id' ] = Auth::user() -> id;
        $data[ 'rate' ] = 0;
        $data[ 'total_send' ] = 0;
        $data[ 'total_receive' ] = 0;
        $data[ 'uuid' ] = (string) Str::uuid();
        $transaction = Transaction::create( $data );
        // dd($data,$transaction);
        $this -> fillColumns( $transaction , $from , $to , $req -> amount );
        // exit( 'everything\'s ok' );
        Emailer::notifyTransaction( $transaction -> id );
        return redirect() -> route( 'transaction_process' , $transaction -> uuid );
        // $this -> stepOne( $transaction );
        // return redirect() -> back();
        // dd($from,$to);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
