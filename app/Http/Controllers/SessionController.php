<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class SessionController extends Controller
{
    //
    public function setTheme ( Request $req , $themeCode ) {
        $theme = 'theme-dark';
        if ( $themeCode == '1' ) {
            $theme = 'theme-light';
        }
        $cookie = cookie( 'theme' , $theme , 2628000 );
        // dd(Cookie::get('theme'));
        return redirect() -> back() -> cookie( $cookie );
    }
}
