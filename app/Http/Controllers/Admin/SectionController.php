<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Helpers\FileHelper;
use App\Helpers\OrderHelper;
use App\Mission;
use App\Section;
// use App\Helpers\ExchangeHelper;
use App\Http\Requests\Admin\Section as Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
    	$this -> missions = Mission::all();
    }
    public function index () {
        $sections = Section::has( 'mission' ) -> orderBy( 'id' , 'DESC' ) -> paginate( 30 );
        $data = [
            'title' => 'Секции',
            'sections' => $sections
        ];
        return view( 'admin.sections.index' , $data );
    }

    public function index_mission ( $mission_id ) {
        $mission = Mission::findOrFail( $mission_id );
        $sections = Section::whereMissionId( $mission_id ) -> orderBy( 'order' , 'DESC' ) -> paginate( 30 );
        $data = [
            'title' => 'Секции в миссии ' . $mission -> title ,
            'sections' => $sections,
        ];
        return view( 'admin.sections.index' , $data );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление секции',
            'missions' => $this -> missions
        ];
        return view( 'admin.sections.create' , $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $input = $req -> except( 'file' , '_token' , '_method' );
        if ( $req -> has( 'file' ) ) {
	        $file = $req -> file( 'file' );
	        $fileInfo = FileHelper::upload( $file , env( 'SECTION_ICONS_PATH' ) );
	        $input[ 'img_url' ] = env( "SECTION_ICONS_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
	        $input[ 'img_path' ] = $fileInfo[ 'path' ];
	    }
        Section::create( $input );
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_sections.index' ) -> withStatus( 'Секция успешно добавлена' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
    	$section = Section::findOrFail( $id );
        $data = [
            'title' => 'Изменение секции',
            'section' => $section,
            'missions' => $this -> missions
        ];
        return view( 'admin.sections.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        $input = $req -> except( 'file' , '_token' , '_method' );
        if ( $req -> has( 'file' ) ) {
            $file = $req -> file( 'file' );
	        $fileInfo = FileHelper::upload( $file , env( 'SECTION_ICONS_PATH' ) );
	        $input[ 'img_url' ] = env( "SECTION_ICONS_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
	        $input[ 'img_path' ] = $fileInfo[ 'path' ];
        }
        Section::whereId( $id ) -> update( $input );
        OrderHelper::orderAll();
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
        return redirect() -> route( 'admin_sections.index' ) -> withStatus( 'Секция успешно изменена' );




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ( $id ) {
        Section::findOrFail( $id ) -> delete();
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_sections.index' ) -> withStatus( 'Секция успешно удалена' );
    }
}
