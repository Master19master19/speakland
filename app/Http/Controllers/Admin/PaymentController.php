<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;

class PaymentController extends Controller
{
    public function index () {
        $payments = Payment::has( 'user' ) -> orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $sum = Payment::has( 'user' ) -> where( 'paid' , 1 ) -> sum( 'amount' );
        $data = [
            'title' => 'Оплаты',
            'payments' => $payments,
            'sum' => $sum
        ];
        return view( 'admin.payments.index' , $data );
    }
}
