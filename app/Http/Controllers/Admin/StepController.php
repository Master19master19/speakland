<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Helpers\FileHelper;
use App\Mission;
use App\Step;
use App\Media;
use App\Section;
use App\Helpers\OrderHelper;
// use App\Helpers\ExchangeHelper;
use App\Http\Requests\Admin\Step as Request;

class StepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
    	$this -> missions = Mission::all();
    	$this -> sections = Section::has( 'mission' ) -> get();
    }
    public function index () {
        $steps = Step::has( 'section' ) -> has( 'mission' ) -> orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Шаги',
            'steps' => $steps,
        ];
        return view( 'admin.steps.index' , $data );
    }

    public function index_mission ( $mission_id ) {
        $mission = Mission::findOrFail( $mission_id );
        $steps = Step::has( 'section' ) -> has( 'mission' ) -> whereMissionId( $mission_id ) -> orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Шаги в миссии ' . $mission -> title ,
            'steps' => $steps,
        ];
        return view( 'admin.steps.index' , $data );
    }

    public function index_section ( $section_id ) {
        $section = Section::findOrFail( $section_id );
        $steps = Step::has( 'section' ) -> has( 'mission' ) -> whereSectionId( $section_id ) -> orderBy( 'order' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Шаги в секции ' . $section -> title ,
            'steps' => $steps,
        ];
        return view( 'admin.steps.index' , $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление шага',
            'sections' => $this -> sections,
        ];
        return view( 'admin.steps.create' , $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */




    protected function handleMedia ( $req , $record , $edit = false ) {
        if ( $edit ) {
            $allMediasIds = Media::where( 'step_id' , $record -> id ) -> pluck( 'id' ) -> toArray();
            $allMediasIds = array_flip( $allMediasIds );
            // dd($allMediasIds);
        }
        if ( null !== $req -> media && count( $req -> media ) ) {
            foreach ( $req -> media as $key => $med ) {
                if ( $edit && ! isset( $med[ 'content' ] ) ) {
                    Media::whereId( $key ) -> update([ 'order' => $med[ 'order' ] ]);
                    if ( isset( $allMediasIds[ $key ] ) ) {
                        unset( $allMediasIds[ $key ] );
                    }
                } else {
                    $data = [
                        'type' => $med[ 'type' ],
                        'order' => $med[ 'order' ],
                        'step_id' => $record[ 'id' ]
                    ];
                    if ( $med[ 'type' ] == 'text' ) {
                        $data[ 'content' ] = $med[ 'content' ];
                    } else {
                        $file = $req -> file( 'media.' . $key . '.content' );
                        $fileInfo = FileHelper::upload( $file , env( 'STEP_MEDIA_PATH' ) );
                        $data[ 'file_url' ] = env( "STEP_MEDIA_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
                        $data[ 'file_path' ] = $fileInfo[ 'path' ];
                    }
                    $media = Media::create( $data );
                }
            }
            if ( $edit ) {
                foreach ( $allMediasIds as $mediaId => $value ) {
                    Media::whereId( $mediaId ) -> delete();
                }
            }
            // dd($allMediasIds);
        }
    }
    public function store ( Request $req ) {
        $input = $req -> except( 'media' , '_token' , '_method' );
        $section = Section::findOrFail( $req -> section_id );
        $input[ 'mission_id' ] = $section -> mission_id; 
        $record = Step::create( $input );
        // dd($req->all());
        $this -> handleMedia( $req , $record );
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_steps.index' ) -> withStatus( 'Шаг успешно добавлен' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
    	$step = Step::findOrFail( $id );
        $data = [
            'title' => 'Изменение шага',
            'step' => $step,
            'sections' => $this -> sections
        ];
        return view( 'admin.steps.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        $input = $req -> except( 'media' , '_token' , '_method' );
        $section = Section::findOrFail( $req -> section_id );
        $input[ 'mission_id' ] = $section -> mission_id; 
        Step::whereId( $id ) -> update( $input );
        $record = Step::find( $id );
        $this -> handleMedia( $req , $record , true );
        OrderHelper::orderAll();
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
        return redirect() -> route( 'admin_steps.index' ) -> withStatus( 'Шаг успешно изменен' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ( $id ) {
        Step::findOrFail( $id ) -> delete();
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_steps.index' ) -> withStatus( 'Шаг успешно удален' );
    }
}
