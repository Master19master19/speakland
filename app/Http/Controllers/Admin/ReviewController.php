<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rating;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Rating::orderBy( 'id' , 'DESC' ) -> paginate( 35 );
        $data = [
            'title' => 'Отзывы',
            'reviews' => $reviews
        ];
        return view( 'admin.reviews.index' , $data );
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Review::findOrFail( $id ) -> update([ 'published' => 1 ]);
        //
    }
    public function publish($id)
    {
        Rating::findOrFail( $id ) -> update([ 'published' => 1 ]);
        return redirect() -> back() -> withStatus( 'Отзыв опубликован' );
        //
    }
    public function delete($id) {
        Rating::findOrFail( $id ) -> delete();
        return redirect() -> back() -> withStatus( 'Отзыв удален' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $review = Rating::findOrFail( $id );
        $data = [
            'title' => 'Редактироване отзыва',
            'review' => $review
        ];
        return view( 'admin.reviews.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //
        $review = Rating::findOrFail( $id );
        $review -> update( $req -> only( [ 'rating' , 'message' ] ));
        return redirect() -> route( 'reviews.index' ) -> withStatus( 'Отзыв Изменен' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
