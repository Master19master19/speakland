<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Currency;
use App\Exchange;
use App\Helpers\FileHelper;
use App\Helpers\ExchangeHelper;
use App\CurrencyGroup;
use App\Bestchange;
use App\Http\Requests\Admin\Currency as Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $currencies = Currency::has( 'group' ) -> get();
        $data = [
            'title' => 'Валюты',
            'currencies' => $currencies
        ];
        return view( 'admin.currencies.index' , $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $currency_groups = CurrencyGroup::all();
        $bestchange = Bestchange::orderBy( 'title' ) -> get();
        $data = [
            'title' => 'Добавление валюты',
            'currency_groups' => $currency_groups,
            'bestchange' => $bestchange
        ];
        return view( 'admin.currencies.create' , $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $file = $req -> file( 'file' );
        $fileInfo = FileHelper::upload( $file , env( 'CURRENCY_ICONS_PATH' ) );
        $input = $req -> except( 'file' , '_token' , '_method' );
        $input[ 'img_uri' ] = env( "CURRENCY_ICONS_FOLDER" ) . $fileInfo[ 'title' ];
        $input[ 'img_path' ] = $fileInfo[ 'path' ];
        if ( $req -> has( 'bank' ) ) $input[ 'bank' ] = 1;
        else $input[ 'bank' ] = 0;
        Currency::create( $input );
        ExchangeHelper::populate();
        return redirect() -> route( 'currencies.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
        $currency = Currency::whereId( $id ) -> firstOrFail();
        $currency_groups = CurrencyGroup::all();
        $exchanges = Exchange::with([ 'currency_to' , 'currency_from' ]) -> whereFrom( $currency -> code_bestchange ) -> orWhere( 'to' , $currency -> code_bestchange  ) -> get();
        $exchanges = Exchange::with([ 'currency_to' , 'currency_from' ]) -> whereFrom( $currency -> code_bestchange ) -> get();
        $bestchange = Bestchange::orderBy( 'title' ) -> get();
        $data = [
            'title' => 'Изменение валюты',
            'currency_groups' => $currency_groups,
            'bestchange' => $bestchange,
            'currency' => $currency,
            'exchanges' => $exchanges
        ];
        return view( 'admin.currencies.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        // dd($req->all());
        $input = $req -> only( 'title' , 'code' , 'code_bestchange' , 'reserve' , 'currency_group_id' , 'field_placeholder' , 'additional_field_placeholder' , 'min' , 'max' , 'min_out' , 'max_out' );
        if ( $req -> has( 'file' ) ) {
            $file = $req -> file( 'file' );
            $fileInfo = FileHelper::upload( $file , env( 'CURRENCY_ICONS_PATH' ) );
            $input[ 'img_uri' ] = env( "CURRENCY_ICONS_FOLDER" ) . $fileInfo[ 'title' ];
            $input[ 'img_path' ] = $fileInfo[ 'path' ];
        }
        if ( $req -> has( 'active' ) ) $input[ 'active' ] = 1;
        else $input[ 'active' ] = 0;
        if ( $req -> has( 'bank' ) ) $input[ 'bank' ] = 1;
        else $input[ 'bank' ] = 0;
        if ( $req -> has( 'active_out' ) ) $input[ 'active_out' ] = 1;
        else $input[ 'active_out' ] = 0;
        // if ( $req -> has( 'parser' ) ) $input[ 'parser' ] = 1;
        // else $input[ 'parser' ] = 0;
        Currency::whereId( $id ) -> update( $input );
        ExchangeHelper::update( $req -> all() );
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        ExchangeHelper::depopulate( $id );
        Currency::findOrFail( $id ) -> delete();
        return redirect() -> route( 'currencies.index' );
    }
}
