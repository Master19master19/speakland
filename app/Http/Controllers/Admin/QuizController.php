<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Helpers\FileHelper;
use App\Mission;
use App\Step;
use App\Quiz;
use App\Section;
// use App\Helpers\ExchangeHelper;
use App\Http\Requests\Admin\Quiz as Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
    	$this -> missions = Mission::all();
    }
    public function index () {
        $quizzes = Quiz::has( 'mission' ) -> orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Тесты',
            'quizzes' => $quizzes,
        ];
        return view( 'admin.quizzes.index' , $data );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление теста',
            'missions' => $this -> missions,
        ];
        return view( 'admin.quizzes.create' , $data );
    }

    public function store ( Request $req ) {
        $input = $req -> except( '_token' , '_method' );
        $record = Quiz::create( $input );
        return redirect() -> route( 'admin_quizzes.index' ) -> withStatus( 'Тест успешно добавлен' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
    	$quiz = Quiz::findOrFail( $id );
        $data = [
            'title' => 'Изменение теста',
            'quiz' => $quiz,
            'missions' => $this -> missions
        ];
        return view( 'admin.quizzes.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        $input = $req -> except( '_token' , '_method' );
        Quiz::whereId( $id ) -> update( $input );
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ( $id ) {
        Quiz::findOrFail( $id ) -> delete();
        return redirect() -> route( 'admin_quizzes.index' ) -> withStatus( 'Тест успешно удален' );
    }
}
