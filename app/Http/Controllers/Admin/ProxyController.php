<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Proxy;
use App\Helpers\FileHelper;
use App\Helpers\ProxyHelper;


class ProxyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $proxies = Proxy::where( 'id' , '!=' , 1 ) -> orderBy( 'id' , 'DESC' ) -> get();
        $data = [
            'title' => 'Прокси',
            'proxies' => $proxies
        ];
        return view( 'admin.proxies.index' , $data );
        //
    }
    public function indexBak () {
        $proxies = Proxy::orderBy( 'id' , 'DESC' ) -> get();
        $data = [
            'title' => 'Прокси',
            'proxies' => $proxies
        ];
        return view( 'admin.proxies.index' , $data );
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $ip = $req -> ip_address;
        $proxy = new Proxy;
        $proxy -> ip = $ip;
        $proxy -> save();
        return redirect() -> back() -> withStatus( 'Сохранено!' );
        //
    }

    public function upload(Request $req)
    {
        $file = $req -> file( 'file' );
        $proxies = FileHelper::uploadProxies( $file );
        $res = ProxyHelper::populate( $proxies );
        if ( ! $res ) return redirect() -> back() -> withStatus( 'Не удалось обновить список' );
        return redirect() -> back() -> withStatus( 'Список успешно обновлен' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Proxy::findOrFail( $id ) -> delete();
        return redirect() -> route( 'proxies.index' );
    }
}
