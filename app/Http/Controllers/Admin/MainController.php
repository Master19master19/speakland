<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Emailer;
use App\Http\Requests\Admin\Auth as AuthRequest;
use App\Helpers\SMSHelper;
use App\User;
use Auth;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        return view( 'admin.index' );
    }
    public function transactionStatus ( $id , Request $req ) {
        $status = $req -> status;
        Transaction::whereId( $id ) -> update( compact( 'status' ) );
        Emailer::notifyTransaction( $id );
        return response() -> json([ 'status' => 'ok' ]);
    }
    public function login () {
        return view( 'admin.login' );
    }

    public function logout() {
        Auth::logout();
        return redirect( '/' );
    }
    public function loginPost ( AuthRequest $req ) {
        if ( Auth::attempt( $req -> only( 'email' , 'password' ) ) ) {
            if ( Auth::user() -> type == 1 ) {
                session([ 'memcache_key' => Auth::user() -> id ]);
                $sms_code = rand( 111111 , 999999 );
                SMSHelper::send( $sms_code , Auth::user() );
                User::whereId( Auth::user() -> id ) -> update([ 'sms_code' => $sms_code ]);
                Auth::logout();
                return redirect( '/admin/sms' );
            } else {
                return redirect() -> back();
            }
        } else {
            return redirect() -> back();
        }
    }
    public function canceled_transactions () {
        $transactions = Transaction::with([ 'from' , 'to' ]) -> orderBy( 'id' , 'DESC' ) -> whereCanceled( 1 ) -> paginate( 30 );
        $data = [
            'title' => 'История отмененных транзакций',
            'transactions' => $transactions
        ];
        return view( 'admin.index' , $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
