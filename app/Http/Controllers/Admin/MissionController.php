<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Helpers\FileHelper;
use App\Mission;
// use App\Helpers\ExchangeHelper;
use App\Http\Requests\Admin\Mission as Request;

class MissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $missions = Mission::orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Миссии',
            'missions' => $missions
        ];
        return view( 'admin.missions.index' , $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление миссии',
        ];
        return view( 'admin.missions.create' , $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $file = $req -> file( 'file' );
        $fileInfo = FileHelper::upload( $file , env( 'MISSION_ICONS_PATH' ) );
        $input = $req -> except( 'file' , '_token' , '_method' );
        $input[ 'img_url' ] = env( "MISSION_ICONS_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
        $input[ 'img_path' ] = $fileInfo[ 'path' ];
        Mission::create( $input );
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_missions.index' ) -> withStatus( 'Миссия успешно добавлена' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
    	$mission = Mission::findOrFail( $id );
        $data = [
            'title' => 'Изменение миссии',
            'mission' => $mission,
        ];
        return view( 'admin.missions.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        $input = $req -> except( 'file' , '_token' , '_method' );
        if ( $req -> has( 'file' ) ) {
            $file = $req -> file( 'file' );
	        $fileInfo = FileHelper::upload( $file , env( 'MISSION_ICONS_PATH' ) );
	        $input[ 'img_url' ] = env( "MISSION_ICONS_PUBLIC_PATH" ) . $fileInfo[ 'title' ];
	        $input[ 'img_path' ] = $fileInfo[ 'path' ];
        }
        Mission::whereId( $id ) -> update( $input );
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_missions.index' ) -> withStatus( 'Миссия успешно изменена' );




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ( $id ) {
        Mission::findOrFail( $id ) -> delete();
        OrderHelper::orderAll();
        return redirect() -> route( 'admin_missions.index' ) -> withStatus( 'Миссия успешно удалена' );
    }
}
