<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Helpers\FileHelper;
use App\Mission;
use App\Step;
use App\Quiz;
use App\Question;
use App\Answer;
use App\Section;
// use App\Helpers\ExchangeHelper;
use App\Http\Requests\Admin\Question as Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
    	$this -> quizzes = Quiz::has( 'mission' ) -> get();
    }
    public function index () {
        $questions = Question::has( 'quiz' ) -> orderBy( 'id' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Вопросы',
            'questions' => $questions,
        ];
        return view( 'admin.questions.index' , $data );
    }

    public function index_quiz ( $quiz_id ) {
    	$quiz = Quiz::findOrFail( $quiz_id );
        $questions = Question::where( 'quiz_id' , $quiz_id ) -> orderBy( 'order' , "DESC" ) -> paginate( 30 );
        $data = [
            'title' => 'Вопросы теста ' . $quiz -> title,
            'questions' => $questions,
        ];
        return view( 'admin.questions.index' , $data );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление вопроса',
            'quizzes' => $this -> quizzes,
        ];
        return view( 'admin.questions.create' , $data );
    }

    private function handleAnswers ( $answers , $record ) {
        Answer::where( 'question_id' , $record -> id ) -> delete();
        foreach ( $answers as $key => $ans ) {
            $right = isset( $ans[ 'right' ] ) ? 1 : 0;
            $data = [
                'question_id' => $record -> id,
                'title' => $ans[ 'title' ],
                'description' => $ans[ 'description' ],
                'order' => $ans[ 'order' ],
                'right' => $right
            ];
            Answer::create( $data );
        }
    }
    public function store ( Request $req ) {
        // dd($req->all());
        if ( ! $req -> has( 'answers' ) ||  ! count( $req -> answers ) ) {
            return redirect() -> back() -> withInput( $req -> all() ) -> withErrors([ 'Ответы' => 'Пожалуйста добавьте варианты ответов на вопрос' ]);
        }
        $input = $req -> except( '_token' , '_method' , 'answers' );
        // $input = $req -> only([ 'title' , 'description' , 'order' , 'mission_id' , 'duration_seconds' ]);
        $record = Question::create( $input );
        $this -> handleAnswers( $req -> answers , $record );
        return redirect() -> route( 'admin_questions.index' ) -> withStatus( 'Вопрос успешно добавлен' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
        $question = Question::findOrFail( $id );
    	$answers = Answer::where( 'question_id' , $id ) -> orderBy( 'order' ) -> get();
        $data = [
            'title' => 'Изменение вопроса',
            'answers' => $answers,
            'question' => $question,
            'quizzes' => $this -> quizzes
        ];
        return view( 'admin.questions.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , $id ) {
        $input = $req -> except( '_token' , '_method' , 'answers' );
        $record = Question::whereId( $id ) -> update( $input );
        $record = Question::findOrFail( $id );
        $this -> handleAnswers( $req -> answers , $record );
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ( $id ) {
        Question::findOrFail( $id ) -> delete();
        Answer::where( 'question_id' , $id ) -> delete();
        return redirect() -> route( 'admin_questions.index' ) -> withStatus( 'Вопрос успешно удален' );
    }
}
