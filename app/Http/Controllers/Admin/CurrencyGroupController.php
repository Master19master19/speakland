<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FileHelper;
use App\CurrencyGroup;
use App\Http\Requests\Admin\CurrencyGroup as Request;

class CurrencyGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $currency_groups = CurrencyGroup::all();
        $data = [
            'title' => 'Группа валют',
            'currency_groups' => $currency_groups
        ];
        return view( 'admin.currency_groups.index' , $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $data = [
            'title' => 'Добавление группы валют',
        ];
        return view( 'admin.currency_groups.create' , $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $input = $req -> only( 'title' );
        CurrencyGroup::create( $input );
        return redirect() -> route( 'currency_groups.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit ( $id ) {
        $currency_group = CurrencyGroup::whereId( $id ) -> firstOrFail();
        $data = [
            'title' => 'Изменение группы валют',
            'currency_group' => $currency_group,
        ];
        return view( 'admin.currency_groups.edit' , $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req , $id ) {
        $input = $req -> only( 'title' );
        CurrencyGroup::whereId( $id ) -> update( $input );
        return redirect() -> back() -> withStatus( 'Изменения сохранены' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        CUrrencyGroup::findOrFail( $id ) -> delete();
        return redirect() -> route( 'currency_groups.index' );
    }
}
