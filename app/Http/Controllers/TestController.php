<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ScrapeHelper;
use App\Helpers\LogHelper;
use App\Bestchange;
use App\Exchange;
use App\Transaction;


class TestController extends Controller {
  public function test ( $url = "http://api.bestchange.ru/info.zip" ) {
    ScrapeHelper::scrape( $url );
}

  public function mail ( $id ) {
  	$transaction = Transaction::with([ 'from' , 'to' ]) -> whereId( $id ) -> firstOrFail();
  	return view( 'transaction.mail' , compact( 'transaction' ) );
}

}
