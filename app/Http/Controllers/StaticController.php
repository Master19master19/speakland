<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rating;


class StaticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about () {
        $pageTitle = __( "About us" );
        return view( 'static.about' );
    }
    public function reviews () {
        $pageTitle = __( "Reviews" );
        $reviews = Rating::wherePublished( 1 ) -> get();
        return view( 'static.reviews' , compact( 'reviews' ) );
    }
    public function rules () {
        $pageTitle = __( "Rules of service" );
        return view( 'static.rules' );
    }
    public function contacts () {
        $pageTitle = __( "Contacts" );
        return view( 'static.contact' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
