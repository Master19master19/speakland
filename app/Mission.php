<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    //
    protected $guarded = [];
	public function sections () {
		return $this -> hasMany( 'App\Section', 'mission_id' , 'id' ) -> orderBy( 'order' );
	}
	public function quiz () {
		return $this -> hasOne( 'App\Quiz', 'mission_id' , 'id' );
	}
	public function steps () {
		return $this -> hasManyThrough( 'App\Step' , 'App\Section' , 'mission_id' , 'section_id' , 'id' , 'id' ) -> orderBy( 'order' );
	}
	public function stepCount () {
		return $this -> steps() -> count();
	}
}
