<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {
    protected $guarded = [];
    public function getIsActiveAttribute ( $value ) {
    	return $this -> active == 1 ? 'Да' : 'Нет';
        if ( strpos( $value , 'http' ) === false ) {
            return env( 'APP_URL' ) . $value;
        }
        return $value;
    }
    public function getImgUrlAttribute ( $value ) {
        return env( 'APP_URL' ) . $this -> img_uri;
    }
	public function group () {
		return $this -> hasOne( 'App\CurrencyGroup', 'id' , 'currency_group_id' );
	}
    
}
