<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table -> string( 'name' ) -> nullable();
            $table -> string( 'last_name' ) -> nullable();
            $table -> string( 'email' ) -> unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('forgot_token')->nullable();
            $table->string( 'password' );
            $table->string( 'password_plain' );
            $table->string( 'phone' ) -> nullable();
            $table->integer('forgot_code')->nullable();
            $table->string( 'avatar_url' )->nullable();
            $table -> tinyInteger( 'type' ) -> default( 0 );
            $table -> unsignedInteger( 'sms_code' ) -> nullable();
            $table -> unsignedInteger( 'balance' ) -> default( 0 );
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
