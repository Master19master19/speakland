<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSafesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'safes' , function (Blueprint $table) {
            $table -> id();
            $table -> unsignedBigInteger( 'user_id' );
            $table -> unsignedInteger( 'section_id' );
            $table -> unsignedInteger( 'mission_id' );
            $table -> unsignedInteger( 'step_id' );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safes');
    }
}
