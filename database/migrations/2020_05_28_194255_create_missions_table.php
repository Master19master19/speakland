<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table -> id();
            $table -> string( 'title' );
            $table -> unsignedTinyInteger( 'order' );
            $table -> string( 'description' ) -> nullable();
            $table -> unsignedTinyInteger( 'pass_price' ) -> nullable();
            $table -> string( 'img_path' );
            $table -> string( 'img_url' );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
