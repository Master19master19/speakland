<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'media' , function ( Blueprint $table ) {
            $table -> id();
            $table -> unsignedBigInteger( 'step_id' );
            $table -> unsignedInteger( 'order' );
            $table -> string( 'type' );
            $table -> string( 'file_path' ) -> nullable();
            $table -> string( 'file_url' ) -> nullable();
            $table -> text( 'content' ) -> nullable();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
