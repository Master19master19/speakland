<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table -> id();
            $table -> unsignedBigInteger( 'section_id' );
            $table -> unsignedBigInteger( 'mission_id' );
            $table -> string( 'title' );
            $table -> string( 'transcription' );
            $table -> unsignedTinyInteger( 'order' );
            $table -> string( 'description' );
            $table -> unsignedInteger( 'duration_seconds' );
            $table -> string( 'video_path' ) -> nullable();
            $table -> string( 'video_url' ) -> nullable();
            $table -> string( 'audio_path' ) -> nullable();
            $table -> string( 'audio_url' ) -> nullable();
            $table -> string( 'pdf_path' ) -> nullable();
            $table -> string( 'pdf_url' ) -> nullable();
            $table -> string( 'img_path' ) -> nullable();
            $table -> string( 'img_url' ) -> nullable();
            $table -> unsignedInteger( 'pass_price' ) -> nullable();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
