$.ajaxSetup({
	headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

$( function() {
	handleTheme();
	handleCollapse();
});




function handleCollapse() {
	$( '[data-toggle="collapse"]' ).each( function() {
		// console.log($( this ).closest( 'div' ).parent( 'div' ).next( 'div' ).hasClass('show'))
		if ( $( this ).closest( 'div' ).parent( 'div' ).next( 'div' ).hasClass( 'show' ) ) {
			$( this ).find( '.fa' ).toggleClass( 'fa-chevron-up fa-chevron-down' );
		}
	});
	$( '[data-toggle="collapse"]' ).click( function() {
		$( this ).find( '.fa' ).toggleClass( 'fa-chevron-up fa-chevron-down' );
	});
}
function handleTheme() {
	$( '[data-id="btn-theme"]' ).on( 'click' , function() {
		let body = $( 'body' );
		let themeCode = 0;
		if ( body.attr( 'id' ) == 'theme-dark' ) {
			body.attr( 'id' , 'theme-light' );
			themeCode = 1;
		} else {
			body.attr( 'id' , 'theme-dark' );
		}
		$.get( '/theme/' + themeCode , function( res ) {
			console.log( res );
		});
	});
}