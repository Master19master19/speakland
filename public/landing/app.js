$( function() {
	$('head').append('<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">');
	setTimeout( function() {
		loadVideos();
	} , 250 );
	floatingButton();
});



function floatingButton () {
	mybutton = document.getElementById( "myBtn" );
	let $btn = $( '#myBtn' );
	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {scrollFunction()};

	function scrollFunction() {
	  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
	    $btn.css({ transform: 'translateY( 0px )' });
	  } else {
	    $btn.css({ transform: 'translateY( 100px )' });
	    // $btn.fadeOut();
	  }
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
	  document.body.scrollTop = 0; // For Safari
	  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}
}

function loadVideos() {
	let x = 1;
	$( '.player' ).each( function() {
		let id = 'player_' + x;
		$( this ).attr( 'id' , id );
		x++;
		let preview = $( this ).data( 'poster' );
		let source = $( this ).data( 'source' );
		let data = {
		  "id": id,
		  "file": source,
		  "default_quality": "1080p",
		};
		if ( preview.length ) {
			data[ 'poster' ] = preview;
		}
		let player = new Playerjs( data );
	});
}