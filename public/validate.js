$( function() {
	if ( $( 'form#main-form' ).length == 0 ) return;
	setValidation();
});



// coin_address - required|string|min:12|max:150
function setValidation() {
	$( 'form#main-form' ).on( 'submit' , function ( e ) {
		$( 'form#main-form #form-error' ).remove();
		if ( ! $( '[name="coin_address"]' ).is( ":hidden" ) ) {
			if ( ! isValidCoinAddress( $( '[name="coin_address"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}
		if ( ! $( '[name="coin_additional_address"]' ).is( ":hidden" ) ) {
			if ( ! isValidCoinAdditionalAddress( $( '[name="coin_additional_address"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}

		if ( ! $( '[name="from_bank_name"]' ).is( ":hidden" ) ) {
			if ( ! isValidFBN( $( '[name="from_bank_name"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}

		if ( ! $( '[name="from_bank_card"]' ).is( ":hidden" ) ) {
			$( '[name="from_bank_card"]' ).val( $( '[name="from_bank_card"]' ).val().split( " " ).join( "" ) )
			if ( ! isValidFBC( $( '[name="from_bank_card"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}
		if ( ! $( '[name="to_bank_name"]' ).is( ":hidden" ) ) {
			if ( ! isValidTBN( $( '[name="to_bank_name"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}
		if ( ! $( '[name="to_bank_card"]' ).is( ":hidden" ) ) {
			$( '[name="to_bank_card"]' ).val( $( '[name="to_bank_card"]' ).val().split( " " ).join( "" ) )
			if ( ! isValidTBC( $( '[name="to_bank_card"]' ).val() ) ) {
				e.preventDefault();
				return false;
			}
		}
		if ( ! isValidEmailAddress( $( '[name="email"]' ).val() ) ) {
			e.preventDefault();
			return false;
		}
		if ( ! isValidPhone( $( '[name="phone"]' ).val() ) ) {
			e.preventDefault();
			return false;
		}
		if ( $( '[data-input-give]' ).hasClass( 'is-invalid' ) ) {
			setError( 'Please check the "give amount" input' );
			e.preventDefault();
			return false;
		}
	});
}


function setError ( text ) {
	let lang = $( '[name="language"]' ).val();
	if ( lang == 'ru' ) {
		text = translations[ text ];
	}
	$( 'form#main-form' ).prepend( "<div id='form-error' class='alert alert-danger'>" + text + "</div>" );
}


let translations = {
	'Receiver full name field must be must be at least 5 characters' : 'Поле полного имени получателя должно быть не менее 5 символов.',
	'Receiver card number field must be 16 digits' : 'Поле номера карты получателя должно состоять из 16 цифр.',
	'Sender full name field must be at least 5 characters' : 'Поле полного имени отправителя должно содержать не менее 5 символов.',
	'Sender card number field must be 16 digits' : 'Поле номера карты отправителя должно состоять из 16 цифр.',
	'Coin additional address field must be at least 2 characters' : 'Дополнительное поле адреса криптовалюты должно содержать не менее 2 символов.',
	'Coin address field must be at least 12 characters' : 'Поле адреса криптовалюты должно быть не менее 12 символов.',
	'Please input a valid email' : 'Пожалуйста, введите правильный адрес электронной почты.',
	'Phone number field must be at least 7 characters' : 'Поле номера телефона должно быть не менее 7 символов.',
	'Please check the "give amount" input' : 'Пожалуйста, проверьте поле "Отдаете"'
};




function isValidTBN ( val ) {
	// return true;
	if ( val.length < 5 || val.length > 150 ) {
		setError( 'Receiver full name field must be must be at least 5 characters' );
    	return false;
	}
	return true;
}
function isValidTBC ( val ) {
	val = parseInt( val ) + '';
	if ( val.length != 16 ) {
		setError( 'Receiver card number field must be 16 digits' );
    	return false;
	}
	return true;
}

function isValidFBN ( val ) {
	if ( val.length < 5 || val.length > 150 ) {
		setError( 'Sender full name field must be at least 5 characters' );
    	return false;
	}
	return true;
}
function isValidFBC ( val ) {
	val = parseInt( val ) + '';
	if ( val.length != 16 ) {
		setError( 'Sender card number field must be 16 digits' );
    	return false;
	}
	return true;
}
function isValidCoinAdditionalAddress ( addr ) {
	if ( addr.length < 2 || addr.length > 150 ) {
		setError( 'Coin additional address field must be at least 2 characters' );
    	return false;
	}
	return true;
}
function isValidCoinAddress ( addr ) {
	if ( addr.length < 12 || addr.length > 150 ) {
		setError( 'Coin address field must be at least 12 characters' );
    	return false;
	}
	return true;
}

function isValidEmailAddress( emailAddress ) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    if ( ! pattern.test( emailAddress ) ) {
    	setError( 'Please input a valid email' );
    	return false;
    }
	return true;
};


function isValidPhone ( phone ) {
	let parsed = parseInt( phone ) + "";
	if ( parsed.length < 7 || parsed.length > 150 ) {
    	setError( 'Phone number field must be at least 7 characters' );
    	return false;
	}
	return true;
};
