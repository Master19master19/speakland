$( function() {
	handleTimer();
  syncBackend();
});

var timerWrapShown = false;
var distance = 0;
var duration = $( '[name="step_duration"]' ).val();
// var duration = 2;
var step_id = $( '[name="step_id"]' ).val();

function handleTimer () {
	var countDownDate = new Date().getTime() + duration * 1000;
	// var countDownDate = new Date().getTime() + 20 * 1000;
	timer( countDownDate );
}


function setDone () {
  let url = '/step/' + step_id + '/done';
  $( '#next-step-wrap' ).append( "<a class='btn btn-outline-success hovered' href='" + url + "'>Изучено</a>")
	// $.get( url , function ( res ) {
	// 	console.log( res );
	// });
}


function syncBackend() {
  if ( duration == undefined ) return;
  setInterval( function() {
    $.post( '/step/' + step_id + '/progress' , { duration: distance } , function ( res ) {
      console.log( res );
    })
  } , 5000 );
}
function timer ( time ) {
  if ( duration == undefined ) return;

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  distance = time - now;

  // Time calculations for days, hours, minutes and seconds
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  if ( minutes < 10 ) minutes = "0" + minutes;
  if ( seconds < 10 ) seconds = "0" + seconds;
  // Display the result in the element with id="demo"
  document.getElementById("timer-min").innerHTML = minutes;
  document.getElementById("timer-sec").innerHTML = seconds;
  if ( ! timerWrapShown ) {
    $( '#timer-wrap' ).css({ visibility: 'visible' });
    timerWrapShown = true;
  }
  // If the count down is finished, write some text
  if (distance <= 0) {
  	$('#timer-wrap').remove()
    clearInterval(x);
    setDone();
    // document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);

}