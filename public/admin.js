$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$( function() {
    upload();
    handleAddAnswers();
	handleAddMedia();
});


function handleAddMedia() {
    $tpl = "\
        <div class=\"row row-parent\">\
        <div class=\"form-group col-md\">\
            <select class=\"form-control\" data-media-type required=\"\" name=\"media[increment][type]\">\
                <option selected=\"\" disabled=\"\" value=\"\">Тип медиа</option>\
                <option value=\"text\">Текст</option>\
                <option value=\"image\">Картинка</option>\
                <option value=\"audio\">Аудио</option>\
                <option value=\"video\">Видео</option>\
                <option value=\"pdf\">PDF</option>\
            </select>\
        </div>\
        <div class=\"form-group col-md\">\
            <input type=\"number\" placeholder=\"Порядок\" class=\"form-control\" required=\"\" min=\"1\" name=\"media[increment][order]\" />\
        </div>\
        <div class=\"form-group col-md col-dynamic\">\
            <textarea rows=\"3\" required=\"\" class=\"form-control\" name=\"media[increment][content]\" placeholder=\"Введите текст\"></textarea>\
        </div>\
        <div class=\"form-check col-md-1\">\
          <button type=\"button\" data-delete class=\"btn btn-block btn-danger\"><i class=\"fa fa-trash\"></i></button>\
        </div>\
    </div>\
    ";
    const searchRegExpName = /dynamic-name/g;
    const searchRegExpTitle = /dynamic-title/g;
    const searchRegExpAccepts = /dynamic-accepts/g;
    let accepts = {
        pdf: 'application/pdf,application/vnd.ms-excel',
        audio: 'audio/*',
        video: 'video/*',
        image: 'image/*',
    };
    let names = {
        pdf: 'PDF',
        audio: 'Аудио',
        video: 'Видео',
        image: 'Картинка',
    };
    let $file = "<div class=\"custom-file\">\
                <input required=\"\" type=\"file\" name=\"dynamic-name\" class=\"custom-file-input\" accept=\"dynamic-accepts\" />\
                <label class=\"custom-file-label\">dynamic-title</label>\
            </div>";
    let $textarea = "<textarea rows=\"3\" required=\"\" class=\"form-control\" name=\"dynamic-name\" placeholder=\"Введите текст\"></textarea>";
    $( 'body' ).on( 'change' , '[data-media-type]' , function() {
        let type = $( this ).val();
        let dynamic = $( this ).closest( '.row-parent' ).find( '.col-dynamic' );
        let name = dynamic.find( 'textarea' ).attr( 'name' );
        if ( undefined == name ) {
            name = dynamic.find( 'input' ).attr( 'name' );
        }
        if ( type == 'text' ) {
            let textarea = $textarea.replace( searchRegExpName , name );
            dynamic.html( textarea );
        } else {
            let file = $file.replace( searchRegExpTitle , names[ type ] );
            file = file.replace( searchRegExpName , name );
            file = file.replace( searchRegExpAccepts , accepts[ type ] );
            dynamic.html( file );
        }
    });


    $( 'body' ).on( 'click' , '[data-add-media]' , function() {
        const searchRegExp = /increment/g;
        // let increment = parseInt( $( this ).data( 'add' ) ) + 1;
        // $( this ).data( 'add' , increment );
        $increment = +new Date();
        let newTpl = $tpl.replace( searchRegExp , $increment );
        $( '.dynamic-container' ).append( newTpl );
    });

    $( 'body' ).on( 'click' , '[data-delete]' , function() {
        $( this ).closest( '.row' ).remove();
    });
}
function handleAddAnswers() {
    $tplAns = "\
        <div class=\"row\">\
            <div class=\"form-group col-md\">\
                <input type=\"text\" placeholder=\"Название\" class=\"form-control\" required=\"\" name=\"answers[increment][title]\" />\
            </div>\
            <div class=\"form-group col-md\">\
                <input type=\"number\" placeholder=\"Порядок\" class=\"form-control\" required=\"\" name=\"answers[increment][order]\" />\
            </div>\
            <div class=\"form-group col-md\">\
                <textarea rows=\"1\" class=\"form-control\" name=\"answers[increment][description]\" placeholder=\"Описание\"></textarea>\
            </div>\
            <div class=\"form-check col-md-1\">\
                <label class=\"switch\">\
                  <input type=\"checkbox\" name=\"answers[increment][right]\">\
                  <span class=\"slider\"></span>\
                </label>\
              <label class=\"form-check-label\" for=\"defaultCheck1\">\
                Правильно\
              </label>\
            </div>\
            <div class=\"form-check col-md-1\">\
              <button type=\"button\" data-delete-answer class=\"btn btn-block btn-danger\"><i class=\"fa fa-trash\"></i></button>\
            </div>\
        </div>\
    ";
    $( 'body' ).on( 'click' , '[data-add-answer]' , function() {
        const searchRegExp = /increment/g;
        // let increment = parseInt( $( this ).data( 'add' ) ) + 1;
        // $( this ).data( 'add' , increment );
        $increment = + new Date();
        let newTpl = $tplAns.replace( searchRegExp , $increment );
        $( '.dynamic-container-answers' ).append( newTpl );
    });
    $( 'body' ).on( 'click' , '[data-delete-answer]' , function() {
        $( this ).closest( '.row' ).remove();
    });
}

function upload() {
    if ($(".custom-file-input").length <1  ) return;
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
	document.getElementById("img-upload").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("upload-thumbnail").src = e.target.result;
        $( '#thumbnail-wrapper' ).removeClass( 'd-none' );
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};

}