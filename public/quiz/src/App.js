import React from 'react';
import './App.css';
import axios from 'axios';

var duration = 0;
const UserCredentials = JSON.parse( document.getElementById( 'user_credentials' ).value );
const Endpoint = document.getElementById( 'api_endpoint' ).value;
const testId = document.getElementById( 'test_id' ).value;
const minScore = document.getElementById( 'min_score' ).value;
var distance = document.getElementById( 'distance' ).value * 1000;
// const UserCredentials = { 'email': 'vmxitaryan@gmail.com' , 'password': 'kjpass' };
// const Endpoint = 'https://speakland.pro/api';
// const testId = '9';
// const minScore = 50;
// var distance = 10 * 1000;

export default class App extends React.Component {
  constructor ( props ) {
    super ( props );
  }
  state = {
    questions: [],
    answers: [],
    minutes: "00",
    seconds: "00",
    token: '',
    user: {},
    question_index: 1 ,
    question: null,
    lastAnswerQuestionId: null,
    lastAnswerIndex: null,
    finishedQuestions: null,
    correctAnswers: 0,
    view: 1,
    durationMinutes: 0,
    durationSeconds: 0,
    elapsed: false,
    duration: 0
  }
  handleChange ( e ) {
    this.setState({
      message: e.target.value
    });
  }
  async getQuestions () {
    let res = await axios.get( `${Endpoint}/questions/${testId}` , {
      headers: {
        Authorization: 'Bearer ' + this.state.token
      }
    });
    let questions = res.data;
    this.setState({
      questions,
      question: questions[ 0 ]
    });
  }

  async authenticate () {
    let res = await axios.post( `${Endpoint}/auth` , UserCredentials );
    console.log( res );
    let user = await axios.get( `${Endpoint}/user` , {
      headers: {
        Authorization: 'Bearer ' + res.data.token
      }
    });
    this.setState({ user: user.data , token: res.data.token });
  }
  async componentDidMount () {
    if ( undefined == UserCredentials || null == UserCredentials ) {
      alert( 'Fatal error' );
      return false;
    }
    await this.authenticate();
    await this.getQuestions();
    this.countdown( duration );
  }
  nextQuestion () {
    let question = this.state.questions[ this.state.question_index ];
    let question_index = this.state.question_index + 1;
    this.setState({
      question,
      question_index
    });
  }
  viewQuestions () {
    this.setState({
      view: 3
    });
  }
  restartQuiz () {
    window.location.reload();
  }
  finish ( elapsed = false ) {
    clearInterval( this.countdown );
    let corrects = 0;
    let newQuestions = this.state.questions;
    let finishedQuestions = {};
    newQuestions.forEach( ( element , quIndex ) => {
      element.answers.forEach( ( ans , index ) => {
        if ( ans.right ) {
          newQuestions[ quIndex ].rightAnswer = { [index] : ans };
          if ( undefined !== element.choices ) {
            if ( ans.id == Object.values( element.choices )[ 0 ][ 'id' ] ) {
              newQuestions[ quIndex ][ 'correct' ] = 1;
              corrects++;
            } else {
              newQuestions[ quIndex ][ 'correct' ] = 0;
            }
          }
        }
      });
      finishedQuestions[ element.id ] = newQuestions[ quIndex ];
    });
    this.setState({
      finishedQuestions,
      elapsed,
      correctAnswers: corrects,
      view: 2
    });
    console.log(finishedQuestions)
  }
  answering ( index , ans ) {
    let newQuestions = this.state.questions;
    newQuestions[ this.state.question_index - 1 ][ 'choices' ] = { [index]: newQuestions[ this.state.question_index - 1 ][ 'answers' ][ index ] };
    this.setState({
      questions: newQuestions,
      lastAnswerQuestionId: ans.question_id,
      lastAnswerIndex: index
    });
  }

  async gotoNextMission () {
    let result = this.state.correctAnswers / this.state.questions.length * 100;
    let res = await axios.get( `${Endpoint}/passed/${testId}/${result}` , {
      headers: {
        Authorization: 'Bearer ' + this.state.token
      }
    });
    // console.log(res.data)
    window.location.assign( res.data.url );
  }

  countdown ( time ) {
    var self = this;
    this.countdown = setInterval( function () {
      duration += 1;
      let durationMinutes = self.state.durationMinutes;
      let durationSeconds = duration;
      if ( duration == 15 ) {
        durationMinutes = self.state.durationMinutes + 1;
        durationSeconds = 0;
        duration = 0;
      }
      if ( durationSeconds < 10 ) {
        durationSeconds = "0" + durationSeconds;
      }
      var now = new Date().getTime();
      distance -= 1000;
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      if ( minutes < 10 ) minutes = "0" + minutes;
      if ( seconds < 10 ) seconds = "0" + seconds;
      self.setState({
        minutes,
        seconds,
        duration,
        durationMinutes,
        durationSeconds
      })
      if ( distance <= 0 ) {
        self.finish( true );
      }
    }, 1000);

    }
  render() {
    return (
        <div className="App">
            {
                  this.state.view == 2 || this.state.view == 3 ?
                  <div className="question-indicator text-theme mt-3 mt-md-4">
                    { this.state.correctAnswers } OF { this.state.questions.length } QUESTIONS ANSWERED CORRECTLY
                  </div>
                  :
                  <div className="question-indicator text-theme mt-3 mt-md-4">
                    QUESTION { this.state.question_index } OF { this.state.questions.length }
                  </div>
            }
            {
                this.state.view == 2 || this.state.view == 3 ?
                <div>
                  <div className="question-timer-wrapper mb-md-4 mb-3 text-theme">
                    <div className="d-inline-block mr-3">
                      Your time:
                    </div>
                    <div className="d-inline-block">
                      <h3 id="timer-wrap" className="text-theme">
                        <span id="timer-min" className=" text-theme">{this.state.durationMinutes < 10 ? ( "0" + this.state.durationMinutes ) : this.state.durationMinutes }</span>
                        <span>:</span>
                        <span id="timer-sec" className=" text-theme">{this.state.durationSeconds}</span>
                      </h3>
                    </div>
                  </div>
                  {
                    this.state.elapsed
                    &&
                    <div className="mt-4">
                      <b>TIME HAS ELAPSED</b>
                    </div>
                  }
                </div>
                :
                <div className="question-timer-wrapper mb-md-4 mb-3 text-theme">
                  <div className="d-inline-block mr-3">
                    Time limit:
                  </div>
                  <div className="d-inline-block">
                    <h3 id="timer-wrap" className="text-theme">
                      <span id="timer-min" className=" text-theme">{ this.state.minutes }</span>
                      <span>:</span>
                      <span id="timer-sec" className=" text-theme">{ this.state.seconds }</span>
                    </h3>
                  </div>
                </div>
            }
          <div className="mt-4 mt-md-5">
          {
            this.state.question && this.state.view == 1 ?
            <div>
                    <div className="question-title">
                      <h5 className="text-theme">
                        { this.state.question.title }
                      </h5>
                    </div>
                    <div className="answers-wrapper text-left">
                      <div className="my-md-4 my-3 text-theme">
                        {
                          this.state.question.answers.map( ( ans , ind ) => {
                            return <div className="custom-control custom-radio" key={ind}>
                            <input checked={ this.state.lastAnswerQuestionId == ans.question_id && ind == this.state.lastAnswerIndex } onChange={ () => this.answering( ind , ans ) } type="radio" id={ 'answerRadio_' + ind  } name="customRadio" className="custom-control-input" />
                            <label className="custom-control-label" htmlFor={ 'answerRadio_' + ind }>{ ans.title }</label>
                            </div>
                          })
                        }
                      </div>
                    </div>
                </div>
            : null

          }
            </div>

            {
              ( this.state.view == 2 || this.state.view == 3 ) &&
              <div>
                  <div className="progressResults mb-4">
                    <div className="d-block d-lg-flex text-center justify-content-center">
                      <span className="mr-3 mb-4 mb-lg-0 text-center "><b>AVERAGE RESULT</b></span>
                      <div className="progress mx-auto mx-lg-0 w-100 mt-1 pt-1">
                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: ( this.state.correctAnswers / this.state.questions.length * 100 ) + '%' }}></div>
                      </div>
                      <span className="ml-3 mb-4 mb-lg-0 text-center d-none d-lg-block"><b>{ this.state.correctAnswers / this.state.questions.length * 100 }%</b></span>
                    </div>
                  </div>
                  <div className="progressResults mb-4">
                    <div className="d-block d-lg-flex text-center justify-content-center">
                      <span className="mr-3 mb-4 mb-lg-0 text-center "><b>YOUR SCORE</b></span>
                      <div className="progress mx-auto mx-lg-0 w-100 mt-1 pt-1">
                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: ( this.state.correctAnswers / this.state.questions.length * 100 ) + '%' }}></div>
                      </div>
                      <span className="ml-3 mb-4 mb-lg-0 text-center d-none d-lg-block"><b>{ this.state.correctAnswers / this.state.questions.length * 100 }%</b></span>
                    </div>
                  </div>
              </div>
            }
          {
            this.state.view == 3 ?
                  <div>
                  { this.state.questions.map( ( el , index ) => {
                      return <div>
                    <div className="question-title text-theme">
                      <h5>
                        { el.title }
                      </h5>
                      </div>
                      <div className="answers-wrapper text-left">
                        <div className="my-md-4 my-3 text-theme">
                          {
                            el.answers.map( ( ans , ind ) => {
                              return <div>
                              <div className={ ans.right ? 'bg-success custom-control custom-radio' : 'custom-control custom-radio' }>
                                <input checked={ undefined !== this.state.finishedQuestions[ ans.question_id ][ 'choices' ][ ind ] } disabled="" type="radio" id={ 'answerRadio_' + ans.id  } name={ 'answerRadioName_' + ans.id  } className="custom-control-input" />
                                <label className="custom-control-label" htmlFor={ 'answerRadio_' + ans.id }>{ ans.title }</label>
                              </div>
                              
                              </div>
                            })
                          }
                          <div className="jumbotron py-4 w-75 mx-auto my-md-5 my-3">
                                {
                                  this.state.finishedQuestions[ el.id ][ 'correct' ] ?
                                  <div className="alert alert-success my-0">Correct answer</div>
                                  :
                                <div className="alert alert-danger my-0">Incorrect answer</div>
                              }
                              </div>
                        </div>
                      </div>
                      </div>
                      })
                }
                </div>
                : null
              
          }
          <div className="question-next-btn-wrapper">
            {
              this.state.view == 1 ?
              undefined !== this.state.questions && undefined !== this.state.questions[ this.state.question_index - 1 ] && undefined !== this.state.questions[ this.state.question_index - 1 ][ 'choices' ] ?
              this.state.questions.length == this.state.question_index ?
                <button onClick={ () => this.finish() } className="btn btn-outline-success">Finish</button>
              :
                <button onClick={ () => this.nextQuestion() } className="btn btn-outline-success">Next</button>
              : null
              : null
            }
            {
              this.state.view == 2 || this.state.view == 3 ?
              <div className="d-block d-lg-flex">
                <button onClick={ () => this.restartQuiz() } className="btn btn-outline-success long mx-lg-4 my-3 my-lg-0 mx-auto d-block flex-1">Restart Quiz</button>
                {
                  ( ( this.state.correctAnswers / this.state.questions.length * 100 ) >= minScore ) &&
                  <button onClick={ () => this.gotoNextMission() } className="btn btn-outline-success longer hovered mx-auto d-block flex-2">Кликните сюда, чтобы продолжить</button>
                }
              </div>
              : null
            }
          </div>
        </div>
    );
  }
}

// <button onClick={ () => this.viewQuestions() } className="btn btn-outline-success long mx-auto d-block flex-1">View questions</button>