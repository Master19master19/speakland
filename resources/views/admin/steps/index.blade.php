@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<a href="{{ route( 'admin_steps.create' ) }}" class="mb-3 btn btn-xl btn-info"><i class="fa fa-plus"></i> Добавить</a>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Название
				</th>
				<th>
					Секция
				</th>
				<th>
					Миссия
				</th>
				<th>
					Описание
				</th>
				<th>
					Порядок
				</th>
				<th>
					Длительность
				</th>
				<th>
					Цена прохождения
				</th>
				<!-- <th>
					Картинка
				</th>
				<th>
					PDF
				</th>
				<th>
					Видео
				</th>
				<th>
					Аудио
				</th> -->
				<th>
					Изменено
				</th>
				<th>
					<i class="fa fa-wrench"></i>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $steps as $step )
				<tr>
					<td>{{ $step -> id }}</td>
					<td>{{ $step -> title }}</td>
					<td><a href="{{ route( 'admin_sections.edit' , $step -> section -> id ) }}">{{ $step -> section -> title }}</a></td>
					<td><a href="{{ route( 'admin_missions.edit' , $step -> mission -> id ) }}">{{ $step -> mission -> title }}</a></td>
					<td>{{ $step -> description }}</td>
					<td>{{ $step -> order }}</td>
					<td>{{ $step -> duration }}</td>
					<td>{{ $step -> pass_price }} {{ env( 'CURRENCY_TITLE' ) }}</td>
					<!-- <td>
						@if ( $step -> img_url )
							<img src="{{ $step -> img_url }}" width="75" />
						@endif
					</td>
					<td>
						@if ( $step -> pdf_url )
							<a href="{{ $step -> pdf_url }}">{{ $step -> pdf_url }}</a>
						@endif
					</td>
					<td>
						@if ( $step -> video_url )
							<video controls="" height="100" src="{{ $step -> video_url }}"></video>
						@endif
					</td>
					<td>
						@if ( $step -> audio_url )
							<audio controls="" src="{{ $step -> audio_url }}"></audio>
						@endif
					</td> -->
					<td>{{ date( 'Y-m-d H:i' , strtotime( $step -> updated_at ) ) }}</td>
					<td>
						<a class="btn btn-sm btn-warning" href="{{ route( 'admin_steps.edit' , $step -> id ) }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-sm btn-danger" onClick="return confirm( 'Вы уверены?' );" href="{{ route( 'admin_steps.delete' , $step -> id ) }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div>
		{{ $steps -> links() }}
	</div>
</div>
@endsection