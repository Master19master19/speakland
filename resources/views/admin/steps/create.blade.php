@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

	<form class="form" action="{{ route( 'admin_steps.store' ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ old( 'title' ) }}" />
		</div>
		<div class="form-group">
			<label>Транскрипция</label>
			<input class="form-control" type="text" name="transcription" value="{{ old( 'transcription' ) }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ old( 'description' ) }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Секция</label>
			<select class="form-control" required="" name="section_id">
				@foreach( $sections as $section )
					<option value="{{ $section -> id }}">{{ $section -> title }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Цена прохождения ({{ env( 'CURRENCY_TITLE' ) }})</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="pass_price" value="{{ old( 'pass_price' ) }}" />
		</div>

		<div class="form-group">
			<label>Длительность в секундах</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="duration_seconds" value="{{ old( 'duration_seconds' ) }}" />
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ old( 'order' ) }}" />
		</div>
		<!-- <div class="form-group text-center d-none" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="img" accept="image/*" class="custom-file-input" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Картинка</label>
			</div>
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="video" class="custom-file-input" accept="video/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Видео</label>
			</div>
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="audio" class="custom-file-input" accept="audio/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Audio</label>
			</div>
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="pdf" class="custom-file-input" accept="application/pdf,application/vnd.ms-excel" id="img-upload" />
				<label class="custom-file-label" for="img-upload">PDF</label>
			</div>
		</div> -->

		<div class="mt-3 mt-md-4">
			<hr>
			<h2 class="text-center"><b>Медиа</b></h2>
			@include( 'admin.steps.media' )
			<hr>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Создать</button>
		</div>
	</form>
</div>
@endsection