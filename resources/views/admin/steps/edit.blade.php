@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $step -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'admin_steps.update' , $step -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<input type="hidden" name="id" value="{{ $step -> id }}" />
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $step -> title }}" />
		</div>
		<div class="form-group">
			<label>Транскрипция</label>
			<input class="form-control" type="text" name="transcription" value="{{ $step -> transcription }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ $step -> description }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Секция</label>
			<select class="form-control" required="" name="section_id">
				@foreach( $sections as $section )
					<option value="{{ $section -> id }}" @if ( $step -> section_id == $section -> id ) selected @endif>
						{{ $section -> title }}
					</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Цена прохождения ({{ env( 'CURRENCY_TITLE' ) }})</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="pass_price" value="{{ $step -> pass_price }}" />
		</div>

		<div class="form-group">
			<label>Длительность в секундах</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="duration_seconds" value="{{ $step -> duration_seconds }}" />
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ $step -> order }}" />
		</div>
		<!-- <div class="form-group text-center" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" src="{{ $step -> img_url }}" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="img" accept="image/*" class="custom-file-input" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Картинка</label>
			</div>
		</div>
		<div class="form-group text-center" id="thumbnail-wrapper">
			@if ( $step -> video_url )
				<video controls="" src="{{ $step -> video_url }}"></video>
			@endif
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="video" class="custom-file-input" accept="video/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Видео</label>
			</div>
		</div>
		<div class="form-group text-center" id="thumbnail-wrapper">
			@if ( $step -> audio_url )
				<audio controls="" src="{{ $step -> audio_url }}"></audio>
			@endif
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="audio" class="custom-file-input" accept="audio/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Audio</label>
			</div>
		</div>
		<div class="form-group text-center" id="thumbnail-wrapper">
			@if ( $step -> pdf_url )
				<iframe style="display: block;height: 500px;width: 100%;" class="mb-3" src="{{ $step -> pdf_url }}"></iframe>
				<a download="" href="{{ $step -> pdf_url }}">{{ $step -> pdf_url }}</a>
			@endif
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="pdf" class="custom-file-input" accept="application/pdf,application/vnd.ms-excel" id="img-upload" />
				<label class="custom-file-label" for="img-upload">PDF</label>
			</div>
		</div> -->
		<div class="mt-3 mt-md-4">
			<hr>
			<h2 class="text-center"><b>Медиа</b></h2>
			@include( 'admin.steps.media' , [ 'media' => $step -> media ] )
			<hr>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>


<!-- <h3 class="text-center">Предпоказ</h3>
<hr>
<iframe style="position: relative;left: -25%;" src="{{ route( 'account.step' , $step -> id ) }}#main-view" width="100%" height="800px"></iframe> -->
@endsection