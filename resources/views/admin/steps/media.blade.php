<div class="mb-4">
	<div class="dynamic-container">
		@isset ( $media )
			@foreach( $media as $key => $med )
				<div class="row">
					<div class="form-group col-md">
						<select class="form-control" required="" disabled="" name="media[{{ $med -> id }}][type]">
							<option disabled="" selected="" value="">Тип медиа</option>
							<option {{ $med -> type == 'text' ? 'selected' : '' }} value="text">Текст</option>
							<option {{ $med -> type == 'image' ? 'selected' : '' }} value="image">Картинка</option>
							<option {{ $med -> type == 'audio' ? 'selected' : '' }} value="audio">Аудио</option>
							<option {{ $med -> type == 'video' ? 'selected' : '' }} value="video">Видео</option>
							<option {{ $med -> type == 'pdf' ? 'selected' : '' }} value="pdf">PDF</option>
						</select>
					</div>
					<div class="form-group col-md">
						<input type="number" placeholder="Порядок"  value="{{ $med -> order }}" class="form-control" required="" min="1" name="media[{{ $med -> id }}][order]" />
					</div>
					<div class="form-group col-md">
						@if ( $med -> type == 'text' )
							<textarea rows="1" class="form-control" disabled="" placeholder="Описание">{{ $med -> content }}</textarea>
						@else
							@if ( $med -> type == 'pdf' )
								<div class="form-group text-center" id="thumbnail-wrapper">
									<iframe style="display: block;height: 500px;width: 100%;" class="mb-3" src="{{ $med -> file_url }}"></iframe>
									<a download="" href="{{ $med -> file_url }}">{{ $med -> file_url }}</a>
								</div>
							@elseif ( $med -> type == 'audio' )
								<div class="form-group text-center" id="thumbnail-wrapper">
									<audio controls="" src="{{ $med -> file_url }}"></audio>
								</div>
							@elseif ( $med -> type == 'video' )
								<div class="form-group text-center" id="thumbnail-wrapper">
									<video controls="" src="{{ $med -> file_url }}"></video>
								</div>
							@elseif ( $med -> type == 'image' )
								<div class="form-group text-center" id="thumbnail-wrapper">
									<img width="200" id="upload-thumbnail" src="{{ $med -> file_url }}" />
								</div>
							@endif
						@endif
					</div>
					<div class="form-check col-md-1">
					  <button type="button" data-delete class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button>
					</div>
				</div>
			@endforeach
		@else
			<div class="row row-parent">
				<div class="form-group col-md">
					<select class="form-control" data-media-type required="" name="media[1][type]">
						<option selected="" disabled="" value="">Тип медиа</option>
						<option value="text">Текст</option>
						<option value="image">Картинка</option>
						<option value="audio">Аудио</option>
						<option value="video">Видео</option>
						<option value="pdf">PDF</option>
					</select>
				</div>
				<div class="form-group col-md">
					<input type="number" placeholder="Порядок" class="form-control" required="" min="1" name="media[1][order]" />
				</div>
				<div class="form-group col-md col-dynamic">
					<textarea rows="3" class="form-control" name="media[1][content]" placeholder="Введите текст"></textarea>
				</div>
				<div class="form-check col-md-1">
				  <button type="button" data-delete class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button>
				</div>
			</div>
		@endisset
	</div>
	<div>
		<button type="button" data-add-media class="btn btn-primary"><i class="fa fa-plus"></i> Добавить медиа</button>
	</div>
</div>