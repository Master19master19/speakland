@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

	<form class="form" action="{{ route( 'admin_sections.store' ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ old( 'title' ) }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ old( 'description' ) }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Миссия</label>
			<select class="form-control" required="" name="mission_id">
				@foreach( $missions as $mission )
					<option value="{{ $mission -> id }}">{{ $mission -> title }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Цена прохождения ({{ env( 'CURRENCY_TITLE' ) }})</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="pass_price" value="{{ old( 'pass_price' ) }}" />
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ old( 'order' ) }}" />
		</div>
		<div class="form-group text-center d-none" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="file" class="custom-file-input" accept="image/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Файл</label>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Создать</button>
		</div>
	</form>
</div>
@endsection