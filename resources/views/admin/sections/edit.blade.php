@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $section -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'admin_sections.update' , $section -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<input type="hidden" name="id" value="{{ $section -> id }}" />
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $section -> title }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ $section -> description }}</textarea>
		</div>
		<div class="form-group">
			<label>Миссия</label>
			<select class="form-control" required="" name="mission_id">
				@foreach( $missions as $mission )
					<option value="{{ $mission -> id }}" @if( $mission -> id == $section -> mission_id ) selected @endif>
						{{ $mission -> title }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Цена прохождения ({{ env( 'CURRENCY_TITLE' ) }})</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="pass_price" value="{{ $section -> pass_price }}" />
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ $section -> order }}" />
		</div>
		<div class="form-group text-center" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" src="{{ $section -> img_url }}" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="file" class="custom-file-input"  accept="image/*" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Choose file</label>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>
@endsection