@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<a href="{{ route( 'admin_sections.create' ) }}" class="mb-3 btn btn-xl btn-info"><i class="fa fa-plus"></i> Добавить</a>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Название
				</th>
				<th>
					Картинка
				</th>
				<th>
					Миссия
				</th>
				<th>
					Описание
				</th>
				<th>
					Порядок
				</th>
				<th>
					Цена прохождения
				</th>
				<th>
					Изменено
				</th>
				<th>
					<i class="fa fa-wrench"></i>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $sections as $section )
				<tr>
					<td>{{ $section -> id }}</td>
					<td>{{ $section -> title }}</td>
					<td>
						<img src="{{ $section -> img_url }}" width="75" />
					</td>
					<td><a href="{{ route( 'admin_missions.edit' , $section -> mission -> id ) }}">{{ $section -> mission -> title }}</a></td>
					<td>{{ $section -> description }}</td>
					<td>{{ $section -> order }}</td>
					<td>{{ $section -> pass_price }} {{ env( 'CURRENCY_TITLE' ) }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $section -> updated_at ) ) }}</td>
					<td>
						<a class="btn btn-sm btn-info" href="{{ route( 'admin_steps.section' , $section -> id ) }}"><i class="fa fa-eye"></i> Шаги <sup>{{ $section -> steps() -> count() }}</sup></a>
						<a class="btn btn-sm btn-warning" href="{{ route( 'admin_sections.edit' , $section -> id ) }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-sm btn-danger" onClick="return confirm( 'Вы уверены?' );" href="{{ route( 'admin_sections.delete' , $section -> id ) }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div>
		{{ $sections -> links() }}
	</div>
</div>
@endsection