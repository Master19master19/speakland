@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Пользователь
				</th>
				<th>
					Создано
				</th>
				<th>
					Оплачено
				</th>
				<th>
					Сумма
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $payments as $payment )
				<tr>
					<td>{{ $payment -> id }}</td>
					<td>{{ $payment -> user -> name }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $payment -> created_at ) ) }}</td>
					<td>{{ $payment -> paid ? 'Да' : 'Нет' }}</td>
					<td>{{ $payment -> amount }}</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4">
					<b>Итого оплачено:</b>
				</td>
				<td>
					<b>{{ $sum }}</b>
				</td>
			</tr>
		</tfoot>
	</table>
	<div>
		{{ $payments -> links() }}
	</div>
</div>
@endsection