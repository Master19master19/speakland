@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<a href="{{ route( 'admin_missions.create' ) }}" class="mb-3 btn btn-xl btn-info"><i class="fa fa-plus"></i> Добавить</a>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Название
				</th>
				<th>
					Картинка
				</th>
				<th>
					Описание
				</th>
				<th>
					Порядок
				</th>
				<th>
					Изменено
				</th>
				<th>
					<i class="fa fa-wrench"></i>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $missions as $mission )
				<tr>
					<td>{{ $mission -> id }}</td>
					<td>{{ $mission -> title }}</td>
					<td>
						<img src="{{ $mission -> img_url }}" width="75" />
					</td>
					<td>{{ $mission -> description }}</td>
					<td>{{ $mission -> order }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $mission -> updated_at ) ) }}</td>
					<td>
						<a class="btn btn-sm btn-info" href="{{ route( 'admin_sections.mission' , $mission -> id ) }}"><i class="fa fa-eye"></i> Секции <sup>{{ $mission -> sections() -> count() }}</sup></a>
						<a class="btn btn-sm btn-info" href="{{ route( 'admin_steps.mission' , $mission -> id ) }}"><i class="fa fa-eye"></i> Шаги <sup>{{ $mission -> steps() -> count() }}</sup></a>
						<a class="btn btn-sm btn-warning" href="{{ route( 'admin_missions.edit' , $mission -> id ) }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-sm btn-danger" onClick="return confirm( 'Вы уверены?' );" href="{{ route( 'admin_missions.delete' , $mission -> id ) }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div>
		{{ $missions -> links() }}
	</div>
</div>
@endsection