@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $mission -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'admin_missions.update' , $mission -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<input type="hidden" name="id" value="{{ $mission -> id }}" />
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $mission -> title }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ $mission -> description }}</textarea>
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ $mission -> order }}" />
		</div>
		<div class="form-group text-center" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" src="{{ $mission -> img_url }}" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input type="file" name="file"  accept="image/*" class="custom-file-input" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Choose file</label>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>
@endsection