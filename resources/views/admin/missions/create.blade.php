@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

	<form class="form" action="{{ route( 'admin_missions.store' ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ old( 'title' ) }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ old( 'description' ) }}</textarea>
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ old( 'order' ) }}" />
		</div>
		<div class="form-group text-center d-none" id="thumbnail-wrapper">
			<img width="200" id="upload-thumbnail" />
		</div>
		<div class="form-group">
			<div class="custom-file">
				<input required="" type="file" name="file"  accept="image/*" class="custom-file-input" id="img-upload" />
				<label class="custom-file-label" for="img-upload">Файл</label>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Создать</button>
		</div>
	</form>
</div>
@endsection