@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $quiz -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'admin_quizzes.update' , $quiz -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<input type="hidden" name="id" value="{{ $quiz -> id }}" />
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $quiz -> title }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ $quiz -> description }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Миссия</label>
			<select class="form-control" required="" name="mission_id">
				@foreach( $missions as $mission )
					<option value="{{ $mission -> id }}" @if ( $quiz -> mission_id = $mission -> id ) selected @endif>{{ $mission -> title }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Минимальная отметка</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="min_score" value="{{ $quiz -> min_score }}" />
		</div>
		<div class="form-group">
			<label>Цена прохождения ({{ env( 'CURRENCY_TITLE' ) }})</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="pass_price" value="{{ $quiz -> pass_price }}" />
		</div>

		<div class="form-group">
			<label>Длительность в секундах</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="duration_seconds" value="{{ $quiz -> duration_seconds }}" />
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>
@endsection