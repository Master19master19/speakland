@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<a href="{{ route( 'admin_quizzes.create' ) }}" class="mb-3 btn btn-xl btn-info"><i class="fa fa-plus"></i> Добавить</a>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Название
				</th>
				<th>
					Миссия
				</th>
				<th>
					Описание
				</th>
				<th>
					Длительность
				</th>
				<th>
					Цена прохождения
				</th>
				<th>
					Мин. отметка
				</th>
				<th>
					Изменено
				</th>
				<th>
					<i class="fa fa-wrench"></i>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $quizzes as $quiz )
				<tr>
					<td>{{ $quiz -> id }}</td>
					<td>{{ $quiz -> title }}</td>
					<td><a href="{{ route( 'admin_missions.edit' , $quiz -> mission -> id ) }}">{{ $quiz -> mission -> title }}</a></td>
					<td>{{ $quiz -> description }}</td>
					<td>{{ $quiz -> duration }}</td>
					<td>{{ $quiz -> pass_price }} {{ env( 'CURRENCY_TITLE' ) }}</td>
					<td>{{ $quiz -> min_score }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $quiz -> updated_at ) ) }}</td>
					<td>
						<a class="btn btn-sm btn-info" href="{{ route( 'admin_questions.quiz' , $quiz -> id ) }}"><i class="fa fa-eye"></i> Вопросы <sup>{{ $quiz -> questions() -> count() }}</sup></a>
						<a class="btn btn-sm btn-warning" href="{{ route( 'admin_quizzes.edit' , $quiz -> id ) }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-sm btn-danger" onClick="return confirm( 'Вы уверены?' );" href="{{ route( 'admin_quizzes.delete' , $quiz -> id ) }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div>
		{{ $quizzes -> links() }}
	</div>
</div>
@endsection