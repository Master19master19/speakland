@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
    <input type="hidden" id='user_credentials' value="{{ json_encode([ 'email' => Auth::user() -> email , 'password' => Auth::user() -> password_plain ]) }}" />
    <input type="hidden" id='api_endpoint' value='{{ env( "APP_URL" ) }}/api' />
    <input type="hidden" id='user_type' value='{{ Auth::user() -> type }}' />
    <h1 class="mb-3">@lang( 'Tickets' )</h1>
    <div>
        
<noscript>You need to enable JavaScript to run this app.</noscript><div id="root"></div><script>!function(e){function t(t){for(var n,l,a=t[0],f=t[1],i=t[2],p=0,s=[];p<a.length;p++)l=a[p],Object.prototype.hasOwnProperty.call(o,l)&&o[l]&&s.push(o[l][0]),o[l]=0;for(n in f)Object.prototype.hasOwnProperty.call(f,n)&&(e[n]=f[n]);for(c&&c(t);s.length;)s.shift()();return u.push.apply(u,i||[]),r()}function r(){for(var e,t=0;t<u.length;t++){for(var r=u[t],n=!0,a=1;a<r.length;a++){var f=r[a];0!==o[f]&&(n=!1)}n&&(u.splice(t--,1),e=l(l.s=r[0]))}return e}var n={},o={1:0},u=[];function l(t){if(n[t])return n[t].exports;var r=n[t]={i:t,l:!1,exports:{}};return e[t].call(r.exports,r,r.exports,l),r.l=!0,r.exports}l.m=e,l.c=n,l.d=function(e,t,r){l.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},l.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},l.t=function(e,t){if(1&t&&(e=l(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(l.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var n in e)l.d(r,n,function(t){return e[t]}.bind(null,n));return r},l.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return l.d(t,"a",t),t},l.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},l.p="/";var a=this.webpackJsonpchat=this.webpackJsonpchat||[],f=a.push.bind(a);a.push=t,a=a.slice();for(var i=0;i<a.length;i++)t(a[i]);var c=f;r()}([])</script><script src="/chat/build/static/js/2.bb3857d3.chunk.js"></script><script src="/chat/build/static/js/main.d6b6e77e.chunk.js"></script>
    </div>
</div>












<div class="container-fluid d-none">
    <div class="chat-wrapper">
        <div class="row">
            <div class="col-lg-2 col-3 col-md-4 col-sm-5 chat-parent-left">
                @for ( $x = 0; $x < 15; $x++ )
                    <div class="row my-2 my-lg-0 chat-logo @if ( $x == 0 ) active @endif">
                        <div class="col-3">
                            <img class="rounded-circle" src="https://scontent.fevn1-4.fna.fbcdn.net/v/t1.0-1/cp0/p60x60/74230426_571346833674459_8808535271881572352_n.jpg?_nc_cat=111&_nc_sid=7206a8&_nc_ohc=km9HSfAKjLwAX8nlQqx&_nc_ht=scontent.fevn1-4.fna&oh=ad6ab77e911193e36053debc016f55b7&oe=5E92B32F" />
                        </div>
                        <div class="col d-none d-sm-block">
                            <div>
                                <p class="my-2">Emmy Maranjyan</p>
                            </div>
                            <div>
                                <p class="my-2">Что скажете?</p>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
            <div class="col pt-3 chat-parent-right">
                <div class="d-flex d-flex flex-column h-100">
                    <div class="chat-message-container">
                        <div class="row">
                            <div class="col-lg-6 my-2">
                                <p class="py-2 mb-0 chat-left px-3">Text message</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 offset-lg-6 my-2">
                                <p class="py-2 mb-0 chat-right px-3">Text message</p>
                            </div>
                        </div>
                    </div>
                    <div class="chat-form mt-auto">
                        <div class="form">
                            <div class="row">
                                <div class="col-9 col-lg-11">
                                    <div class="form-group">
                                        <textarea rows='1' type="text" placeholder="@lang( 'Type a message...' )" name="chat-message" class="form-control form-control-lg" ></textarea>
                                    </div>
                                </div>
                                <div class="col mt-auto pointer">
                                    <div class="form-group">
                                        <span class=""><svg height="36px" width="36px" viewBox="0 0 36 36"><g fill="none" fill-rule="evenodd"><g><polygon points="0 36 36 36 36 0 0 0"></polygon><path d="M31.1059281,19.4468693 L10.3449666,29.8224462 C8.94594087,30.5217547 7.49043432,29.0215929 8.17420251,27.6529892 C8.17420251,27.6529892 10.7473302,22.456697 11.4550902,21.0955966 C12.1628503,19.7344961 12.9730756,19.4988922 20.4970248,18.5264632 C20.7754304,18.4904474 21.0033531,18.2803547 21.0033531,17.9997309 C21.0033531,17.7196073 20.7754304,17.5095146 20.4970248,17.4734988 C12.9730756,16.5010698 12.1628503,16.2654659 11.4550902,14.9043654 C10.7473302,13.5437652 8.17420251,8.34697281 8.17420251,8.34697281 C7.49043432,6.9788693 8.94594087,5.47820732 10.3449666,6.1775158 L31.1059281,16.553593 C32.298024,17.1488555 32.298024,18.8511065 31.1059281,19.4468693" fill="#20CEF5"></path></g></g></svg></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
