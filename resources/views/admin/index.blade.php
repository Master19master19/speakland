@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">Welcome</h1>
	<h2>
		Speakland admin {{ Auth::user() -> name }}
	</h2>
</div>
@endsection