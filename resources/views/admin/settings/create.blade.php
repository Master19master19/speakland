@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

	<form class="form" action="{{ route( 'currency_groups.store' ) }}" method="POST">
		@csrf
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ old( 'title' ) }}" />
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Создать</button>
		</div>
	</form>
</div>
@endsection