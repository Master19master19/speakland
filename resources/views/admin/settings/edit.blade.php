@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $currency_group -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'currency_groups.update' , $currency_group -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $currency_group -> title }}" />
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>
@endsection