@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>

	@if ( Session::has( 'status' ) )
		<p class="alert alert-info">{{ Session::get( 'status' ) }}</p>
	@endif

	<hr>
	<form class="form" action="{{ route( 'settings.store' ) }}" method="POST">
		@csrf
		@foreach ( $settings as $key => $setting )
			<div class="form-group">
				<label><b>{{ $setting -> title }}</b></label>
				<input type="text" style="font-weight: bold;" class="form-control" @if ( $key == 0 ) autofocus="" @endif name="{{ $setting -> code }}" value="{{ $setting -> value }}" />
			</div>
		@endforeach
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
	<hr>
</div>
@endsection