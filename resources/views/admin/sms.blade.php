
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@isset( $title ) {{ $title }} @else {{ config('app.name', 'Laravel') }} @endisset</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="favicon.png" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">
			<div class="container-fluid">
				<h1 class="mb-4">{{ $title }}</h1>
				<form class="form" method="POST" action="{{ route( 'admin.check_sms' ) }}">
					@csrf
					   @if($errors->any())
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                    @endif

					<div class="form-group">
						<input autofocus="" type="text" class="form-control" value="{{ old( 'ss' ) }}" name="ss" placeholder="Код из sms" required="" minlength="6" maxlength="6" min="111111" max="999999" />
					</div>
					<div class="form-group">
						<button class="btn btn-block btn-success">Проверить</button>
					</div>
				</form>
			</div>

        </main>
    </div>
</body>
</html>
