@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">Пользователи</h1>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Имя
				</th>
				<th>
					Почта
				</th>
				<th>
					Номер телефона
				</th>
				<th>
					Зарегистрирован
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $users as $user )
				<tr>
					<td>{{ $user -> id }}</td>
					<td>{{ $user -> name }}</td>
					<td>{{ $user -> email }}</td>
					<td>{{ $user -> phone }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $user -> created_at ) ) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection