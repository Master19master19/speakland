<div class="mb-4">
	<div class="dynamic-container-answers">
		@isset ( $answers )
			@foreach( $answers as $key => $answer )
				<div class="row">
					<div class="form-group col-md">
						<input type="text" placeholder="Название" class="form-control" required="" minlength="2" maxlength="200" name="answers[{{$key+900}}][title]" value="{{ $answer -> title }}" />
					</div>
					<div class="form-group col-md">
						<input type="number" placeholder="Порядок"  value="{{ $answer -> order }}" class="form-control" required="" min="1" name="answers[{{$key+900}}][order]" />
					</div>
					<div class="form-group col-md">
						<textarea rows="1" class="form-control" name="answers[{{$key+900}}][description]" placeholder="Описание">{{ $answer -> description }}</textarea>
					</div>
					<div class="form-check col-md-1">
						<label class="switch">
						  <input type="checkbox" name="answers[{{$key+900}}][right]" @if ( $answer[ 'right' ] == 1 ) checked @endif>
						  <span class="slider"></span>
						</label>
					  <label class="form-check-label" for="defaultCheck1">
					    Правильно
					  </label>
					</div>
					<div class="form-check col-md-1">
					  <button type="button" data-delete-answer class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button>
					</div>
				</div>
			@endforeach
		@else
			<div class="row">
				<div class="form-group col-md">
					<input type="text" placeholder="Название" class="form-control" required="" minlength="2" maxlength="200" name="answers[1][title]" />
				</div>
				<div class="form-group col-md">
					<input type="number" placeholder="Порядок" class="form-control" required="" min="1" name="answers[1][order]" />
				</div>
				<div class="form-group col-md">
					<textarea rows="1" class="form-control" name="answers[1][description]" placeholder="Описание"></textarea>
				</div>
				<div class="form-check col-md-1">
					<label class="switch">
					  <input type="checkbox" name="answers[1][right]">
					  <span class="slider"></span>
					</label>
				  <label class="form-check-label" for="defaultCheck1">
				    Правильно
				  </label>
				</div>
				<div class="form-check col-md-1">
				  <button type="button" data-delete-answer class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button>
				</div>
			</div>
		@endisset
	</div>
	<div>
		<button type="button" data-add-answer class="btn btn-primary"><i class="fa fa-plus"></i> Добавить ответ</button>
	</div>
</div>