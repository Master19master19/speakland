@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<a href="{{ route( 'admin_questions.create' ) }}" class="mb-3 btn btn-xl btn-info"><i class="fa fa-plus"></i> Добавить</a>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Название
				</th>
				<th>
					Тест
				</th>
				<th>
					Описание
				</th>
				<th>
					Длительность
				</th>
				<th>
					Порядок
				</th>
				<th>
					Изменено
				</th>
				<th>
					<i class="fa fa-wrench"></i>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $questions as $question )
				<tr>
					<td>{{ $question -> id }}</td>
					<td>{{ $question -> title }}</td>
					<td><a href="{{ route( 'admin_quizzes.edit' , $question -> quiz -> id ) }}">{{ $question -> quiz -> title }}</a></td>
					<td>{{ $question -> description }}</td>
					<td>{{ $question -> duration }}</td>
					<td>{{ $question -> order }}</td>
					<td>{{ date( 'Y-m-d H:i' , strtotime( $question -> updated_at ) ) }}</td>
					<td>
						<a class="btn btn-sm btn-warning" href="{{ route( 'admin_questions.edit' , $question -> id ) }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-sm btn-danger" onClick="return confirm( 'Вы уверены?' );" href="{{ route( 'admin_questions.delete' , $question -> id ) }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div>
		{{ $questions -> links() }}
	</div>
</div>
@endsection