@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }} #{{ $question -> id }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

    @if ( session( 'status' ) )
        <p class="alert alert-success">{{ session( 'status' ) }}</p>
    @endif
	<form class="form" action="{{ route( 'admin_questions.update' , $question -> id ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		@method( 'PUT' )
		<input type="hidden" name="id" value="{{ $question -> id }}" />
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ $question -> title }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ $question -> description }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Тест</label>
			<select class="form-control" required="" name="quiz_id">
				@foreach( $quizzes as $quiz )
					<option value="{{ $quiz -> id }}" @if ( $question -> quiz_id == $quiz -> id ) selected @endif>{{ $quiz -> title }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ $question -> order }}" />
		</div>

		<div class="form-group">
			<label>Длительность в секундах</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="duration_seconds" value="{{ $question -> duration_seconds }}" />
		</div>
		<div class="mt-3 mt-md-4">
			<hr>
			<h2 class="text-center"><b>Ответы</b></h2>
			@include( 'admin.questions.answer' , [ 'answers' => $answers ] )
			<hr>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Сохранить</button>
		</div>
	</form>
</div>
@endsection