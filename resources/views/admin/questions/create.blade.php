@extends( 'layouts.admin' )
@section( 'content' )

<div class="container-fluid">
	<h1 class="mb-4">{{ $title }}</h1>
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif

	<form class="form" action="{{ route( 'admin_questions.store' ) }}" enctype="multipart/form-data" method="POST">
		@csrf
		<div class="form-group">
			<label>Название</label>
			<input autofocus="" class="form-control" type="text" required="" name="title" value="{{ old( 'title' ) }}" />
		</div>
		<div class="form-group">
			<label>Описание</label>
			<textarea minlength="10" name="description" class="form-control">{{ old( 'description' ) }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Тест</label>
			<select class="form-control" required="" name="quiz_id">
				@foreach( $quizzes as $quiz )
					<option value="{{ $quiz -> id }}">{{ $quiz -> title }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Длительность в секундах</label>
			<input class="form-control" type="number" min="1" max="10000" required="" name="duration_seconds" value="{{ old( 'duration_seconds' ) }}" />
		</div>
		<div class="form-group">
			<label>Порядок</label>
			<input class="form-control" type="number" required="" name="order" value="{{ old( 'order' ) }}" />
		</div>
		<div class="mt-3 mt-md-4">
			<hr>
			<h2 class="text-center"><b>Ответы</b></h2>
			@include( 'admin.questions.answer' )
			<hr>
		</div>
		<div class="form-group">
			<button class="btn btn-block btn-primary">Создать</button>
		</div>
	</form>
</div>
@endsection