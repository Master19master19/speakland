<!DOCTYPE html>
<html>
<head>
	<title>{{ env( 'APP_NAME' ) }}</title>
	<meta charset="utf-8">
</head>
<body>
	<div style="">
		<img width="150" src="{{ env( 'APP_URL' ) }}/logo.png" />
	</div>
	<h1 style="text-align: center;">@lang( 'Password recovery' )</h1>
	<table border="1" cellpadding="5" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<th style="font-size: 20px;padding: 10px;"><b>@lang( 'Your code is:' ) {{ $code }}</b></th>
			</tr>
		</thead>
	</table>
</body>
</html>