<!DOCTYPE html>
<html>
<head>
	<title>Speak Land</title>
	<meta charset="utf-8">
</head>
<body style="font-family: sans-serif;">
	<div style="text-align: center;">
		<img style="width: 70px;height: 70px;margin: auto;display: block;" src="{{ env( 'APP_URL' ) }}/logo.png" />
	</div>
	<h2 style="text-align: center;">
		Поздравляем вас с успешной оплатой подписки! 
	</h2>
	<p style="font-size: 15px;">
		Ваши данные доступа: <br>
		Логин:<br>
		<b>{{ $data[ 'email' ] }}</b><br>
		Пароль:<br>
		<b>{{ $data[ 'password' ] }}</b><br>
	</p>
	<p style="font-size: 15px;">
		При желании вы можете изменить пароль в личном кабинете. 
	</p>
	<p style="font-size: 15px;">
		Для входа в личный кабинет перейдите по ссылке:
		<a href="{{ $data[ 'href' ] }}">{{ $data[ 'href' ] }}</a>
	</p>
	<p style="font-size: 13px;">
		С уважением, команда Speak Land
	</p>
</body>
</html>