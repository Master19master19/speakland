<!DOCTYPE html>
<html>
<head>
	<title>Speak Land</title>
	<meta charset="utf-8">
</head>
<body style="font-family: sans-serif;">
	<div style="text-align: center;">
		<img style="width: 70px;height: 70px;margin: auto;display: block;" src="{{ env( 'APP_URL' ) }}/logo.png" />
	</div>
	<h2 style="text-align: center;">
		Поздравляем! Вы на пол пути от начала изучения английского языка на уровне родного! 
	</h2>
	<p style="font-size: 15px;">
		Для подключения к платформе, вам необходимо оплатить подписку, ее стоимость составляет {{ env( 'PAYMENT_AMOUNT' ) }} USD в месяц.
	</p>
	<p style="font-size: 15px;">Для оплаты перейдите по ссылке: <a href="{{ $data[ 'href' ] }}">{{ $data[ 'href' ] }}</a></p>
	<p style="font-size: 15px;">
		После оплаты вы сразу же сможете начать обучение. Также вам на почту прийдут данные доступа к платформе. 
	</p>
	<p style="font-size: 13px;">
		С уважением, команда Speak Land
	</p>
</body>
</html>