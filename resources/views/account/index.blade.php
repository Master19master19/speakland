@extends( 'layouts.app' )
@section( 'head' )
<style type="text/css">
    #theme-light .mission {
        border: 1px solid black;
    }
    .card-img-top {
      height: 200px;
      width: 100%;
      object-fit: cover;
    }
</style>
@endsection
@section( 'pre_content' )
    <div>
        <h2 class="text-center my-4 text-theme">Missions</h2>
    </div>
@endsection
@section( 'content' )

                    <div class="card-deck d-flex justify-content-center row mt-5">
                        @foreach ( $missions as $x => $mission )
                          <div class="h-100 mission col-xl-3 {{ $x < 3 ? 'mb-4' : 'my-4' }} mx-md-4 text-center">
                            <img style="{{ Auth::user() -> canMission( $mission -> id ) ? '' : 'filter: gsrayscale(100%);' }}" class="card-img-top mt-3" src="{{ $mission -> img_url }}" alt="Card image cap">
                            <div class="px-0 card-body text-left">
                              <p style="min-height: 66px;" class="card-title text-theme mb-0">{{ $mission -> title }}</p>
                              <div class="d-flex flex-row justify-content-between">
                                @if ( Auth::user() -> canMission( $mission -> id ) == false )
                                  <a href="#" class="btn btn-outline-success w-75">Изучать</a>
                                @else
                                  <a href="{{ route( 'account.mission' , $mission -> id ) }}" class="btn btn-outline-success w-75">Изучать</a>
                                @endif
                                @if ( Auth::user() -> canMission( $mission -> id ) === 1 )
                                  <span class="icon-spinner"></span>
                                @elseif ( Auth::user() -> canMission( $mission -> id ) === true )
                                  <span class="icon-check"></span>
                                @else
                                  <span class="icon-lock"></span>
                                @endif
                              </div>
                            </div>
                          </div>
                        @endforeach
                    </div>

@endsection

@section( 'scripts' )
@endsection