@extends( 'layouts.app' )
@section( 'head' )
<style type="text/css">
	.imgstyle  {
		max-width: 250px;
	}
</style>
@endsection
@section( 'content' )
<div class=" mb-5 mb-lg-5 main-border-steps-sections-missions mission-progress-wrapper px-4 pb-3 pb-lg-4">
	<div class="row text-center pb-1">
		<div class="col-xl text-left d-flex flex-lg-row flex-column align-items-center align-items-lg-start">
			<img class="mt-4 imgstyle d-inline" src="{{ $mission -> img_url }}" alt="">
			<div class="text-center ml-lg-3 mt-lg-4 mt-3">
				<div class="text-center text-xl-left mb-2 mt-lg-2">
					<h3 class="text-theme">{{ $mission -> title }}</h3>
				</div>
				@include( 'components.progress' )
			</div>
		</div>
	</div>
</div>
<div class="mb-5 list-items-wrapper">
	<div class="list-items  bg-theme">
		<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
			<b>Sections</b>
		</p>
	</div>
	@foreach( $sections as $sec )
	<div class="list-items bg-theme">
		<a href="{{ Auth::user() -> canSection( $sec -> id ) ? route( 'account.section' , $sec -> id ) : '#' }}" >
			<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
				@if ( Auth::user() -> canSection( $sec -> id ) === true )
                    <span class="icon-check"></span>
				@elseif ( Auth::user() -> canSection( $sec -> id ) === 1 )
                    <span class="icon-spinner"></span>
				@else
                    <span class="icon-lock"></span>
				@endif
				{{ $sec -> title }}
				<span class="float-right mr-3 mt-2">{{ $sec -> stepCount() }} steps</span>
			</p>
		</a>
	</div>
	@endforeach
</div>
<div class="mb-5 list-items-wrapper">
	<div class="list-items  bg-theme">
		<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
			<b>Quizzes</b>
		</p>
	</div>
	<div class="list-items bg-theme">
		<a href="{{ Auth::user() -> canQuiz( $mission -> quiz -> id ) ? route( 'account.quiz' , $mission -> quiz -> id ) : '#' }}">
			<p class="text-theme py-2 my-0 px-2 px-md-0">
				@if ( Auth::user() -> canQuiz( $mission -> quiz -> id ) == true )
                    <span class="icon-check"></span>
				@elseif ( Auth::user() -> canQuiz( $mission -> quiz -> id ) == 1 )
                    <span class="icon-spinner"></span>
				@else
                    <span class="icon-lock"></span>
				@endif
				{{ $mission -> quiz -> title }}
			</p>	
		</a>
	</div>
</div>

@endsection

@section( 'scripts' )
@endsection