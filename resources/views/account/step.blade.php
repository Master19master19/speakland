@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )
						@if ( Session::has( 'status' ) )
							<p class="alert alert-info">{{ Session::get( 'status' ) }}</p>
						@endif

	<div class=" mb-5 mb-lg-5 main-border-steps-sections-missions mission-progress-wrapper px-4 pb-3 pb-lg-4">
		<div class="row text-center pb-1">
			<div class="col-xl text-left d-flex flex-lg-row flex-column align-items-center align-items-lg-start">
				<div class="text-center ml-lg-3 mt-lg-4 mt-3">
					<div class="text-center text-xl-left mb-2 mt-lg-2">
						<h3 class="text-theme">{{ $step -> title }}</h3>
	           			<a href="{{ route( 'safe.save' , [ $step -> id , 'step' ] ) }}" class="btn btn-outline-success mt-4 long">Добавить в Сейф</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center my-3">
			<div class="col-xl">
				@include( 'components.pagination' , [ "type" => 'step' ] )
			</div>
		</div>
	</div>
						  <div  class="main-border-steps-sections-missions mb-5">
					          <div id="main-view" class="bg-theme content_name mb-3 main-border-steps-sections-missions">
					          	@foreach( $step -> media as $key => $med )
					          		@if ( $med -> type == 'text' )
				          				<div class="my-2 mx-3 mx-lg-4">
					          					<pre class="text-theme mb-0 preformat">{!! $med -> content !!}</pre>
					          			</div>
					          		@elseif ( $med -> type == 'audio' )
					          			<div class="my-3 my-lg-4">
					          				<div class="row">
					          					<div class="col-md-3 d-flex justify-content-center mb-3 mb-md-0">
					          						<audio class="d-block d-md-inline" controls="" src="{{ $med -> file_url }}"></audio>
					          					</div>
					          					<div class="col-md-2 d-flex">
					          						<a download="" class="btn btn-outline-success long  my-auto mx-auto d-block d-md-inline" href="{{ $med -> file_url }}">Скачать Аудио</a>
					          					</div>
					          				</div>
					          			</div>
					          		@elseif ( $med -> type == 'pdf' )
					          			<div class="my-3 my-lg-4">
					          				<a download="" class="btn btn-outline-success long" href="{{ $med -> file_url }}">Скачать PDF</a>
					          			</div>
					          		@elseif ( $med -> type == 'video' )
					          			<div class="my-3 my-lg-4 mx-auto video-wrapper mb-4 pb-3">
											<div data-source="[240p]{{ str_replace( '1080' , '240' , $med -> file_url ) }},[480p]{{ str_replace( '1080' , '480' , $med -> file_url ) }},[720p]{{ str_replace( '1080' , '720' , $med -> file_url ) }},[1080p]{{ $med -> file_url }}" class="player"></div>
					          			</div>
					          		@elseif ( $med -> type == 'image' )
					          			<div class="my-3 my-lg-4">
					          				<img width="95%" style="max-height: 400px;object-fit: cover;" class="mr-auto d-block" src="{{ $med -> file_url }}" />
					          			</div>
					          		@endif
					          	@endforeach
				          		<div class="text-theme text-center mt-4 mb-1 py-2" id="next-step-wrap">
				          			<h3 id="timer-wrap" class="text-theme">
				          				<span id="timer-min"></span>
				          				<span>:</span>
				          				<span id="timer-sec"></span>
				          			</h3>
				          		</div>
					          </div>
					          <input type="hidden" value="{{ $step -> id }}" name="step_id" />
					          @if ( Auth::user() -> step -> id == $step -> id )
					          	<input type="hidden" value="{{ Auth::user() -> duration == 0 ? $step -> duration_seconds : Auth::user() -> duration }}" name="step_duration" />
					          @endif
					          <input type="hidden" value="countdown" name="measure" />
						</div>
						
						  <div  class="mission mb-5" style="display: none;">
					          <div class="bg-theme content_name mb-3">
									@include( 'components.pagination' , [ "type" => 'step' ] )
					          </div>
						</div>


@endsection

@section( 'scripts' )
	<script type="text/javascript" src="/playerjs.js"></script>
	<script type="text/javascript">
		let default_quality = localStorage.getItem( 'default_quality' );
		if ( null == default_quality ) {
			default_quality = "1080p";
			localStorage.setItem( 'default_quality' , default_quality );
		}
		function onQuality( event,id,data ){
			console.log(event,id,data)
		   if(event=="play"){
		      alert(event);
		   }
		   if(event == "time"){
		      console.log(event,id,data);
		   }
		}

		let x = 1;
		$( '.player' ).each( function() {
			let id = 'player_' + x;
			$( this ).attr( 'id' , id );
			x++;
			let source = $( this ).data( 'source' );
			let player = new Playerjs({
			  "id": id,
			  "file": source,
			  "default_quality": default_quality,
			  "pause": "onQuality"
			});
			document.getElementById( id ).addEventListener( "play" , onQuality );
		});
	</script>
	<script type="text/javascript" src="/step.js"></script>
@endsection