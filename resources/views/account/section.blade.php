@extends( 'layouts.app' )
@section( 'head' )
<style type="text/css">
	.imgstyle  {
		max-width: 250px;
	}
</style>
@endsection
@section( 'content' )
	@if ( Session::has( 'status' ) )
		<p class="alert alert-info">{{ Session::get( 'status' ) }}</p>
	@endif
	<div class=" mb-5 mb-lg-5 main-border-steps-sections-missions mission-progress-wrapper px-4 pb-3 pb-lg-4">
		<div class="row text-center pb-1">
			<div class="col-xl text-left d-flex flex-lg-row flex-column align-items-center align-items-lg-start">
				<img class="mt-4 imgstyle d-inline" src="{{ $section -> mission -> img_url }}" alt="">
				<div class="text-center ml-lg-3 mt-lg-4 mt-3">
					<div class="text-center text-xl-left mb-2 mt-lg-2">
						<h3 class="text-theme">{{ $section -> title }}</h3>
	           			<a href="{{ route( 'safe.save' , [ $section -> id , 'section' ] ) }}" class="btn btn-outline-success mt-4 long">Добавить в Сейф</a>
					</div>
				</div>
			</div>
		</div>
	</div>
						  <div  class="list-items-wrapper mb-5">
							<div class="list-items  bg-theme">
								<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
									<b>Section Steps</b>
								</p>
							</div>
					          @foreach( $steps as $step )
					          <div class="list-items bg-theme">
								  <a href="{{ Auth::user() -> canStep( $step -> id ) ? route( 'account.step' , $step -> id ) : '#' }}" >
								  	<p class="text-theme py-2 my-0 px-2 px-md-0">
									@if ( Auth::user() -> canStep( $step -> id ) === true )
                                  		<span class="icon-check"></span>
									@elseif ( Auth::user() -> canStep( $step -> id ) === 1 )
                                  		<span class="icon-spinner"></span>
									@else
                                  		<span class="icon-lock"></span>
									@endif
									  	{{ $step -> title }}
									  </p>
								  </a>
					          </div>
					          @endforeach
						</div>
						  <!-- <div  class="list-items-wrapper mb-5">
								<div class="list-items  bg-theme">
									<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
										<b>Quizzes</b>
									</p>
								</div>
							  <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">Final Quiz</p></div>
						</div> -->

@endsection

@section( 'scripts' )
@endsection