@extends( 'layouts.app' )
@section( 'head' )
<style type="text/css">
	.imgstyle  {
		max-width: 250px;
	}
</style>
@endsection
@section( 'content' )
						@if ( Session::has( 'status' ) )
							<p class="alert alert-info">{{ Session::get( 'status' ) }}</p>
						@endif
	<div class=" mb-5 mb-lg-5 main-border-steps-sections-missions mission-progress-wrapper px-4 pb-3 pb-lg-4">
		<div class="row text-center pb-1">
			<div class="col-xl text-left d-flex flex-lg-row flex-column align-items-center align-items-lg-start">
				<img class="mt-4 imgstyle d-inline" src="{{ $quiz -> mission -> img_url }}" alt="">
				<div class="text-center ml-lg-3 mt-lg-4 mt-3">
					<div class="text-center text-xl-left mb-2 mt-lg-2">
						<h3 class="text-theme">{{ $quiz -> mission -> title }} > {{ $quiz -> title }}</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center my-3">
			<div class="col-xl">
				@include( 'components.pagination' , [ "type" => 'quiz' ] )
			</div>
		</div>
	</div>
	<div  class="list-items-wrapper mb-5">
		<div class="list-items  bg-theme">
			<p class="text-theme text-left py-2 my-0 px-2 px-md-0">
				<b>{{ $quiz -> title }}</b>
			</p>
		</div>
		<div class="list-items  bg-theme">
			<div id="main-view" class="bg-theme">
          		<button class="btn-outline-success btn long">Start quiz</button>
          		<div class="text-theme text-center" id="quiz-wrapper">
      				<section id="root" class="text-left px-3 px-md-4"></section>
          		</div>
          </div>
		</div>
	</div>
    <input type="hidden" id='user_credentials' value="{{ json_encode([ 'email' => Auth::user() -> email , 'password' => Auth::user() -> password_plain ]) }}" />
    <input type="hidden" id='api_endpoint' value='{{ env( "APP_URL" ) }}/api' />
	<input type="hidden" id="test_id" value="{{ $quiz -> id }}" name="test_id" />
	<input type="hidden" id="distance" value="{{ $quiz -> duration_seconds }}" name="distance" />
	<input type="hidden" id="min_score" value="{{ $quiz -> min_score }}" name="min_score" />
@endsection

@section( 'scripts' )
<script>!function(e){function r(r){for(var n,i,l=r[0],f=r[1],a=r[2],c=0,s=[];c<l.length;c++)i=l[c],Object.prototype.hasOwnProperty.call(o,i)&&o[i]&&s.push(o[i][0]),o[i]=0;for(n in f)Object.prototype.hasOwnProperty.call(f,n)&&(e[n]=f[n]);for(p&&p(r);s.length;)s.shift()();return u.push.apply(u,a||[]),t()}function t(){for(var e,r=0;r<u.length;r++){for(var t=u[r],n=!0,l=1;l<t.length;l++){var f=t[l];0!==o[f]&&(n=!1)}n&&(u.splice(r--,1),e=i(i.s=t[0]))}return e}var n={},o={1:0},u=[];function i(r){if(n[r])return n[r].exports;var t=n[r]={i:r,l:!1,exports:{}};return e[r].call(t.exports,t,t.exports,i),t.l=!0,t.exports}i.m=e,i.c=n,i.d=function(e,r,t){i.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:t})},i.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},i.t=function(e,r){if(1&r&&(e=i(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var t=Object.create(null);if(i.r(t),Object.defineProperty(t,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var n in e)i.d(t,n,function(r){return e[r]}.bind(null,n));return t},i.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return i.d(r,"a",r),r},i.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},i.p="/";var l=this.webpackJsonpquiz=this.webpackJsonpquiz||[],f=l.push.bind(l);l.push=r,l=l.slice();for(var a=0;a<l.length;a++)r(l[a]);var p=f;t()}([])</script><script src="/quiz/build/static/js/2.5e897e49.chunk.js"></script><script src="/quiz/build/static/js/main.b6ef3477.chunk.js"></script>
@endsection