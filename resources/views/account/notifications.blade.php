@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )
          <div class="bg-theme content_name  mb-3">
            <h3 class="text-theme">Уведомления</h3>
          </div>
          <div class=" mb-3">
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 15:55]  Успешная регистрация на платформе + 100 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 16:15]  Пройдено Mission 1 >> Pronunciation >> Step 1 [f] + 10 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 16:30]  Пройдено Mission 1 >> Pronunciation >> Step 2 [p] + 10 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 16:45]  Пройдено Mission 1 >> Pronunciation >> Step 3 [i:] + 10 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 18:15]  Пройдено Mission 1 >> Pronunciation + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[01.07.2020 19:15]  Пройдено Mission 1 + 100 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[02.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[03.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[04.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[04.07.2020 19:15]  Заходили в личный кабинет 3 дня подряд + 100 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[05.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[06.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[05.07.2020 19:15]  Зашли в личный кабинет + 50 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[07.07.2020 19:15]  Заходили в личный кабинет 7 дней подряд + 150 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[14.07.2020 19:15]  Заходили в личный кабинет 14 дней подряд + 200 X-Pounds</p></div>
          <div class="list-items bg-theme"><p class="text-theme py-2 my-0 px-2 px-md-0">[14.07.2020 19:15]  Заходили в личный кабинет 30 дней подряд + 300 X-Pounds</p></div>
        </div>

@endsection

@section( 'scripts' )
@endsection