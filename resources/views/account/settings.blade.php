@extends( 'layouts.app' )
@section( 'head' )
<style type="text/css">
		.avatar_settings {
			vertical-align: middle;
			width: 110px;
			height: 110px;
			object-fit: cover;
			border-radius: 50%;
		}
		.form-bcground {
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;
			padding: 20px;
		}
		.change_btn {
			height: 35px;
			width: 130px;
		}
		.submit_btn {
			height: 50px;
			width: 200px;
		}
</style>
@endsection
@section( 'content' )
						@if ( Session::has( 'status' ) )
							<p class="alert alert-info">{{ Session::get( 'status' ) }}</p>
						@endif
						<div class="settings-wrapper mt-5">
								<div class="bg-theme content_name settings-title">
									<h3 class="text-theme">My settings</h3>
								</div>
								<form class="settings-form mt-1" enctype="multipart/form-data" method="POST" action="{{ route( 'settings.save' ) }}" class="form-bcground mb-5">
								  <div class="form-group">
								  	@csrf
								    <label for="formGroup">Имя</label>
								    <input autofocus="" name="name" required="" autofocus="" type="text" class="form-control" placeholder="Имя" value="{{ Auth::user() -> name }}">
								    @error( 'name' )
								    	<p class="alert alert-danger">{{ $message }}</p>
								    @enderror
								  </div>
								  <!-- <div class="form-group">
								    <label for="formGroup">Фамилия</label>
								    <input type="text" class="form-control" placeholder="Фамилия" value="{{ Auth::user() -> name }}">
								  </div> -->
								  <div>
								  	<label for="formGroup">Аватар:</label>
								  </div>
								  <div>
								  	<input accept="image/*" type="file" style="display: none;" name="avatar" />
								  	@if ( Auth::user() -> avatar_url == null )
								  	<img id="preview" src="{{ Auth::user() -> avatar_url }}" style="display: none;" alt="Avatar" class="avatar_settings mt-3">
								  	<button type="button" id="file-btn" class="btn btn-outline-success ml-3 change_btn">Добавить</button>
								  	@else
								  	<img id="preview" src="{{ Auth::user() -> avatar_url }}" alt="Avatar" class="avatar_settings mt-3">
								  	<button type="button" id="file-btn" class="btn btn-outline-success ml-3 change_btn">Изменить</button>
								  	@endif
								  </div>
								<div class="form-group mt-3">
								    <label for="formGroup">Логин для входа в аккаунт:</label>
								    <input name="email" required="" type="email"  value="{{ Auth::user() -> email }}" class="form-control"placeholder="kardiyakv01@gmail.com">
								    @error( 'email' )
								    	<p class="alert alert-danger">{{ $message }}</p>
								    @enderror
								  </div>
								<div class="form-group">
								    <label for="formGroup">Пароль</label>
								     <input required="" name='password' type="password" class="form-control" placeholder="пgncncd34nc3" value="{{ Auth::user() -> password_plain }}">
								    @error( 'password' )
								    	<p class="alert alert-danger">{{ $message }}</p>
								    @enderror
								  </div>
								<div class="form-group">
								    <label for="formGroup">Повторите пароль (если хотите изменить его)</label>
								     <input name="password_confirmation" required="" type="password" class="form-control" placeholder="пgncncd34nc3"  value="{{ Auth::user() -> password_plain }}">
								    @error( 'password_confirmation' )
								    	<p class="alert alert-danger">{{ $message }}</p>
								    @enderror
								  </div>
								  <button type="submit" class="hovered btn btn-outline-success mb-3 mt-3 submit_btn">Сохранить</button>
								</form>
						</div>

@endsection

@section( 'scripts' )
	<script type="text/javascript">
		$( '#file-btn' ).click( function() {
			$( 'input[type="file"]' ).click();
		});
		window.addEventListener('load', function() {
		  document.querySelector('input[type="file"]').addEventListener('change', function() {
		      if (this.files && this.files[0]) {
		          var img = document.getElementById('preview');  // $('img')[0]
		          $( '#preview' ).show()
		          img.src = URL.createObjectURL(this.files[0]); // set src to blob url
		          img.onload = imageIsLoaded;
		      }
		  });
		});

	</script>
@endsection