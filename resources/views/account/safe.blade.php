@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )
						  <div  class="mission mb-5 mt-5">
					          <div id="accordionn" class="safe accordio">
							  <div class="card">
							    <div class="card-header" id="headingOne">
							      <h5 class="mb-0">
							      	<div class="btn btn-link d-flex text-theme">
							            <h3 class="mr-auto">
							                My Safe
							            </h3>
							        </div>
							      </h5>
							    </div>
							   </div>
					          	@php $x = 0; @endphp
					          	@if ( isset( $safeInfo[ 'sections' ] ) )
			                @foreach ( $safeInfo[ 'sections' ] as $key => $section )
			                @php $x++; @endphp
							  <div class="card">
							    <div class="card-header" id="headingOne">
							      <h5 class="mb-0">
							      	<div class="btn btn-link d-flex text-theme" data-toggle="collapse" data-target="#safe_section-{{ $key }}" aria-expanded="true" aria-controls="safe_section-{{ $key }}">
							            <span class="mr-auto">
							                {{ $section[ 'title' ] }}
							            </span>
							            <span class="ml-auto ">
							                <i class="fa fa-chevron-down"></i>
							            </span>
							        </div>
							        <!-- <button class="btn btn-link text-theme" data-toggle="collapse" data-target="#safe_section-{{ $key }}" aria-expanded="true" aria-controls="safe_section-{{ $key }}">
							          {{ $section[ 'title' ] }}
							        </button> -->
							      </h5>
							    </div>

							    <div id="safe_section-{{ $key }}" class="collapse {{ $x == 1 ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionn">
							      <div class="card-body">
							        <ul class="list-unstyled ml-2">
							            @foreach ( $section[ 'steps' ] as $step )
							                <li class="">
							                    @if ( Auth::user() -> canStep( $step[ 'id' ] ) === true )
							                        <span class="icon-check"></span>
								                    <a href="{{ route( 'account.step' , $step[ 'id' ] ) }}" class="text-theme">{{ $step[ 'title' ] }}</a>
							                    @elseif ( Auth::user() -> canStep( $step[ 'id' ] ) === 1 )
							                        <span class="icon-spinner"></span>
								                    <a href="{{ route( 'account.step' , $step[ 'id' ] ) }}" class="text-theme">{{ $step[ 'title' ] }}</a>
							                    @else
							                        <span class="icon-lock"></span>
							                        <a href="#" class="text-theme">{{ $step[ 'title' ] }}</a>
							                    @endif
							                </li>
							            @endforeach
							        </ul>
							      </div>
							    </div>
							  </div>
							  @endforeach
							  @endif
							</div>
						</div>

@endsection

@section( 'scripts' )
@endsection