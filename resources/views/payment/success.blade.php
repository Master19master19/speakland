<!DOCTYPE html>
<html>
<head>
	<title>
		Оплата прошла успешно
	</title>
</head>
<body style="text-align: center;">
	<h1>Поздравляем вас с успешной оплатой подписки!</h1>
	<h2>
		<a style="color: black;" href="{{ env( 'APP_URL' ) }}/home">Зайти в личный кабинет</a>
	</h2>
</body>
</html>