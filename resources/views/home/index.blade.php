@extends( 'layouts.home' )
@section( 'content' )

<header class="masthead bg-primary text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <hr>
                <hr>
                <hr>
                <h1 class="masthead-heading text-uppercase mb-3">Speak Land</h1>
                <hr>
                <hr>
                <p class="masthead-subheading font-weight-light mb-3">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </header>
        <section class="page-section portfolio" id="portfolio">
            <div class="container-fluid">
                <h3 class="page-section-heading text-center text-uppercase text-secondary mb-3">About</h3>
            <div class="row">
                <div class="col-lg-5">
                    <p class="masthead-subheading font-weight-light">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-lg-7">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="" data-src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                      <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->
                    </div>
                </div>
        </section>
        <section class="page-section bg-primary text-white mb-0" id="about">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-white">Form</h2>
                <div class="row">
                    <div class="col-lg-8 mx-auto bg-white">
                        <form id="contactForm" name="sentMessage" method="POST" action="{{ route( 'register_normal' ) }}">
                            @csrf
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input name="name" value="{{ old( 'name' ) }}" class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                    @error( 'name' )
                                        @if ( old( 'try' ) == null )
                                            <p class="help-block text-danger">{{ $message }}</p>
                                        @endif
                                    @enderror
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input name="email" value="{{ old( 'email' ) }}" class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                    @error( 'email' )
                                        @if ( old( 'try' ) == null )
                                            <p class="help-block text-danger">{{ $message }}</p>
                                        @endif
                                    @enderror
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input name="phone" value="{{ old( 'phone' ) }}" class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                                    @error( 'phone' )
                                        @if ( old( 'try' ) == null )
                                            <p class="help-block text-danger">{{ $message }}</p>
                                        @endif
                                    @enderror
                                </div>
                            </div>
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section" id="contact">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Beta try</h2>
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <form id="contactForm" name="sentMessage" action="{{ route( 'register_try' ) }}" method="POST">
                            @csrf
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input type="hidden" value="1" type='text' name="try" />
                                    <p class="help-block text-danger"></p>
                                    <input name="name" value="{{ old( 'name' ) }}" class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                    @error( 'name' )
                                        @if ( old( 'try' ) ) <p class="help-block text-danger">{{ $message }}</p>@endif
                                    @enderror
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input name="email" value="{{ old( 'email' ) }}" class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                    @error( 'email' )
                                        @if ( old( 'try' ) ) <p class="help-block text-danger">{{ $message }}</p>@endif
                                    @enderror
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input name="phone" value="{{ old( 'phone' ) }}" class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your email address." />
                                    @error( 'phone' )
                                        @if ( old( 'try' ) ) <p class="help-block text-danger">{{ $message }}</p>@endif
                                    @enderror
                                </div>
                            </div>
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

@endsection

@section( 'scripts' )
@endsection