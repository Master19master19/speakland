<footer class="pt-3 text-center pb-4">
	<div>
		<a href="/privacy-policy#tab-privacy">
			<small>Политика конфиденциальности</small>
		</a>
	</div>
	<div>
		<a href="/terms-of-use">
			<small>Условия оказания услуг</small>
		</a>
	</div>
	<div>
		<a href="/promotion-terms">
			<small>Условия акции</small>
		</a>
	</div>
	<div>
		<p class="mb-0">ФЛП Токарь Елена Викторовна</p>
		<p class="mb-0">ЕГРПОУ 2782622769</p>
		<!-- <p>тел. <a target="_blank" href="tel:+38 (093) 107-08-39">+38 (093) 107-08-39</a></p> -->
		<p class="mb-0"><a target="_blank" href="mailto:support@speakland.pro">support@speakland.pro</a></p>
	</div>
</footer>