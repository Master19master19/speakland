  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Speak Land">
  <link href="/android-chrome-512x512.png" sizes="512x512" rel="apple-touch-startup-image" />
  <script type="text/javascript" src="/a2hs.js"></script>
  <meta name="apple-mobile-web-app-status-bar-style" content="#38ffd2">
  <link rel="apple-touch-icon" sizes="152x152" href="/logo.png" type="image/png">
  <link rel="apple-touch-icon" sizes="167x167" href="/logo.png" type="image/png">
  <link rel="apple-touch-icon" sizes="180x180" href="/logo.png" type="image/png">
  <link rel="mask-icon" href="assets/images/icons/safari-pinned-tab.svg" color="#38ffd2">
  <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#38ffd2">
  <meta name="msapplication-TileColor" content="#38ffd2">
  <meta name="theme-color" content="#38ffd2">

<!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '324853248710265');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=324853248710265&ev=PageView&noscript=1"
        /></noscript>
<!-- End Facebook Pixel Code -->