<div class="mt-2 form-wrapper">
	<h4 class="mb-3">Всего 20$ / мес.</h4>
	<form class="form" action="{{ route( 'create_payment' ) }}" method="POST">
		@csrf
		<input type="hidden" name="utm_medium" value="{{ Request::input( 'utm_medium' ) }}" />
		<input type="hidden" name="utm_campaign" value="{{ Request::input( 'utm_campaign' ) }}" />
		<input type="hidden" name="utm_content" value="{{ Request::input( 'utm_content' ) }}" />
		<div class="form-group">
			<input type="email" value="{{ old( 'email' ) }}" minlength="5" maxlength="40" class="form-control @error( 'email' ) is-invalid @enderror" required="" placeholder="Ваш E-mail" name="email" />
			@error( 'email' )
				<div class="my-4">
					<p class="alert alert-danger">{{ $message }}</p>
				</div>
			@enderror
		</div>
		<div class="form-group">
			<input type="tel" value="{{ old( 'phone' ) }}" minlength="7" maxlength="20" class="form-control @error( 'phone' ) is-invalid @enderror" required="" placeholder="Ваш телефон" name="phone" />
			@error( 'phone' )
				<div class="my-4">
					<p class="alert alert-danger">{{ $message }}</p>
				</div>
			@enderror
		</div>
		<div class="form-group">
			<button class="btn btn-success">Оформить подписку</button>
		</div>
		<div class="form-group">
			<small>
				Отправляя эту форму, вы соглашаетесь с <br><a href="/terms-of-use">условиями оказания услуг</a>
			</small>
		</div>
	</form>
</div>