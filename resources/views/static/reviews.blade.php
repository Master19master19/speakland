@extends( 'layouts.app' )
    @section( 'head' )
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @endsection
@section( 'content' )
<style type="text/css">
	.rules-background {
		background-color: white;
		border-radius: 5px;
		box-shadow: 0px 4px 4px #C7C8B8;
	}
	.hr-rules-clr {
		border: 0.5px solid #DAE4CE;
	}
	.btn-for-rules-to-change:hover {
		border-bottom: 5px solid #87C232;
		padding-bottom: 10px;
	}
	.btn-for-rules-to-change {
		padding-bottom: 15px;
	}
	.border-ul {
		border-bottom: 0.5px solid #DAE4CE;
	}
</style>


<section class="container-fluid">
	<div class="">
		<h2 class="font-weight-bold pb-4 txt-h-font">@lang( 'User reviews' )</h2>
	</div>
	<div class="rules-background pt-4 px-3">		
		<div class="card-deck mx-auto">
			@foreach ( $reviews as $review )
				<div class="mx-auto" style="max-width: 100%; flex:1; min-width: 14rem;">
					<div class="card border-success mb-3" >
						<div class="card-header bg-transparent border-success txt-h-font">{{ $review -> name }}</div>
						<div class="card-body text-success">
							<p class="card-text">{{ $review -> message }}</p>
						</div>

		              	<div class='rating-stars unable-star text-center'>
			                <ul id='stars'>
			                  	<li class='star hover' title='Poor' data-value='1'>
				                    <i class='fa fa-star fa-fw'></i>
				                </li>
				                <li class='star hover' title='Fair' data-value='2'>
				                    <i class='fa fa-star fa-fw'></i>
				                </li>
				                <li class='star hover' title='Good' data-value='3'>
				                    <i class='fa fa-star fa-fw'></i>
				                </li>
				                <li class='star @if ( $review -> rating > 3 ) hover @endif' title='Excellent' data-value='4'>
				                    <i class='fa fa-star fa-fw'></i>
				                </li>
				                <li class='star @if ( $review -> rating == 5 ) hover @endif' title='WOW!!!' data-value='5'>
				                    <i class='fa fa-star fa-fw'></i>
				                </li>
				            </ul>
			        	</div>
						<div class="card-footer">
							<small class="text-muted">{{ date( 'Y-m-d H:i' , strtotime( $review -> created_at ) ) }}</small>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>

@endsection