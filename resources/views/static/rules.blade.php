@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )


<style type="text/css">
    .rules-background {
        background-color: white;
        border-radius: 5px;
        box-shadow: 0px 4px 4px #C7C8B8;
    }
    .hr-rules-clr {
        border: 0.5px solid #DAE4CE;
    }
    .btn-for-rules-to-change:hover {
        border-bottom: 5px solid #87C232;
        padding-bottom: 10px;
    }
    .btn-for-rules-to-change {
        padding-bottom: 15px;
    }
    .border-ul {
        border-bottom: 0.5px solid #DAE4CE;
    }
</style>


<section class="pt-4 container-fluid">
    <div class="">
        <h2 class="font-weight-bold pb-4 txt-h-font">Правила сервиса</h2>
    </div>
    <div class="rules-background pt-4">

        <nav class="navbar navbar-light navbar-expand-lg mb-0 pb-0">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarrere">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- <div class="collapse navbar-collapse" id="navbarrere">
                <ul class="navbar-nav mx-auto text-center border-ul">
                    <a class="px-4 text-secondary btn-for-rules-to-change" href="#"><h6 class="d-inline btn-for-rules-txt">Правила обмена</h6></a>
                    <a class="px-4 text-secondary btn-for-rules-to-change" href="#"><h6 class="d-inline btn-for-rules-txt">Политика конфидециальности</h6></a>
                    <a class="px-4 text-secondary btn-for-rules-to-change" href="#"><h6 class="d-inline btn-for-rules-txt">Политика подачи жалоб</h6></a>
                    <a class="px-4 text-secondary btn-for-rules-to-change" href="#"><h6 class="d-inline btn-for-rules-txt">Правила АМЛ</h6></a>
                    <a class="px-4 text-secondary btn-for-rules-to-change" href="#"><h6 class="d-inline btn-for-rules-txt">Контакты</h6></a>
                </ul>
            </div> -->
        </nav>
        <div class=" pb-md-5 pb-3">
            <div class="px-3 pt-3">
                <h4 class="font-weight-bold pb-3 txt-h-font">Условия и положения</h4>
                <h5 class="font-weight-bold pb-3 txt-h-font">Соглашение о предоставлении услуг сервисом Cashalot Exchange</h5>
                <h6 class="font-weight-bold pb-3 txt-h-font">1. Общие положения</h6>
                <p class="txt-clr-rules">Настоящее соглашение (далее по тексту Соглашение) описывает правила и условия, на основании которых предоставляются услуги мультивалютного обменного сервиса Cashalot Exchange и является официальной письменной публичной офертой, адресованной физическим лицам (далее по тексту Пользователь), заключить Соглашение о предоставлении услуг сервисом Cashalot Exchange на изложенных ниже условиях. Перед тем как воспользоваться услугами сервиса Cashalot Exchange, Пользователь обязан ознакомиться в полном объеме с условиями «Соглашения о предоставлении услуг сервисом Cashalot Exchange». Использование услуг сервиса Cashalot Exchange возможно только, если Пользователь принимает все условия Соглашения. Действующая версия Соглашения расположена для публичного доступа на интернет-сайте сервиса Cashalot Exchange (https://cashalot.exchange/rules).</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">2. Термины и определения, используемые в Соглашении</h6>
                <p class="txt-clr-rules">Сервис Cashalot Exchange — торговая марка и коммерческое обозначение системы предоставления интернет услуг по обмену электронных валют. 
                Интернет-сайт Сервиса – https://cashalot.exchange/. 
                Пользователь — любое физическое лицо, которое желает воспользоваться услугами сервиса Cashalot Exchange и осуществившее акцепт Соглашения в соответствии с его условиями. 
                Платежная система — программный продукт, созданный третьей стороной, представляющий собой механизм реализации учета денежных и/или иных обязательств, оплату товаров и услуг в сети Интернет, а также организацию взаиморасчетов между своими пользователями. 
                Электронная валюта – денежное и/или иное обязательство между разработчиком данной валюты и ее пользователем, выраженное цифровым способом. 
                Платеж/операция — перевод электронной и/или иной валюты от плательщика к получателю. 
                Клиент платежной системы – лицо, заключившее соглашение с соответствующей платежной системой на приобретение имущественных прав требования к ней, измеряемых в условных единицах, принятых в соответствующей платежной системе. 
                Заявка – выражение намерения Пользователя воспользоваться одной из услуг предлагаемых Сервисом Cashalot Exchange, путем заполнения электронной формы через Интернет-сайт Сервиса, на условиях, описанных в Соглашении и указанных в параметрах этой Заявки. 
                Исходная валюта – электронная валюта, которую Пользователь желает продать или обменять. 
                Исходный счет – номер кошелька или любое другое обозначения счета Пользователя в Платежной системе, с которого была отправлена Исходная валюта. 
                Результирующая валюта – электронная валюта, которую Пользовать получает в результате продажи или обмена Исходной валюты. 
                Результирующий счет – номер кошелька или любое другое обозначения счета Пользователя в Платежной системе, на который будет отправлена Результирующая валюта. 
                Резерв валюты - имеющийся в распоряжении Сервиса Cashalot Exchange, на момент создания Заявки, объем определенной Электронной валюты. 
                Обмен валюты - обмен электронной валюте одной платежной системы на электронную валюту другой платежной системы. 
                Курс — стоимостное соотношение двух электронных валют при их обмене. 
                Хакер — квалифицированный компьютерный взломщик, злоумышленник, специализирующийся на поиске и использовании несанкционированного доступа в вычислительные сети или другое компьютеризированное оборудование для незаконного получения информации, извлечения выгоды, нанесения ущерба.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">3. Предмет Соглашения</h6>
                3.1. Предметом настоящего Соглашения является предоставление Пользователю Сервисом Cashalot Exchange услуги по обмену электронных валют.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">4. Порядок оказания услуг Сервисом</h6>
                <p class="txt-clr-rules">4.1. Заказ услуг Сервиса Cashalot Exchange осуществляется Пользователем путем направления Заявки через Интернет-сайт Сервиса. 
                4.2. Управление процессом сделки или получение информации о ходе выполнения сделки Пользователем производятся при помощи соответствующего пользовательского интерфейса, расположенного на Интернет-сайте Сервиса. 
                4.3. Сервис Cashalot Exchange осуществляет исполнение Заявок на безотзывной основе в соответствии с условиями работы соответствующих платежных систем. 
                4.5. Сервис Cashalot Exchange не является стороной соглашения между Платежной системой и Клиентом платежной системы и ни в коем случае не несет ответственности за действия Платежной системы и ее Клиента. Права и обязанности платежной системы и ее Клиента регулируются условиями предоставления услуг соответствующих Платежных систем. 
                4.6. Сервис Cashalot Exchange не требует удостоверения в том, что отправитель и получатель средств, участвующих в Операции, является одним и тем лицом, Сервис Cashalot Exchange не является стороной во взаимоотношениях отправителя и получателя денежных средств или электронной валюты. 
                4.7. Сервис Cashalot Exchange не проверяет правомочность и законность владения Пользователем электронными валютами и/или денежными средствами, участвующими в конкретной Операции. 
                4.8. Воспользовавшись услугами Сервиса Cashalot Exchange, Пользователь подтверждает, что законно владеет и распоряжается денежными средствами и электронной валютой, участвующими в соответствующем Платеже. 
                4.9. Пользователь обязуется самостоятельно исчислять и уплачивать все налоги, требуемые согласно налоговому законодательству места нахождения Пользователя. 
                4.10. Заботясь о качестве оказываемых Пользователям услуг, Сервис Cashalot Exchange обязуется совершать все действия, в рамках настоящего Соглашения, максимально оперативно.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">5. Стоимость услуг</h6>
                <p class="txt-clr-rules">5.1. Стоимость услуг Сервиса устанавливается руководством Сервиса и публикуется на интернет-сайте Сервиса . 
                5.2. Сервис вправе самостоятельно изменять курсы обмена электронных валют и взимаемых комиссионных в любое время в одностороннем порядке, о чем уведомляет Пользователей сервиса предварительным размещением информации об этих изменениях на интернет-сайте Сервиса. 
                5.3. В Заявке создаваемой Пользователем на интернет сайте сервиса указывается Курс, размер комиссии, взимаемый соответствующей Платежной системой, за проведение Операции, размер вознаграждения Сервиса Cashalot Exchange, а также общая сумма перечисляемых денежных средств или электронной валюты. 
                5.4. Сервис Cashalot Exchange взимает стоимость своего вознаграждения в момент проведения соответствующей Операции. Вознаграждение Сервиса вычитается из суммы Результирующей валюты.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">6. Обмен электронной валюты</h6>
                <p class="txt-clr-rules">6.1. Путем оформления Заявки Пользователь поручает, а Сервис Cashalot Exchange от своего имени и за счет Пользователя, совершает действия по обмену Электронной валюты одной Платежной системы (Исходная валюта) на Электронную валюту другой Платежной системы (Результирующая валюта) выбранной Пользователем. 
                6.2. Пользователь обязуется перечислить (передать) Исходную валюту, в размере указанном в Заявке, а Сервис Cashalot Exchange, после получения соответствующей Электронной валюты, обязуется перечислить (передать) Пользователю Результирующую валюту, рассчитанную по Курсу и в соответствие с тарифами сервиса . 
                6.3. Размер вознаграждения Сервиса Cashalot Exchange отражается в Заявке и подтверждается Пользователем нажатием кнопки «Далее» на одной из страниц пользовательского интерфейса при оформление заявки. 
                6.4. Обязанность Сервиса Cashalot Exchange по перечислению (передаче) Электронной валюты Пользователю считается исполненной в момент списания Электронной валюты в соответствующей Платежной системе со счета Сервиса Cashalot Exchange, что регистрируется в истории операций соответствующей Платежной системы. 
                6.5. Сервис Cashalot Exchange вправе отменить созданную Пользователем Заявку если средства в сумме Исходной валюты не поступили на счет сервиса в течение 20 минут. 
                6.6. Сервис Cashalot Exchange вправе приостановить операцию и удержать средства Пользователя, в целях предотвращения мошеннических и других действий, которые могут нанести финансовые и репутационные потери для Сервиса или Пользователя. 
                6.7. Сервис Cashalot Exchange вправе установить финансовые и количественные лимиты на Операции. Информация о лимитах указывается на Интернет-сайте Сервиса.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">7. Вступление Соглашения в силу</h6>
                <p class="txt-clr-rules">7.1. Настоящее Соглашение считается заключенным на условиях публичной оферты, акцептуемой Пользователем в ходе подачи им Заявки. 
                7.2. Публичной офертой признается отображаемая Сервисом Cashalot Exchange информация о параметрах и условиях Заявки. 
                7.3. Акцептом публичной оферты признается совершение Пользователем действий по завершению формирования Заявки, подтверждающих его намерение воспользоваться услугами Сервиса Cashalot Exchange на условиях, описанных в настоящем Соглашениях и указанных в Заявке. 
                7.4. Дата и время акцепта, а также параметры условий Заявки фиксируются Сервисом Cashalot Exchange автоматически в момент завершения формирования заявки. 
                7.5. Соглашение вступает в силу с момента завершения формирования Заявки Пользователем. Пользователь имеет право отказаться от совершения операции по Заявке до оплаты Операции.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">8. Ответственность Сторон</h6>
                <p class="txt-clr-rules">8.1. Сервис Cashalot Exchange несет ответственность перед Пользователем в размере, не превышающем вверенной Пользователем суммы денежных средств или электронной валюты. 
                8.2. Сервис Cashalot Exchange не отвечает за неисправности, ошибки и сбои в работе программных и/или аппаратных средств, обеспечивающих функционирование услуг Сервиса Cashalot Exchange, возникшие по причинам, не зависящим от Сервиса Cashalot Exchange, а также связанные с этим убытки Пользователя. 
                8.3 Сервис Cashalot Exchange оказывает услуги только по обмену, покупке и продаже электронных валют. Cashalot Exchange ни коем образом не принимает оплату в пользу каких-либо третьих лиц, а также, запрещает обмен на кошельки/счета, которые не принадлежат пользователю. Cashalot Exchange не вступает ни в какие партнерские отношения, не заключает ни каких договоров с получателями платежей за свои товары или услуги, а категорически против таких отношений. Cashalot Exchange нельзя использовать как промежуточный сервис для взаиморасчетов между покупателем и продавцом (заказчиком и исполнителем и т.д.). 
                8.4. Сервис Cashalot Exchange не отвечает за убытки Пользователя, возникшие в результате неправомерных действий третьих лиц. 
                8.5. Пользователь несет всю ответственность за достоверность сведений, указанных им при заполнении Заявки. В случае если Пользователь не указал или неверно указал данные, Сервис Cashalot Exchange не отвечает за убытки Пользователя, понесенные в результате допущенной ошибки. 
                8.6. Информация по Операции сохраняется в базе данных сервиса и является первоисточником, на который ориентируются Стороны соглашения в спорных ситуациях. 
                8.7. Стороны освобождаются от ответственности за полное или частичное неисполнение своих обязательств по Соглашению, если таковое явилось следствием обстоятельств непреодолимой силы, возникших после вступления в силу Соглашения, в результате событий чрезвычайного характера, которые не могли быть предвидены и предотвращены разумными мерами. 
                8.8. В других случаях неисполнения или ненадлежащего исполнения своих обязательств по Соглашению Стороны несут ответственность в соответствии с законодательством Украины с учетом условий Соглашения.</p>
                <h6 class="font-weight-bold pb-3 txt-h-font">9. Прочие положения</h6>
                <p class="txt-clr-rules">9.1. Сервис Cashalot Exchange вправе в одностороннем порядке вносить изменения в Соглашения путем публикации изменений на Сайте Системы. Изменения вступают в силу с момента опубликования, если иной срок вступления изменений в силу не определен дополнительно при их публикации.
                9.2. Сервис Cashalot Exchange вправе отказать пользователю в оформлении заявки без пояснения причин отказа. 
                9.3. Сервис Cashalot Exchange вправе в случае подозрительных действий в процессе оформления заявки пользователем, во избежание ущерба от Хакерских атак приостанавливать выполнения таких операций до выяснения причин этих действий. 
                9.4. Сервис Cashalot Exchange вправе отказать в выполнение операции обмена, покупки и продажи электронных валют, если передача Исходной валюты на счет сервиса была произведена без оформления заявки при помощи пользовательских интерфейсов на сайте сервиса. Электронная валюта, перечисленная на счета сервиса, без оформления заявки при помощи пользовательских интерфейсов на сайте сервиса может быть возвращена пользователю по запросу с учетом вычета комиссии Платежной системы, если таковая присутствует, и комиссии сервиса Cashalot Exchange в размере 2%.
                9.5. Сервис Cashalot Exchange вправе отправлять Пользователю на указанный e-mail информацию о состояние процесса обмена, т.к. это является неотъемлемой частью процесса успешного завершения обмена. 
                9.6. Все споры и разногласия, возникшие или могущие возникнуть из настоящего Соглашения, подлежат разрешению путем переговоров на основании письменного заявления Пользователя. Сервис Cashalot Exchange после получения от Пользователя претензии обязан в течение 15 (пятнадцати) дней удовлетворить заявленные в претензии требования либо направить Пользователю мотивированный отказ. К ответу должны быть приложены все необходимые документы. В случае если возникший спор не будет разрешен в претензионном порядке в течение 60 (шестидесяти) дней, любая из Сторон вправе обратиться за разрешением спора в суд по месту нахождения Пользователя.
                Пользователь подтверждает, что он ознакомлен со всеми положениями настоящего Соглашения и безусловно принимает их.</p>
            </div>
        </div>
    </div>







    @endsection
