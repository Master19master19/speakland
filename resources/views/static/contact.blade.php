@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )


<style type="text/css">
    .rules-background {
        background-color: white;
        border-radius: 5px;
        box-shadow: 0px 4px 4px #C7C8B8;
    }
    .hr-rules-clr {
        border: 0.5px solid #DAE4CE;
    }
    .btn-for-rules-to-change:hover {
        border-bottom: 5px solid #87C232;
        padding-bottom: 10px;
    }
    .btn-for-rules-to-change {
        padding-bottom: 15px;
    }
    .border-ul {
        border-bottom: 0.5px solid #DAE4CE;
    }
</style>





<section class="container">
    <div class="rules-background pt-4">
        <div class="ml-4 pt-2">
            <h4 class="font-weight-bold pb-4 txt-h-font">@lang( 'Contacts' )</h4>
        </div>
        <div class="container-fluid">
            <div class="row mx-auto">
                <div class="col-sm-12 col-lg-5">
                    <p class="txt-clr-rules pl-lg-3"><i class="far fa-clock"></i>
                        @lang( 'Working hours:' )
                    @lang( 'from' ) 08:00 @lang( 'to' ) 12:00</p>
                    <p class="txt-clr-rules pl-lg-3"><i class="far fa-envelope"></i>
                        @lang( 'Mail Support:' )
                        <a href="#" class="to-partners-font-clr">support@cashalot.com</a></p>
                        <p class="txt-clr-rules pl-lg-3"><i class="far fa-comment-dots"></i>
                            @lang( 'Online chat:' )
                            <a href="#" class="to-partners-font-clr">@lang( 'Open chat now' )</a></p>
                        </div>
                        <div  class="col-sm-12 col-lg-7">
                            <p class="txt-clr-rules mb-1">
                            @lang( 'If you have any problems of a technical or financial plan, write to us and we will help you in solving your problem. We answer questions within an hour, depending on the load of the service.' )</p>
                            <button type="button" class="btn btn-inline-block text-center py-2 px-md-5 exchange-btn">@lang( 'Write to operator' )</button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="ml-4 pt-2">
                    <h4 class="font-weight-bold pb-4 txt-h-font">@lang( 'Collaboration and PR' )</h4>
                </div>  
                <div class="container-fluid">
                    <div class="row mx-auto">
                        <div class="col-sm-12 col-lg-5">
                            <p class="txt-clr-rules pl-lg-3"><i class="far fa-clock"></i>
                                @lang( 'Working hours:' )
                            @lang( 'from' ) 12:00 @lang( 'to' ) 18:00</p>
                            <p class="txt-clr-rules pl-lg-3"><i class="far fa-envelope"></i>
                                @lang( 'Our e-mail:' )
                                <a href="#" class="to-partners-font-clr">info@cashalot.com</a></p>
                                <p class="txt-clr-rules pl-lg-3"><i class="far fa-comment-dots"></i>
                                    @lang( 'Support:' )
                                    <a href="#" class="to-partners-font-clr">
                                    @lang( 'Contact support' )</a></p>
                                </div>
                                <div  class="col-sm-12 col-lg-7">
                                    <p class="txt-clr-rules mb-1">
                                    @lang( 'If you have a proposal for cooperation, monitoring, questions about advertising, adding a new currency - write, we will be happy to cooperate!' )</p>
                                    <button type="button" class="btn btn-inline-block text-center py-2 px-md-5 exchange-btn">@lang( 'Write now' )</button>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="ml-4 pt-2">
                            <h4 class="font-weight-bold pb-4 txt-h-font">@lang( 'Service Reviews' )</h4>
                        </div>  
                        <div class="container-fluid">
                            <div class="row mx-auto">
                                <div class="col-sm-12 col-lg-6">
                                    <p><img src="/img/bestchange.png"></p>
                                    <p class="txt-clr-rules">
                                        @lang( 'Reviews about us on the personal monitoring page BestChange' )
                                        <br>
                                        <a href="#" class="to-partners-font-clr">Bestchange.ru</a></p>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <p><img src="/img/mmgp.png"></p>
                                        <p class="txt-clr-rules">
                                            @lang( 'Reviews about us in the MMGP forum thread:' )
                                            <br>
                                            <a href="#" class="to-partners-font-clr">MMGP.ru</a></p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </div>
                        </section>








@endsection