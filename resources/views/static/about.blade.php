@extends( 'layouts.app' )
@section( 'head' )
@endsection
@section( 'content' )


<style type="text/css">
    .about-background {
        background-color: white;
        border-radius: 10px;
        box-shadow: 0px 4px 4px #C7C8B8;
    }
</style>


<section class="pt-4 container-fluid">
    <div class="row">
        <div class="col-lg-11 mx-auto">
            <div class="">
                <h1 class="font-weight-bold pb-4 txt-h-font">@lang( 'About us' )</h1>
            </div>
            <div class="pt-5 about-background">
                <h3 class="pb-3 ml-5 txt-h">@lang( 'About service' )</h3>
                <p class="px-5 txt-clr-rules">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.
                </p>
                <h5 class="font-weight-bold pt-3 ml-5 txt-h-font">Работаем ежедневно с 6:00 до 2:00</h5>
                <p class="px-5 txt-clr-rules">Наши операторы обрабатывают заявки в ручном режиме.
                    Время обработки заявок от 5 до 20 минут.
                </p>
                <h5 class="font-weight-bold pt-3 ml-5 txt-h-font">У нас Вы можете обменять: </h5><p class="px-5 txt-clr-rules">QIWI, Яндекс. Деньги, Visa/MasterCard RUB, Сберонлайн, Perfect Money USD, e-Voucher USD, Payeer RUB, Payeer USD, AdvCash RUB, AdvCash USD, Exmo RUB, Exmo USD, Bitcoin, Bitcoin Cash, Ethereum, Ethereum Classic, Litecoin, Dash, Ripple.
                </p>
                <div class="pt-5 mb-5 d-flex flex-column">
                    <h5 class=" font-weight-bold txt-h-font ml-auto mr-5">Sincerely, <br> cashalot.com</h5>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>



@endsection
