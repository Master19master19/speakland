<!DOCTYPE html>
<html>
<head>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @include( 'landing.head' )
      <title>Speak Land</title>
</head>
<body style="text-align: center;font-family: Montserrat;">





		<!-- <h1 style="text-align: center;margin-top: 2rem;font-size: 30px;margin-bottom: 2rem;">Перенаправляем</h1> -->
		<img src="https://cdn.lowgif.com/full/ff8280aafe27319d-ajax-loading-gif-transparent-background-2-gif-images.gif" style="width: 150px;
		height: 150px;margin-top: 2rem;">
        <form id="form" style="display: none;" method="post" action="https://secure.wayforpay.com/pay">
            <input type="text" name="merchantAccount" value="{{ $data[ 'account' ] }}">
            <input type="text" name="merchantAuthType" value="SimpleSignature">
            <input type="text" name="merchantDomainName" value="{{ $data[ 'domain' ] }}">
            <input type="text" name="merchantSignature" value="{{ $data[ 'hash' ] }}">
            <input type="text" name="orderReference" value="{{ $data[ 'payment_id' ] }}">
            <input type="text" name="orderDate" value="{{ $data[ 'time' ] }}">
            <input type="text" name="amount" value="{{ $data[ 'amount' ] }}">
            <input type="text" name="currency" value="{{ $data[ 'currency' ] }}">
            <input type="text" name="orderTimeout" value="49000">
            <input type="text" name="productName[]" value="{{ $data[ 'product' ] }}">
            <input type="text" name="productPrice[]" value="{{ $data[ 'amount' ] }}">
            <input type="text" name="productCount[]" value="{{ $data[ 'count' ] }}">
            <input type="text" name="clientEmail" value="{{ $data[ 'email' ] }}">
            <input type="text" name="clientPhone" value="{{ $data[ 'phone' ] }}">
            <input type="text" name="defaultPaymentSystem" value="card">
            <input type="text" name="paymentSystems" value="card">
            <input type="text" name="regularMode" value="{{ $data[ 'regularMode' ] }}">
            <input type="text" name="regularAmount" value="{{ $data[ 'regularAmount' ] }}">
            <input type="text" name="regularCount" value="{{ $data[ 'regularCount' ] }}">
            <input type="text" name="regularOn" value="{{ $data[ 'regularOn' ] }}">
            <input type="text" name="dateNext" value="{{ $data[ 'dateNext' ] }}">
            <input type="text" name="language" value="RU" />
            <button type="submit" >Submit</button>
        </form>
        <script type="text/javascript">
        	setTimeout( function() {
        		document.getElementById( 'form' ).submit();
        	} , 1500 );
        </script>
</body>
</html>