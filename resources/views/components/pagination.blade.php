<style type="text/css">
	.pagination a {
		font-size: 20px;
	}
	.pagination span {
		font-size: 25px;
	}

	@media only screen and (max-width: 1200px) {
		.pagination a {
			font-size: 15px;
		}
		.pagination span {
			font-size: 20px;
		}
	}

</style>
<div class="row mx-1 pagination">
	@if ( $type !== 'quiz' )
		@php $next = Helper::getNextStep( $step -> id ); @endphp
		@php $prev = Helper::getPrevStep( $step -> id ); @endphp

		@if ( $prev[ 'type' ] == 'step' )
			<a href='{{ route( "account.step" , $prev[ 'value' ] -> id ) }}' class="text-theme "><b><span class="mr-2"><</span> Previous step</b></a>
		@else
			<a disabled href='#' class=" " style="color: gray;"><b><span class="mr-2"><</span> Previous step</b></a>
		@endif

		@if ( $next[ 'type' ] == 'step' && Auth::user() -> canStep( $next[ 'value' ] -> id ) )
			<a href='{{ route( "account.step" , $next[ 'value' ] -> id ) }}' class="text-theme  ml-auto"><b>Next step <span class="ml-2">></span></b></a>
		@elseif ( $next[ 'type' ] == 'quiz' && Auth::user() -> canQuiz( $next[ 'value' ] -> id ) )
			<a href='{{ route( "account.quiz" , $next[ 'value' ] -> id ) }}' class="text-theme  ml-auto"><b>Next step <span class="ml-2">></span></b></a>
		@else
			<a disabled href='#' class="ml-auto" style="color: gray;"><b>Next step <span class="ml-2">></span></b></a>
		@endif
	@else
		@php $prev = Helper::getMissionLastStep( $quiz -> mission -> id ); @endphp
		<a href='{{ route( "account.step" , $prev -> id ) }}' class="text-theme "><b><span class="mr-2"><</span> Previous step</b></a>
	@endif
</div>