<p class="text-theme progress-text">
	{{ Auth::user() -> getProgress()[ 'stepOrder' ] }} out of {{ Auth::user() -> getProgress()[ 'stepCount' ] }} steps completed {{ Auth::user() -> getProgress()[ 'progress' ] }}%
</p>
<div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{ Auth::user() -> getProgress()[ 'progress' ] }}%" aria-valuenow="{{ Auth::user() -> getProgress()[ 'progress' ] }}" aria-valuemin="0" aria-valuemax="100"></div>
</div>