<!doctype html>
<html lang="{{ str_replace( '_' , '-' , app() -> getLocale() ) }}">
<head>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Speakland</title>
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
        <link href="/landing/css/styles.css" rel="stylesheet" />
        @yield( 'head' )
    </head>
</head>
<body id="page-top">
    <div id="app">
        @include( 'partials.home.header' )
            <main class="">
                @yield( 'content' )
            </main>
        @include( 'partials.home.footer' )
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" defer="" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer=""></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" defer=""></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" defer=""></script>
    <!-- Core theme JS-->
    <script src="/landing/js/scripts.js" defer=""></script>
    @yield( 'scripts' )
</body>
