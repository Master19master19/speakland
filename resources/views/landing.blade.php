<!doctype html>
<html lang="{{ str_replace( '_' , '-' , app() -> getLocale() ) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Speak Land</title>
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="{{ asset('/landing/app.css') }}" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	@include( 'landing.head' )
</head>
<body id="">





	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <a href="{{ route( 'account.home' ) }}" class="d-block mx-auto btn btn-outline-success my-2 my-sm-0" type="submit">Логин</a>
  </div>
</nav>




	<!-- <a class="btn btn-success" href="{{ route( 'account.home' ) }}" id="myBtn" title="Go to top">Логин</a> -->
	<div class="container text-center">
		<section class="section-first">
			<img src="/landing/img/first.png" />
			<h1 class="title-main">
				Английский по методу внешней разведки
			</h1>
			<p class="desc">
				Онлайн-платформа для изучения английского языка, на уровне родного
			</p>
			@include( 'landing.form' )
		</section>
		<div class="divider"></div>
		<section class="section-first">
			<h2 class="title">
				Так, английский, вы еще точно не учили!
			</h2>
			<div>
				<div class="player" data-source="[1080p]/landing/videos/Speak Land описание-1080.mp4,[720p]/landing/videos/Speak Land описание-720.mp4,[480p]/landing/videos/Speak Land описание-480.mp4" data-poster="/landing/videos/poster-1.jpg"></div>
			</div>
			@include(
				'landing.pool' ,
				[
					'title' => 'Английская речь только от носителей языка' ,
					'desc' => 'В нашей программе вы сразу учитесь правильному произношению без акцента, подражая носителям языка.' ,
					'icon' => '/landing/img/icon-1.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Динамические связки вместо зубрежки' ,
					'desc' => 'В нашей методике мы отрабатываем всё на специальных моделях. Доведя эти модели до автоматизма, вы сможете наполнять их любыми комбинациями.' ,
					'icon' => '/landing/img/icon-2.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Около 90% времени - говорение' ,
					'desc' => 'Чтобы научиться говорить по-английски — нужно говорить. Поэтому в нашей программе большую часть времени вы говорите, общаясь с виртуальными носителями языка.' ,
					'icon' => '/landing/img/icon-3.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Грамматика по полочкам' ,
					'desc' => 'Наша программа позволяет понять все тонкости языка максимально легко. При этом вам совершенно не нужно зубрить правила — вместо этого вам нужно всего лишь отработать их на специальных моделях и вы будете говорить по-английски совершенно не задумываясь о грамматике.' ,
					'icon' => '/landing/img/icon-4.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Интерактивная система обучения' ,
					'desc' => 'Общаясь с виртуальным носителем языка вы сможете сравнивать каждую свою фразу с эталонным звучанием и сразу исправлять все ошибки в автоматическом режиме!' ,
					'icon' => '/landing/img/icon-5.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Изучайте язык, как родной' ,
					'desc' => 'В нашей программе вы изучаете английский так, как это делает маленький ребенок, когда учится говорить. То есть при помощи образов, а не перевода с русского языка. ' ,
					'icon' => '/landing/img/icon-6.png'
				]
			)
			@include(
				'landing.pool' ,
				[
					'title' => 'Уникальная технология запоминания' ,
					'desc' => 'Закрепляйте материал при помощи световой пульсации, ритма и английской речи от носителей языка. ' ,
					'icon' => '/landing/img/icon-7.png'
				]
			)
			@include( 'landing.form' )
		</section>
		<div class="divider"></div>
		<section class="section-first">
			<h2 class="title">
				Просто попробуйте!  <br>
				И вы будете приятно удивлены!
			</h2>
			<p class="desc">
				Посмотрите примеры наших уроков грамматики (обратите внимание, что до грамматики идет еще 3 раздела):
			</p>
			<div>
				<div class="player" data-source="[1080p]/landing/videos/Grammar Step 1-1080.mp4,[720p]/landing/videos/Grammar Step 1-720.mp4,[480p]/landing/videos/Grammar Step 1-480.mp4"  data-poster="/landing/videos/poster-2.jpg"></div>
			</div>
			<div>
				<div class="player" data-source="[1080p]/landing/videos/Grammar Step 2-1080.mp4,[720p]/landing/videos/Grammar Step 2-720.mp4,[480p]/landing/videos/Grammar Step 2-480.mp4"  data-poster="/landing/videos/poster-2.jpg"></div>
			</div>
			<div>
				<div class="player" data-source="[1080p]/landing/videos/Grammar Step 3-1080.mp4,[720p]/landing/videos/Grammar Step 3-720.mp4,[480p]/landing/videos/Grammar Step 3-480.mp4"  data-poster="/landing/videos/poster-2.jpg"></div>
			</div>
			<p class="desc">
				Посмотрите, как мы закрепляем слова при помощи световой пульсации, ритма и мелодии:
			</p>
			<div>
				<div class="player" data-source="[1080p]/landing/videos/Ритмопедия-1080.mp4,[720p]/landing/videos/Ритмопедия-720.mp4,[480p]/landing/videos/Ритмопедия-480.mp4" data-poster="/landing/videos/poster-3.jpg"></div>
			</div>
			@include( 'landing.form' )
		</section>
		<div class="divider"></div>
		<section class="section-first">
			<h2 class="title mb-3">
				Розыгрыш iPhone 11 Pro Max и других призов!
			</h2>
			<p class="desc">
				Условия очень простые! Вам нужно зарегистрироваться на нашей платформе, пройти первую миссию и подписаться на наш Instagram с розыгрышем. Всего то! <br>
				И 15 сентября 2020 года в 19.00 мы разыграем среди подписчиков новенький iPhone 11 Pro Max!
			</p>
			<div class="promotion-wrapper">
				<img src="/landing/img/second.png">
			</div>
			<div class="promotion-wrapper">
				<p class="mb-0">Второе место - наушники apple AirPods pro:</p>
				<img src="/landing/img/third.jpg">
			</div>
			<div class="promotion-wrapper">
				<p>Третье место - колонка JBL Charge 4::</p>
				<img style="width: 90%" class="my-5" src="/landing/img/fourth.jpg">
			</div>
			<p>
				Ссылку на Instagram с розыгрышем вы увидите, когда пройдете первую миссию. За счет этого, на данный Инстаграм смогут подписаться только те, кто участвуют в розыгрыше, поэтому он будет максимально прозрачный и честный.
			</p>
			<p>
				Регистрируйтесь прямо сейчас, изучайте английский по самой современной технологии и участвуйте в розыгрыше новенького iPhone!
			</p>
			@include( 'landing.form' )
		</section>
	</div>
		@include( 'landing.footer' )
    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript" src="/playerjs.js"></script>
    <script src="/landing/app.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
