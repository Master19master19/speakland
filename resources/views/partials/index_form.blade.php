
                <form class="mx-4 mt-2" method="POST" id="main-form" action="{{ route( 'transactions.start' ) }}">
                    @if($errors->any())
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                    @endif

                    @csrf
                    <div id="coin_address" class="form-group">
                        <input value="{{ old( 'coin_address' ) }}" name="coin_address" type="text" class="inp-style form-control input-btn" placeholder="@lang( 'Wallet' )">
                    </div>
                    <div id="coin_additional_address" style="display: none;" class="form-group">
                        <input value="{{ old( 'coin_additional_address' ) }}" name="coin_additional_address" type="text" class="inp-style form-control input-btn" placeholder="@lang( 'Additional' )">
                    </div>
                    <div id="fromBank" style="display: none;">
                        <div class="form-group">
                            <input value="{{ old( 'from_bank_name' ) }}" type="text" name="from_bank_name" data-from-bank-name="from_bank_name" class="inp-style form-control input-btn" placeholder="@lang( 'Sender full name' )">
                        </div>
                        <div class="form-group">
                            <input value="{{ old( 'from_bank_card' ) }}" type="text" name="from_bank_card" data-from-bank-card="from_bank_card" class="inp-style form-control input-btn" placeholder="@lang( 'Sender card number' )">
                        </div>
                    </div>
                    <div id="toBank" style="display: none;">
                        <div class="form-group">
                            <input value="{{ old( 'to_bank_name' ) }}" type="text" name="to_bank_name" data-to-bank-name="to_bank_name" class="inp-style form-control input-btn" placeholder="@lang( 'Receiver full name' )">
                        </div>
                        <div class="form-group">
                            <input value="{{ old( 'to_bank_card' ) }}" type="text" name="to_bank_card" data-to-bank-card="to_bank_card" class="inp-style form-control input-btn" placeholder="@lang( 'Receiver card number' )">
                        </div>
                    </div>
                    <input type="hidden" {{ old( 'send_from' ) }} name="send_from" value="" />
                    <input type="hidden" {{ old( 'amount' ) }} name="amount" value="" />
                    <input type="hidden" {{ old( 'send_to' ) }} name="send_to" value="" />
                    <hr class="hr-clr my-4">
                    <div class="form-group">
                        <input value="@auth {{ Auth::user() -> email }} @else {{ old( 'email' ) }} @endauth" type="email" name="email" class="inp-style  form-control input-btn" placeholder="@lang( 'Enter your e-mail' )">
                    </div>
                    <div class="form-group">
                        <input value="@auth{{ Auth::user() -> phone }}@else{{ old( 'phone' ) }}@endauth" type="text" name="phone" class="inp-style  form-control input-btn" placeholder="@lang( 'Enter your phone number' )">
                    </div>
                    <div class="form-group">
                        <label for="agree">
                            <span class="ml-4 mb-0">
                                <div class="custom-control custom-checkbox mb-3">
                                    <input required="" type="checkbox" class="custom-control-input" id="agreement" name="agreement">
                                    <label class="custom-control-label" for="agreement">@lang( "I agree" ) <a class="agreement-href" href="{{ route( 'rules' ) }}" target=_blank><u>@lang( "to the terms and conditions of service" )</u></a></label>
                                </div>
                            </span>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block text-center  px-3 py-2 exchange-btn">@lang( 'Exchange now' )</button><br>
                    </div>
                </form>