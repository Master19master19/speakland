<header>
	<nav class="navbar navbar-expand-lg bg-theme">
	  <a class="navbar-brand ml-xl-4" href="/home">
	  	<img src="/logo.png" width="75">
	  </a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse row" id="navbarSupportedContent">
	    <div class="navbar-nav mx-auto col-lg-5 col-xl-5 row text-center text-lg-left">
	    	<div class="row ml-auto">
	    		<div class="col-lg-3 my-4 my-lg-4">
	    			<span class="font-weight-bold text-theme">
		    			{{ Auth::user() -> mission -> title }}
	    			</span>
	    		</div>
	    		<div class="col-lg my-4 my-lg-4">
	    			@include( 'components.progress' )
	    		</div>
	    	</div>
	    </div>
	    <div class="ml-auto col-lg-5 col-xl-3">
	    	<div class="row text-center text-lg-right">
	    		<!-- <div class="col-lg font-weight-bold text-warning my-3 my-lg-2">
	    			X-Pounds: {{ Auth::user() -> balance }}
	    		</div> -->
	    		<div class="col-lg ">
	             <div class="nav-item dropdown">
	             	<div style="width: {{ ( strlen( Auth::user() -> name ) < 2 ) ? '104px' : '' }};" class=" header-user mr-xl-4">
				        <a class="nav-link pt-0 text-black dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          {{ Auth::user() -> name }}
	    					<img style="margin-left: 8px;" class="" src="/assets/img/user.png" height="35">
				        </a>
				        <div class="dropdown-menu centered" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="{{ route( 'logout' ) }}">Выйти</a>
				        </div>
	             	</div>
			      </div>
	    		</div>
	    	</div>
	    </div>
	  </div>
	</nav>
	<hr class="my-0">
</header>