
    <div class="col-lg-3 sidebar">
        <div class="section-title align-middle d-block mt-5">
            <div class="align-middle">
                <a href="/home" class="btn btn-outline-theme btn-block mb-3"><b>Missions</b></a>
            </div>
            <div class="align-middle">
                <a href="/safe" class="btn btn-outline-theme btn-block mb-3"><b>My safe</b></a>
            </div>
           <!--  <div class="align-middle">
                <a href="/notifications" class="btn btn-outline-theme btn-block mb-3">Уведомления</a>
            </div> -->
            <div >
                <a href="/settings" class="btn btn-outline-theme btn-block mb-3"><b>My Settings</b></a>
            </div>
            <!-- <div >
                <a href="/promotions" class="btn btn-outline-theme btn-block mb-3"><b>Акции и розыгрыши</b></a>
            </div> -->
        </div>



        @isset ( $sideBarInfo )
        <section>
            


            <div id="accordion" class="accordio">
                @foreach ( $sideBarInfo[ 'sections' ] as $key => $section )
  <div class="card">
    <div class="card-header" id="headingOne">
      <!-- <h5 class="mb-0"> -->
        <div class="btn btn-link d-flex text-theme" data-toggle="collapse" data-target="#section-{{ $key }}" aria-expanded="true" aria-controls="section-{{ $key }}">
            <span class="mr-auto">
                {{ $section -> title }}
            </span>
            <span class="ml-auto ">
                <i class="fa fa-chevron-down"></i>
            </span>
        </div>
      <!-- </h5> -->
    </div>

    <div id="section-{{ $key }}" class="collapse {{ ! isset( $sideBarInfo[ 'section' ] ) && $key == 0 ? 'show' : isset( $sideBarInfo[ 'section' ] ) && $sideBarInfo[ 'section' ] -> id == $section -> id ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-unstyled ml-2">
            @foreach ( $section -> steps as $step )
                <li class="">
                    @if ( Auth::user() -> canStep( $step -> id ) === true )
                        <span class="icon-check tiny"></span>
                        <a href="{{ route( 'account.step' , $step -> id ) }}" class="text-theme">{{ $step -> title }}</a>
                    @elseif ( Auth::user() -> canStep( $step -> id ) === 1 )
                        <span class="icon-spinner tiny"></span>
                        <a href="{{ route( 'account.step' , $step -> id ) }}" class="text-theme">{{ $step -> title }}</a>
                    @else
                        <span class="icon-lock tiny"></span>
                        <a href="#" class="text-theme">{{ $step -> title }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
  @endforeach

  <div class="card">
    <div class="card-header" id="headingOne">
      <!-- <h5 class="mb-0"> -->
        <div class="btn btn-link d-flex text-theme" aria-expanded="true">
            @if ( Auth::user() -> canQuiz( $sideBarInfo[ 'quiz' ] -> id ) )
                <a class="mr-auto text-theme" href="{{ route( 'account.quiz' , $sideBarInfo[ 'quiz' ] -> id )}}">
                    {{ $sideBarInfo[ 'quiz' ] -> title }}
                </a>
            @else
                <span class="mr-auto text-theme">
                    {{ $sideBarInfo[ 'quiz' ] -> title }}
                </span>
            @endif
        </div>
      <!-- </h5> -->
    </div>
  </div>
</div>

        </section>
        @endisset
    </div>

