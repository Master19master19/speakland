<footer class="mt-5  mb-0 container-fluid footer fixed-bottom bg-theme pt-3">
			<p class="footer-desc text-theme text-center">© {{ date( 'Y' ) }} , speakland.pro — Учись английский</p>
	<div class="row" style="display: none;">
		<div class="col-8 col-lg-4 col-xl-5">
			<p class="footer-desc text-theme text-center">© {{ date( 'Y' ) }} , speakland.pro — Учись английский</p>
		</div>
		<div class="col col-lg-6 col-xl-5 d-none d-lg-block">
			<u><a class="px-3" href="">Контакты</a></u>
			<u><a href="#theme" class="px-3" data-id="btn-theme">Изменить тему</a></u>
		</div>
		<div class="text-center col-12 d-lg-none mb-3">
			<u><a class="px-3" href="">Контакты</a></u>
			<u><a href="#theme" class="px-3" data-id="btn-theme">Изменить тему</a></u>
		</div>
	</div>
</footer>

