<div class="row mx-1 ">
	<a href="@if ( App::isLocale( 'en' ) ) {{ '/en/account' }} @else {{ '/account' }} @endif" class="col-12 col-md-4 text-center p-3  btn-for-rules account-header-btn @if ( strpos( Route::current() -> getName() , 'accountHistory' ) === false && strpos( Route::current() -> getName() , 'accountTickets' ) === false ) active @endif">
		<img src="/img/profile-icon.png" alt="Profile icon" height="20" width="20" />
		<span class="px-2 text-secondary btn-rules-to-change">
			<h6 class="d-inline btn-for-rules-txt">@lang( 'Account' )</h6>
		</span>
	</a>
	<a href="@if ( App::isLocale( 'en' ) ) {{ '/en/account.history' }} @else {{ '/account.history' }} @endif" class="col-12 col-md-4 text-center p-3  btn-for-rules account-header-btn @if ( strpos( Route::current() -> getName() , 'accountHistory' ) !== false ) active @endif">
		<img src="/img/arrow.png" alt="Profile icon" height="20" width="20" />
		<span class="px-2 text-secondary btn-rules-to-change">
			<h6 class="d-inline btn-for-rules-txt">@lang( 'Transaction history' )</h6>
		</span>
	</a>
	<a href="@if ( App::isLocale( 'en' ) ) {{ '/en/account.tickets/' . auth() -> user() -> id }} @else {{ '/account.tickets/' . auth() -> user() -> id }} @endif" class="col-12 col-md-4 text-center p-3  btn-for-rules account-header-btn @if ( strpos( Route::current() -> getName() , 'accountTickets' ) !== false ) active @endif">
		<img src="/img/arrow.png" alt="Profile icon" height="20" width="20" />
		<span class="px-2 text-secondary btn-rules-to-change">
			<h6 class="d-inline btn-for-rules-txt">@lang( 'My tickets' )</h6>
		</span>
	</a>
</div>