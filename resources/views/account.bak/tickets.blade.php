@extends( 'layouts.app' )
@section( 'head' )
<link rel="stylesheet" type="text/css" href="/css/profile.css" />
@endsection
@section( 'content' )



<section class="pt-5">
	<div class="">
		<h2 class="pb-4 text-center txt-h-font">@lang( 'Customer Account' )</h2>
	</div>
    @if (session('status'))
    <div class="text-center alert alert-success">
        {{ __( session( 'status' ) ) }}
    </div>
    @endif
	<div class="profile-background mx-md-5 mx-2 pt-4">
		@include( 'account.components.collapse' )
		<div class="mx-lg-5 mx-1 pb-4">
			<div class="mx-lg-5 mx-1 pt-3">
				<p class="pl-3 	profile-title-font">@lang( 'Account' )</p>
				<p class="pl-3 profile-settings-txt">@lang( 'You can change the basic settings of the account and leave data for communication:' )</p>
			</div>				
			<hr class="mx-lg-5 mx-1">
			<form class="form" method="POST" action="{{ route( 'userSavePersonalData' ) }}">
				@csrf
				<div class="mx-lg-5 mx-1 pt-3">
					<h6 class="pl-1">@lang( 'Personal data:' )</h6>
					<p class="px-3 pt-3">@lang( 'Your name' )</p>
					<div class="px-3">
						<input type="text" autofocus="" name="name" class="inp-style my-2 form-control input-btn @error( 'name' ) is-invalid @enderror" value="{{ $user -> name }}">

	                    @error( 'name' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
					<p class="px-3 pt-3">@lang( 'E-mail' )</p>
					<div class="px-3">
						<input type="email" name="email" class="@error( 'email' ) is-invalid @enderror inp-style my-2 form-control input-btn" value="{{ $user -> email }}">
	                    @error( 'email' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
					<span class="pl-3 profile-password-rule">Ваша почта верифицирована</span>
					<p class="px-3 pt-3">@lang( 'Telegram' )</p>
					<div class="px-3">
						<input type="text" name="telegram" class="@error( 'telegram' ) is-invalid @enderror inp-style my-2 form-control input-btn" value="{{ $user -> telegram }}">
	                    @error( 'telegram' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
					<p class="px-3 pt-3">@lang( 'Phone number' )</p>
					<div class="px-3">
						<input type="text" name="phone" class="@error( 'phone' ) is-invalid @enderror inp-style my-2 form-control input-btn" value="{{ $user -> phone }}">
	                    @error( 'phone' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<hr class="mx-lg-5 mx-1">
				<div class="mx-lg-5 mx-1">
					<h6 class="pt-3 pl-1">@lang( 'Password settings' ):</h6>
					<p class="px-3 pt-3">@lang( 'Old password' ):</p>
					<div class="px-3">
						<input type="text" value="" name="old_password" class="@error( 'old_password' ) is-invalid @enderror inp-style my-2 form-control input-btn" placeholder="">
						@error( 'old_password' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
					<p class="px-3 pt-3">@lang( 'New password' ):</p>
					<div class="px-3">
						<input type="text" value="" name="password" class="@error( 'password' ) is-invalid @enderror inp-style my-2 form-control input-btn" placeholder="">
						@error( 'password' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
					<span class="px-3  pl-3 profile-password-rule">@lang( 'Password must contain 8 characters and include: numbers, Latin lowercase and uppercase letters' )</span>
					<p class="px-3 pt-3">@lang( 'Repeat password' ):</p>
					<div class="px-3">
						<input type="text" value="" name="password_confirmation" class="@error( 'password_confirmation' ) is-invalid @enderror inp-style my-2 form-control input-btn" placeholder="">
						@error( 'password_confirmation' )
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ __( $message ) }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="mx-lg-5 mx-1 mt-4 pt-2">
					<div class="form-group mx-lg-3 mx-1">
						<button type="submit" class="btn btn-block btn-choose">@lang( 'Submit' )</button>
					</div>
				</div>
			</form>
			<div class="mx-lg-5 mx-1 mt-4 pt-2">
				<div class="form-group mx-lg-3 mx-1">
					<a class="btn btn-block btn-danger" href="@if ( App::isLocale( 'en' ) ) {{ '/en/logout' }} @else {{ 'logout' }} @endif">@lang( 'Logout' )</a>
				</div>
			</div>
			<!-- <hr class="mx-lg-5 mx-1">
			<div class="mx-lg-5 mx-1 pt-3">
				<h6 class="pl-1">Авторизация через соц. сети</h6>
				<p class="pl-3 profile-settings-txt">Подключите свои социальные сети для удобства моментальной авторизации в Личном кабинете</p>
				<p class="pl-3">google.com</p>
				<p class="pl-3">Facebook</p>
				<p class="pl-3">VKontakte</p>
			</div>
			<hr class="mx-lg-5 mx-1"> -->
		</div>


	</div>
</section>


@endsection