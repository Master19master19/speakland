@extends( 'layouts.app' )
@section( 'head' )
<link rel="stylesheet" type="text/css" href="/css/profile.css" />
@endsection
@section( 'content' )



<section class="pt-5">
	<div class="">
		<h2 class="pb-4 text-center txt-h-font">@lang( 'Customer Account' )</h2>
	</div>
	<div class="profile-background mx-5 pt-4">
		@include( 'account.components.collapse' )
		<div class="exchange-search-background pt-4 py-2">
			<div class="d-inline">
				<!-- <i class="fas fa-calendar-alt icon ml-2"></i> -->
				<!-- <input type="text" class="inp-style px-3 d-inline profile-search-btn" placeholder="Выберите дату"> -->
				<!-- <i class="fas fa-calendar-alt icon ml-1"></i> -->
				<!-- <input type="text" class="inp-style px-3 d-inline profile-search-btn" placeholder="Выберите дату"> -->
				<!-- <i class="fas fa-search icon ml-3"></i> -->
				<!-- <input type="text" class="inp-style px-3 d-inline profile-search-btn" placeholder="Поиск по ID"> -->
				<div class="py-2">
					<span class="txt-clr-rules ml-4"><a class="font-weight-bold pl-1 text-secondary profile-txt-size btn-rules-to-change" href="?sort=today">@lang( 'Today' )</a></span>
					<span class="txt-clr-rules"><a class="font-weight-bold pl-1 text-secondary profile-txt-size btn-rules-to-change" href="?sort=week">@lang( 'Week' )</a></span>
					<span class="txt-clr-rules"><a class="font-weight-bold pl-1 text-secondary profile-txt-size btn-rules-to-change" href="?sort=month">@lang( 'Month' )</a></span>
					<span class="txt-clr-rules"><a class="font-weight-bold pl-1 text-secondary profile-txt-size btn-rules-to-change" href="?sort=year">@lang( 'Year' )</a></span></div>
				</div>
			</div>
			<table class="table bg-white">
				<thead class="">
					<tr class="py-3 font-weight-bold text-uppercase profile-history-id-txt" style="border-bottom: 3px solid #CFE7AD;">
						<th class="py-3">ID</th>
						<th class="py-3">@lang( 'Time' )</th>
						<th class="py-3">@lang( 'Given' )</th>
						<th class="py-3">@lang( 'Received' )</th>
						<th class="py-3">@lang( 'Wallet' )</th>
						<th class="py-3">@lang( 'Status' )</th>
					</tr>
				</thead>
				<tbody>
					@forelse ( $transactions as $trans )
						<tr>
							<td>
								{{ $trans -> id }}
							</td>
							<td>
								{{ date( 'Y-m-d H:i:s' , strtotime( $trans -> created_at ) ) }}
							</td>
							<td>
								{{ $trans -> total_send / env( 'COIN_DECIMAL' ) }} {{ $trans -> from -> code }}
							</td>
							<td>
								{{ round( $trans -> total_receive / env( 'COIN_DECIMAL' ) , env( 'COIN_FLOAT' ) ) }} {{ $trans -> to -> code }}
							</td>
							<td>
								{{ $trans -> btc_address }}
							</td>
							<td>
								{{ $trans -> status_title }}
							</td>
						</tr>
					@empty
						<tr class="text-center">
							<td colspan="45" class="py-5 my-5 pt-3"><h3>@lang( 'No operations found' )</h3></td>
						</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</section>


	@endsection