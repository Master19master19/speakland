<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get( '/theme/{theme}' , 'SessionController@setTheme' );
Auth::routes();

Route::post( '/payment/notify' , 'PaymentController@notify' );

Route::get( '/' , 'HomeController@index' );
Route::get( '/fat' , 'HomeController@fat' );

Route::get( '/promotion-terms' , 'HomeController@promotion_terms' );
Route::get( '/terms-of-use' , 'HomeController@terms' );
Route::get( '/privacy-policy' , 'HomeController@privacy' );
Route::post( '/createPayment' , 'PaymentController@create' ) -> name( 'create_payment' );
Route::get( '/login/{email}' , 'HomeController@login_with_email' );
Route::post( '/payment/success' , 'HomeController@payment_success' );
Route::post( '/payment/error' , 'HomeController@payment_error' );
Route::get( '/payment/{email}' , 'PaymentController@create_with_email' );

Route::group([ 'middleware' => [ 'auth' ] ] , function() {
	Route::get( '/logout' , 'HomeController@logout' ) -> name( 'logout' );
	Route::get( '/home' , 'HomeController@home' ) -> name( 'account.home' );
	Route::get( '/settings' , 'HomeController@settings' );
	Route::get( '/promotions' , 'HomeController@promotions' );
	Route::get( '/notifications' , 'HomeController@notifications' );
	Route::get( '/safe' , 'HomeController@safe' );
	Route::get( '/mission/{missionId}' , 'HomeController@mission' ) -> name( 'account.mission' );
	Route::get( '/quiz/{missionId}' , 'HomeController@quiz' ) -> name( 'account.quiz' );
	Route::get( '/section/{sectionId}' , 'HomeController@section' ) -> name( 'account.section' );
	Route::get( '/step/{stepId}' , 'HomeController@step' ) -> name( 'account.step' );
	Route::get( '/step/{stepId}/done' , 'HomeController@stepDone' ) -> name( 'account.stepDone' );
	Route::post( '/step/{stepId}/progress' , 'HomeController@stepProcess' ) -> name( 'account.stepProcess' );
	Route::post( '/register.normal' , 'HomeController@register' ) -> name( 'register_normal' );
	Route::post( '/register.try' , 'HomeController@register_try' ) -> name( 'register_try' );
	Route::get( '/safe/save/{id}/{type}' , 'SafeController@save' ) -> name( 'safe.save' );
	Route::post( '/settings' , 'HomeController@save_settings' ) -> name( 'settings.save' );
});
Route::get( '/admin/sms' , 'Admin\SMSController@index' ) -> name( 'admin.sms' );
Route::post( '/admin/check_sms' , 'Admin\SMSController@check' ) -> name( 'admin.check_sms' );
Route::get( '/admin/login' , 'Admin\MainController@login' ) -> name( 'admin.login' );
Route::post( '/admin/login' , 'Admin\MainController@loginPost' ) -> name( 'admin.loginPost' );
Route::group([ 'middleware' => [ 'auth' ] ] , function() {
	Route::get( '/logout' , 'Admin\MainController@logout' ) -> name( 'logout' );
});
Route::group([ 'middleware' => [ 'auth' , 'App\Http\Middleware\Admin' ] , 'prefix' => 'admin' ] , function() {
	Route::get( '/' , 'Admin\MainController@index' ) -> name( 'admin.index' );
	Route::get( '/canceled' , 'Admin\MainController@canceled_transactions' ) -> name( 'admin.canceled_transactions' );
	Route::post( '/transaction/status/{id}' , 'Admin\MainController@transactionStatus' );
	Route::get( '/users' , 'Admin\UserController@index' ) -> name( 'admin.users' );
	Route::get( '/tickets' , 'Admin\ChatController@index' ) -> name( 'admin.tickets' );
	Route::post( '/proxies/upload' , 'Admin\ProxyController@upload' ) -> name( 'proxies.upload' );
	Route::post( '/proxies/store' , 'Admin\ProxyController@store' ) -> name( 'proxies.store' );
	Route::resources([
		'admin_missions' => 'Admin\MissionController',
		'admin_sections' => 'Admin\SectionController',
		'admin_steps' => 'Admin\StepController',
		'admin_quizzes' => 'Admin\QuizController',
		'admin_questions' => 'Admin\QuestionController',
		'proxies' => 'Admin\ProxyController',
		'settings' => 'Admin\SettingController',
		'currency_groups' => 'Admin\CurrencyGroupController',
		'reviews' => 'Admin\ReviewController'
	]);
	Route::get( '/admin_sections/mission/{mission_id}' , 'Admin\SectionController@index_mission' ) -> name( 'admin_sections.mission' );
	Route::get( '/admin_steps/mission/{mission_id}' , 'Admin\StepController@index_mission' ) -> name( 'admin_steps.mission' );
	Route::get( '/admin_steps/section/{section_id}' , 'Admin\StepController@index_section' ) -> name( 'admin_steps.section' );
	Route::get( '/admin_steps/delete/{id}' , 'Admin\StepController@delete' ) -> name( 'admin_steps.delete' );
	Route::get( '/admin_quizzes/delete/{id}' , 'Admin\QuizController@delete' ) -> name( 'admin_quizzes.delete' );
	Route::get( '/admin_questions/delete/{id}' , 'Admin\QuestionController@delete' ) -> name( 'admin_questions.delete' );
	Route::get( '/admin_questions/quiz/{quiz_id}' , 'Admin\QuestionController@index_quiz' ) -> name( 'admin_questions.quiz' );
	Route::get( '/admin_missions/delete/{id}' , 'Admin\MissionController@delete' ) -> name( 'admin_missions.delete' );
	Route::get( '/admin_sections/delete/{id}' , 'Admin\SectionController@delete' ) -> name( 'admin_sections.delete' );
	Route::get( '/admin_payments' , 'Admin\PaymentController@index' ) -> name( 'admin_payments.index' );
	Route::get( '/reviews/publish/{id}' , 'Admin\ReviewController@publish' ) -> name( 'reviews.publish' );
});

Route::get( 'testemail' , 'HomeController@testemail' );